#!/usr/bin/env python
'''Build an example module.

The function `build` is the entry point.  This function creates a new
module file and then starts to add elements to it.  The function,
directly or indirectly, uses the other functions defined in this
script.

We use the API given by
[`pywargame`](https://gitlab.com/wargames_tex/pywargame) as exported
to the LaTeX package
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex).  You will
need to get the `wargame` package, and have Python installed, and
probably also LaTeX.

For GNU/Linux use your package manager to install these

    sudo apt install texlive-latex-recommended python3 \
        poppler-utils python3-pil

For other OS, please follow the instructions of the various package:

- LaTeX: See the [LaTeX project](https://www.latex-project.org/get/)
- Python: For Windows, see for example
  [here](https://docs.python.org/3/using/windows.html), and for MacOSX
  see for example [here](https://docs.python.org/3/using/mac.html).

  There are other ways of installing Python on these two platforms
  that may be more suitable for your - for example via
  [Anaconda](https://www.anaconda.com/),
  [MiniConda](https://docs.anaconda.com/miniconda/install/), or some
  other third-party environment.
- Python packages: Use `pip` to install needed package, for example

      pip install Pillow

'''
from wgexport import *

# ====================================================================
def getBoundingBox(image):
    '''Get bounding box of image

    Parameters
    ----------
    image : str or BytesIO
        Image to process, either a file name or bytes file-like object

    Returns
    -------
    bb : tuple
        Bouding box `(ulx,uly,lrx,lry)` where `ul` is "upper left",
        `lr` is "lower right", and `x` and `y` are the horizontal
        and vertical coordinates,  respectively.  Note, the vertical
        coordinate is starts at the top and increases as you go down.    
    '''
    from PIL import Image

    with Image.open(image) as img:
        return img.getbbox()

# --------------------------------------------------------------------
def readImage(image):
    '''Read image data from disk'

    Parameters
    ----------
    image : str or BytesIO
        Image to read, either filename or file-like object

    Returns
    -------
    img : bytes
        Array of bytes 
    '''
    from io import BytesIO

    with open(image,'rb') as img:
        return img.read()

# --------------------------------------------------------------------
def storeImage(vmod,image,real=None):
    '''Copy image into the VMOD.

    This checks that the image isn't already in the VMod

    Parameters
    ----------
    vmod : VMod
         Module to add image to
    image : str
         Name of image (under sub-directory `images`) in the module.
         If `real` is None, then this is taken to be the name of the
         image file in the current directory which should be read and
         stored in the VMod
    real : str
         If not None, this is the file name of the image on disk to
         read and copy into the VMod.  If None, assume that `image`
         also names and image file in the current directory to read
         and store in the VMod
    '''
    from pathlib import Path

    if real is None:
        real = image
        
    mapping = vmod.getFileMapping()
    if Path(real).stem in mapping:
        return

    vmod.addFile(f'images/{image}', readImage(real))
    
# ====================================================================
def layerMarkTraits(vmod,
                    name,
                    image,
                    basic=False,
                    **marks):
    '''Create a simple layer trait and mark traits

    The layer trait simple contains and image and the layer property
    is set to `name`.  The image is copied to the VMod

    Additional keyword arguments will be added as `MarkTrait` traits
    with the name set to the key, and the value to the value.  For
    example, if the argument

        Faction = "Allied"

    is given, then a `MarkTrait` with name `Faction` and value
    `Allied` will be made.

    Parameters
    ----------
    vmod : VMod
        Module to add image to
    name : str
        Name of image property in `LayerTrait`
    image : str
        Image file name (on disk and in `images` sub-directory)
    basic : bool
        If true, also added an empty `BasicTrait` at the end
    **marks : dict
        Additional `MarkTrait` to define

    Returns
    -------
    traits : list
        List of traits. 
    '''
    storeImage(vmod, image)
    
    return [
        LayerTrait(
            images       = [image],
            newNames     = [''],
            activateName = '',
            activateMask = '',
            activateChar = '',
            increaseName = '',
            increaseMask = '',
            increaseChar = '',
            decreaseName = '',
            decreaseMask = '',
            decreaseChar = '',
            resetName    = '',
            resetKey     = '',
            resetLevel   = 1,
            under        = False,
            underXoff    = 0,
            underYoff    = 0,
            loop         = False,
            name         = name,
            description  = '',
            randomKey    = '',
            randomName   = '',
            follow       = False,
            expression   = '',
            first        = 1,
            version      = 1,
            always       = True,
            activateKey  = '',
            increaseKey  = '',
            decreaseKey  = '',
            scale        = 1.)] + [
                MarkTrait(name = str(key), value = str(value))
                for key, value in marks.items()] + ([] if not basic else
                                                    [BasicTrait()])

# --------------------------------------------------------------------
def stepLayerMarkTraits(vmod,
                        name,
                        image1,
                        image2,
                        follow,
                        basic=False,
                        **marks):
    '''Create steps for a two-step background trait

    The layer trait simple contains two images and the layer property
    is set to `name`.  The images is copied to the VMod

    Additional keyword arguments will be added as `MarkTrait` traits
    with the name set to the key, and the value to the value.  For
    example, if the argument

        Faction = "Allied"

    is given, then a `MarkTrait` with name `Faction` and value
    `Allied` will be made.

    Parameters
    ----------
    vmod : VMod
        Module to add image to
    name : str
        Name of image property in `LayerTrait`
    image1 : str
        1st Image file name (on disk and in `images` sub-directory)
    image2 : str
        2nd Image file name (on disk and in `images` sub-directory)
    basic : bool
        If true, also added an empty `BasicTrait` at the end
    **marks : dict
        Additional `MarkTrait` to define

    Returns
    -------
    traits : list
        List of traits.
    '''
    storeImage(vmod, image1)
    storeImage(vmod, image2)
    
    return [
        LayerTrait(
            images       = [image1,image2],
            newNames     = ['','+ reduced'],
            activateName = '',
            activateMask = '',
            activateChar = '',
            increaseName = '',
            increaseMask = '',
            increaseChar = '',
            decreaseName = '',
            decreaseMask = '',
            decreaseChar = '',
            resetName    = '',
            resetKey     = '',
            resetLevel   = 1,
            under        = False,
            underXoff    = 0,
            underYoff    = 0,
            loop         = False,
            name         = name,
            description  = '',
            randomKey    = '',
            randomName   = '',
            follow       = True,
            expression   = f'{{{follow}}}',
            first        = 1,
            version      = 1,
            always       = True,
            activateKey  = '',
            increaseKey  = '',
            decreaseKey  = '',
            scale        = 1.)] + [
                MarkTrait(name = str(key), value = str(value))
                for key, value in marks.items()] + ([] if not basic else
                                                    [BasicTrait()])

# ====================================================================
def copyPrototypes(pieceSpecs,
                   container,
                   vmod,
                   directory='../pngs'):
    '''Copy prototypes XML snippets into the VMOD and their associated
    images.

    The function loops over the piece specifications in `pieceSpecs`
    and extracts the unique list of prototypes (and associated images)
    to put into the module.

    Then, for each found prototype, it will copy a XML snippet of a
    Prototype to the target container in the VMod, as well as the
    associated image.

    The snippet is read from `<directory>/snippet/` and images from
    `<directory>/images`.  These could have been written by
    `wgmakenato.py`, for example.

    Parameters
    ----------
    pieceSpecs : dict
        Dictionary of piece specifications
    container : Prototypes
        Container of prototypes in the VMod
    vmod : VMod
        Module file to add images to
    directory : str
        Where to read snippets and images from

    ''' 
    types    = []
    echelons = []

    def extract(faction = 'friendly',
                command = 'land',
                type    = 'infantry',
                echelon = 'corps',
                **kwargs):
        return (f'{faction}_{command}_{type}',
                f'{faction}_{command}_{echelon}')
    
    for name, spec in pieceSpecs.items():
        t, e = extract(**spec)

        types.append(t)
        echelons.append(e)

    types    = list(set(types))
    echelons = list(set(echelons))
    dummy    = DummyElement(None,None)
    
    def getProto(name, dummy=dummy):
        from xml.dom.minidom import parse

        dom    = parse(f'{directory}/snippets/{name}.xml')
        protos = dom.getElementsByTagName(Prototype.TAG)
        ret    = []
        for proto in protos:
            po = Prototype(dummy,node=proto)
            ret.append(po)

        return ret

    def addImage(name,vmod):
        storeImage(vmod,name+'.png',f'{directory}/images/{name}.png')
        
    def addProtos(protos,container=container):
        for proto in protos:
            traits = proto.getTraits()
            container.addPrototype(
                name        = proto['name'],
                description = proto['description'],
                traits=traits)

    for name in types+echelons:
        print(name)
        nam = name.replace(' ','-')
        protos = getProto(nam)
        addImage(nam,vmod)
        addProtos(protos,container)
        
# ====================================================================
def addPiece(parent,
             name,
             nation,
             cf,
             mf,
             command   = 'land',
             faction   = 'friendly',
             type      = 'infantry',
             echelon   = 'corps',
             reducedCF = None,
             uniqueID  = '',
             parentID  = '',
             baseWH    = [0,0],
             **kwargs):
    '''Build a piece via prototypes

    Parameters
    ----------
    parent : Element
        Element to add piece to
    name : str
        Name of the piece - Human readable
    nation : str
        The base layer representing the nation of the piece
    cf : int (or str)
        Combat factor
    mf : int (or str)
        Movement factor
    command : str
        Type of command (activity, air, equipment, installation, land,
        missile, sea mine, sea surface, space, sub surface)
    faction : str
        Belligerent side (friendly, hostile, neutral, unknown, dismounted)
    echelon : str
        Echelon (size) of unit (team, squad, section, platoon, company,
        battalion, regiment, brigade, division, corps, army, army group,
        theatre, command(?))
    reducedCF : int (or str)
        If present and not null, then the piece is 2-step, and this
        is the reduced step combat factor
    uniqueID : str
        Unit unique idenfitifer (put on the right)
    parentID : str
        Parent unit unique identifier (put on the left)
    baseWH : tuple of 2 ints
        Width and height of base image ("base.png")

    Returns
    -------
    piece : PieceSlot
        The defined piece
    '''

    # Note that the ordering of the traits is important.
    # Traits that should be evaluated first (for example image layers
    # that should be in the "background") need to be added _last_(!)
    #
    # That also means that traits that are added _first_ override
    # traits that are added later.  Thus, for example, adding a
    # MarkTrait _before_ some PrototypeTrait can override properties
    # defined in that PrototypeTrait
    traits = [
        MarkTrait(name = 'UniqueID', value = uniqueID),
        MarkTrait(name = 'ParentID', value = parentID),
        MarkTrait(name = 'MF',       value = mf),
        MarkTrait(name  = 'FullCF' if reducedCF else 'CF', value = cf)]
    if reducedCF:
        traits.extend([MarkTrait(name = 'ReducedCF',
                                 value = reducedCF),
                       PrototypeTrait(name = '2Step')])

    cmd = command.replace(' ','-')
    ech = echelon.replace(' ','-')
    typ = type   .replace(' ','-')
    traits.extend([
        PrototypeTrait(name = '2Factors'), # adds factors
        PrototypeTrait(name = 'LeftID'),   # adds parent ID
        PrototypeTrait(name = 'RightID'),  # adds unique ID
        PrototypeTrait(name = f'{faction}_{cmd}_{ech}'), 
        PrototypeTrait(name = f'{faction}_{cmd}_{typ}'), 
        PrototypeTrait(name = f'{nation}{"2Step" if reducedCF else ""}'),
        BasicTrait(name = name,
                   filename = 'base.png')])

    return parent.addPieceSlot(entryName = name,
                               traits    = traits,
                               width     = baseWH[0],
                               height    = baseWH[1])
                                                

# --------------------------------------------------------------------
def addAtStart(pieceSpecs,pieceMap,map,board):
    '''Place pieces that define the `at-start` key
    on the board in their starting locations

    Parameters
    ----------
    pieceSpecs : dict
        Dictionary of piece specifications
    pieceMap : dict
        Mapping from piece name to PieceSlot object
    map : Map
        Map to add at-start piece to
    board : Board
        Board, within map, to add the at-start to
    '''
    locations = {}
    # First, collect the locations to add at, so that we only add one
    # `AtStart` per unique location.
    for name, spec in pieceSpecs.items():
        loc = spec.get('at-start',None)
        if not loc:
            continue

        if loc not in locations:
            locations[loc] = []

        locations[loc].append(name)

    # The name of the board
    boardName = board["name"]
    
    # Now create our `AtStart` elements
    for location,names in locations.items():
        pieces = [pieceMap.get(name,None) for name in names]

        atStart = map.addAtStart(
            name             = f'{boardName}@{location}',
            location        = location,
            useGridLocation = True,
            owningBoard     = boardName)

        atStart.addPieces(*pieces)
            

        

# ====================================================================
def build():
    '''Build our module'''

    # Open the module as a context manager, so that all we do below
    # goes in the right place, and the module is closed automatically
    # when we finish.
    with VMod('Test.vmod','w') as vmod:

        # ------------------------------------------------------------
        # Module data
        moduleData = ModuleData()
        data       = moduleData.addData()
        data.addVersion      (version    = '0.1.0')
        data.addVASSALVersion(version    = '3.7.0')
        data.addName         (name       = 'Test')
        data.addDescription  (description= 'A test')
        data.addDateSaved    ()

        # ------------------------------------------------------------
        # Build file 
        build = BuildFile()

        # ------------------------------------------------------------
        # Game and basic setup 
        game  = build.addGame(name = 'Test',
                                   version = '0.1.0',
                                   description = 'A test')
        game.addBasicCommandEncoder()
        globalOptions = game.addGlobalOptions(
            autoReport         = GlobalOptions.PROMPT,
            centerOnMove       = GlobalOptions.PROMPT,
            nonOwnerUnmaskable = GlobalOptions.PROMPT,
            playerIdFormat     = '$playerName$')
        globalOptions.addOption(name='undoHotKey',value=key('Z'))
        globalOptions.addOption(name='undoIcon',  value='/images/Undo16.gif')
        
        game.addPlayerRoster()

        # ------------------------------------------------------------
        # Global properties 
        globalProperties    = game.addGlobalProperties()

        # ------------------------------------------------------------
        # Prototypes
        prototypesContainer = game.addPrototypes()

        # -- Faction prototypes --------------------------------------
        # We add two kinds - one that does not have a step and one
        # that does.  Mainly for illustration purposes. 
        for faction in ['German', 'UK', 'US']:
            imgBase = faction.lower()
            prototypesContainer.addPrototype(
                name        = faction,
                description = f'{faction} prototype',
                traits      = layerMarkTraits(
                    vmod,
                    name    = 'Faction',
                    image   = f'{imgBase}.png',
                    basic   = True,
                    Faction = faction))
            prototypesContainer.addPrototype(
                name        = faction+'2Step',
                description = f'{faction} (2 steps) prototype',
                traits      = stepLayerMarkTraits(
                    vmod,
                    name    = 'Faction',
                    image1  = f'{imgBase}.png',
                    image2  = f'{imgBase}_flipped.png',
                    follow  = 'Step',
                    basic   = True,
                    Faction = faction))

        # -- 2-step prototype ----------------------------------------
        # This defines a state called `Step` and modifies the `CF`
        # property according to properties `FullCF` and `ReducedCF` in
        # the piece it self.
        traits = [
            DynamicPropertyTrait(
                ['Step loss',key('F'),DynamicPropertyTrait.INCREMENT,'{1}'],
                ['Reinforce',key('R'),DynamicPropertyTrait.DIRECT,   '{1}'],
                name = 'Step',
                numeric = True,
                min     = 1,
                max     = 2),
            CalculatedTrait(
                name = 'CF',
                expression = '{Step==1?FullCF:ReducedCF}'),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = '2Step',
                                         description = 'Units with 2 steps',
                                         traits      = traits)

        # -- Factors prototype ---------------------------------------
        # A simple 2-factors prototype that will use the piece
        # properties `CF` and `MF`.  Note, the can be calculated
        # dynamically.
        traits = [
            LabelTrait(
                label           = '{CF+"-"+MF}', # dynamic change :-)
                labelKey        = '',
                menuCommand     = '',
                fontSize        = 24,
                background      = '',
                foreground      = rgb(0,0,0),
                vertical        = LabelTraitCodes.BOTTOM,
                verticalOff     = -5,
                horizontal      = LabelTraitCodes.CENTER,
                horizontalOff   = 0,
                verticalJust    = LabelTraitCodes.BOTTOM,
                horizontalJust  = LabelTraitCodes.CENTER,
                nameFormat      = '',
                fontFamily      = 'SansSerif',
                fontStyle       = LabelTraitCodes.BOLD,
                rotate          = 0,
                propertyName    = 'Factors',
                description     = '2 factors, CF and MF',
                alwaysUseFormat = False),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = '2Factors',
                                         description = 'Units with 2 factors',
                                         traits      = traits)

        # -- Identifiers ---------------------------------------------
        # Left and right side identifiers.  
        for name,prop,vplace,voff,hoff in \
                [('LeftID','ParentID',LabelTraitCodes.TOP,4,6),
                 ('RightID','UniqueID',LabelTraitCodes.BOTTOM,-4,6)]:
            traits = [
                LabelTrait(
                    label           = f'{{{prop}}}',
                    labelKey        = '',
                    menuCommand     = '',
                    fontSize        = 14,
                    background      = '',
                    foreground      = rgb(0,0,0),
                    vertical        = vplace,
                    verticalOff     = voff,
                    horizontal      = LabelTraitCodes.CENTER,
                    horizontalOff   = hoff,
                    verticalJust    = vplace,
                    horizontalJust  = LabelTraitCodes.CENTER,
                    nameFormat      = '',
                    fontFamily      = 'SansSerif',#Sans Serif
                    fontStyle       = LabelTraitCodes.BOLD,
                    rotate          = 270,
                    propertyName    = name,
                    description     = f'{name} showing {prop}',
                    alwaysUseFormat = False),
                BasicTrait()]
            prototypesContainer.addPrototype(name        = name,
                                             description = f'Show {prop}',
                                             traits      = traits)
            
        # ------------------------------------------------------------
        # Pieces
        pieceWindow = game.addPieceWindow(
            name    = 'Counters',
            icon    = '/images/counter.gif',
            hotkey  = key('C',ALT))
        pieceTabs   = pieceWindow.addTabs(entryName='Counters')
        piecePanel  = pieceTabs.addPanel(entryName='Counters', fixed=False)

        # -- Store drop-shadow base image ----------------------------
        baseBB = getBoundingBox('base.png')
        baseWH = [int(abs(baseBB[2]-baseBB[0])),int(baseBB[3]-baseBB[1])]
        storeImage(vmod, 'base.png')

        # -- Specs of pieces -----------------------------------------
        # We can add more specs there, possibly leaving out settings
        # for which the defaults are OK (see `addPiece`).  We could
        # also be reading these specs from some external source, such
        # as a JSON file or a spreadsheet. 
        pieceSpecs = {
            'German 9th Army XXIII Corps 102 Infantry Division': {
                'cf'        : 4,
                'reducedCF' : 2,
                'mf'        : 3,
                'uniqueID'  : 102,
                'parentID'  : 'XXIII',
                'nation'    : 'German',
                'command'   : 'land',
                'faction'   : 'friendly',
                'echelon'   : 'division',
                'type'      : 'infantry',
                'at-start'  : 'B2'},
            'UK 8th Army, XXX Corps, 4th Armoured Brigade, 2nd Scots Guard Battalion': {
                'cf'        : 1,
                'mf'        : 6,
                'uniqueID'  : "2SG",
                'parentID'  : "4",
                'nation'    : 'UK',
                'echelon'   : 'battalion',
                'type'      : 'armoured infantry',
                'at-start'  : 'C3'
            }
        }

        # -- First get our prototypes needed -------------------------
        # This will fetch prototype snippets and images from the
        # parent directory, and put them into the module.  Note, we
        # only take the snippets and images we need.
        copyPrototypes(pieceSpecs, prototypesContainer, vmod)
        
        # -- Loop through specs and add pieces -----------------------
        # We store the pieces in a dictonary, so that we may add them
        # at-start if we need to.
        pieceMap = {}
        for name, spec in pieceSpecs.items():
            p = addPiece(piecePanel,
                         name = name,
                         baseWH = baseWH,
                         **spec)
            pieceMap[name] = p

        # ------------------------------------------------------------
        # Map
        map = game.addMap(mapName = 'Map', markUnmovedHotkey=key('M'))
        map.addCounterDetailViewer(
            fontSize = 14,
            summaryReportFormat = '<b>$LocationName$</b>',
            hotkey = key('\n'),
            stopAfterShowing = True)
        map.addHidePiecesButton()
        map.addGlobalMap()
        # Basics
        map.addStackMetrics()
        map.addImageSaver()
        map.addTextSaver()
        map.addForwardToChatter()     
        map.addMenuDisplayer()        
        map.addMapCenterer()          
        map.addStackExpander()        
        map.addPieceMover()           
        map.addKeyBufferer()          
        map.addSelectionHighlighters()
        map.addHighlightLastMoved()   
        map.addZoomer()

        # ------------------------------------------------------------
        # Board
        vmod.addFile('images/board.png',readImage('board.png'))
        
        boardBB         = getBoundingBox('board.png')
        ulx,uly,lrx,lry = boardBB
        width           = int(abs(ulx-lrx))
        height          = int(abs(uly-lry))
        
        picker = map.addBoardPicker()
        board  = picker.addBoard(name     = 'Board',
                                 image    = 'board.png',
                                 width    = width,
                                 height   = height)
        zoned  = board.addZonedGrid()
        zoned.addHighlighter()
        zone   = zoned.addZone(name          = 'Full',
                               useParentGrid = False,
                               path          =(f'{ulx},{uly};' +
                                               f'{lrx},{uly};' +
                                               f'{lrx},{lry};' +
                                               f'{ulx},{lry}'))

        # Grid parameters often need tweaking
        grid = zone.addHexGrid(color        = rgb(0,0,0),
                               x0           = 59,
                               y0           = 0,
                               # edgesLegal   = True,
                               # cornersLegal = True,
                               visible      = False)
        grid.addNumbering(color    = rgb(0,0,0),
                          vDrawOff = 0,
                          fontSize = 16,
                          visible  = True)
        
        # ------------------------------------------------------------
        # Set-up some 'at-start' units
        addAtStart(pieceSpecs,pieceMap, map, board)
        
        # ------------------------------------------------------------
        # Add the build and module files to the module
        vmod.addFiles(**{VMod.BUILD_FILE  :
                         build.encode(),
                         VMod.MODULE_DATA :
                         moduleData.encode()})
            
if __name__ == '__main__':
    build()
