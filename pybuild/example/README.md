# Example of using `pywargame` API to build a VASSAL module 

In this directory is a small example of how to use the
[`pywargame`](https://gitlab.com/wargames_tex/pywargame) API to make a
VASSAL module in Python. 

![](screenshot.png)

The built module can be found [here](https://gitlab.com/wargames_tex/vassal_my_contrib/-/jobs/artifacts/master/file/public/example/Test.vmod?job=dist)

The example uses the LaTeX package
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex) to generate
the graphics.  However, if you generate the graphics in some other
way, for example using [Inkscape](https://inkscape.org) you can
equally well use the same approach as used here. 

## Building the module 

The main point here is the Python script [`build.py`](build.py) which
generates the VASSAL module.  I will not go into details about that
script here.  The script is heavy commented, and I refer you to those
comments and documentation strings. 

## Requirements 

Again, please see the top of the [`build.py`](build.py) script for
specific requirements of this example.  

In general, you need a way to make the graphics (board, tables, etc.).
That could be [Inkscape](https://inkscape.org) or - as I prefer -
LaTeX with the
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex) package. 

Then, you also need Python and the
[`pywargame`](https://gitlab.com/wargames_tex/pywargame) Python
package.   Note, if you use
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex), you already
have the `pywargame` package (sort of), and so you don't need to
install that. 

In this example, the graphics is made from the LaTeX source in
[`example.tex`](example.tex). That simply generates the board,
background images for some nations, and a base image to add
drop-shadows to the pieces. 

Other graphics for the NATO App6 symbology is taken from the snippets
and images in the [parent directory](..).

## To build 

Just do 

    make 
    
That should give you `Test.vmod` - see the screenshot above. 

