# Generate XML prototype snippets and images of NATO App6 

The files in this directory are used to make XML snippets of
`Prototype`s and (PNG or SVG) images to include into a VASSAL module. 

This uses the script `wgmakenato.py` from the LaTeX
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex) package. 

## Pre-built snippets and images 

Pre-build XML prototype snippets and images can be downloaded 
[here](https://gitlab.com/wargames_tex/vassal_my_contrib/-/jobs/artifacts/master/download?job=dist).
You can also them [here](https://gitlab.com/wargames_tex/vassal_my_contrib/-/jobs/artifacts/master/browse?job=dist)

## Requirements 

You need 

- An installation of LaTeX 
- An installation of Python and some packages 
- An installation of recent `wargame` package for LaTeX 

To install the first 2 on GNU/Linux (using `apt` - adapt for `yum` or
similar) 

    sudo apt install texlive-latex-recommended python3 \
        poppler-utils python3-pil

For other platforms, see 

- LaTeX: See the [LaTeX project](https://www.latex-project.org/get/)
- Python: For Windows, see for example
  [here](https://docs.python.org/3/using/windows.html), and for MacOSX
  see for example [here](https://docs.python.org/3/using/mac.html).

  There are other ways of installing Python on these two platforms
  that may be more suitable for your - for example via
  [Anaconda](https://www.anaconda.com/),
  [MiniConda](https://docs.anaconda.com/miniconda/install/), or some
  other third-party environment.
- Python packages: Use `pip` to install needed package, for example

      pip install Pillow

To install a recent version (>=0.8) of the `wargame` package, please
see the [installation
instructions](https://gitlab.com/wargames_tex/wargame_tex#installation).

Note, the current version (0.6 as of November 2024) of `wargame` that
comes with **TeXLive** is not up to date.  You need version 0.8 or
better. 

## Making the snippets and images 

To make snippets that use PNG images and the images themselves, do 

    make pngs 
    
To make snippets that uses SVG images and the images themselves, do 

    make svgs 
    
## How to use the snippets and images 

If you build your VASSAL module using the
[`pywargame`](https://gitlab.com/wargames_tex/pywargame) then see how
to include these by looking at the [example](example).  In particular
[`example/build.py`](example/build.py). 

If you use VASSAL editor to make your module, you need to do a bit of
hand crafting. 

- Open the snippet you need - e.g.,
  `pngs/snippets/friendly_land_infantry.xml` and copy the
  `<VASSAL.build.module.PrototypeDefinition>` element. Make sure to
  copy the whole thing - e.g., 
  
        <VASSAL.build.module.PrototypeDefinition
         name="friendly_land_infantry"
         description="NATO App6 symbol">+/null/emb2;;;;;;;;;;;;1;false;0;-8;friendly_land_infantry.png;;false;NATOAPP6_command_faction_main;;;false;;1;1;true;;;;NATO App6 symbology;1.0  mark;NATOAPP6_command\  mark;NATOAPP6_type\\    piece;;;;/1     land\   _infantry\\     null;0;0;;0</VASSAL.build.module.PrototypeDefinition>

  Also make sure you copy it _exactly_ as it is. 

- Extract the `buildFile.xml` from your module 

      unzip MyModule.vmod buildFile.xml 
      
- Open `buildFile.xml` in your favourite text editor (_not_ some fancy
  XML editor that doesn't allow you to manipulate the basic XML - on
  Windows, **Notepad** would be a choice). 
  
      edit buildFile.xml 
      
- Find the `<VASSAL.build.module.PrototypesContainer>` element 
  
- Then paste the snippet you copied above into the 
  `<VASSAL.build.module.PrototypesContainer>`
  
        <VASSAL.build.module.PrototypesContainer>
          <VASSAL.build.module.PrototypeDefinition
            name="friendly_land_infantry"
          ...

  Again, make sure you paste the snippet _exactly_. 
  
- Now make a directory called `images` 

        mkdir images 
        
- Copy the image file(s) you need to that directory, for example 

        cp pngs/images/friendly_land_infantry.png images/
        
- Finally, update your module file with the new content 

        zip -r -u MyModule.vmod buildFile.xml images
        
You can now open the module in the VASSAL editor and use the prototype
you just copied in - e.g., `friendly_land_infantry` as shown above. 

Note that unit type prototypes does not contain an echelon
definition. For that, you need the appropriate prototype, e.g.,
`friendly_land_division`. 

## Adding more symbols 

The JSON files in this directory contains specifications for how to
generate the NATO App6 symbols and associated snippets.  You can add
another JSON file with your symbols.  For example, to make an armoured
unit with a main gun, you should do 

    [
        { 
            "output":  "{faction}_{command}_armoured main gun",
            "command": "land",
            "main":    ["armoured","main gun"]
        }
    ]
    
in your custom JSON file.  You should then open the
[`Makefile`](Makefile) and add your JSON file to the `INPUTS`
variable.   Note, above `{faction}` and `{command}` are replaced with
the value passed on the `wgmakenato.py` command line.

## Other factions 

The code is only set-up to make snippets and images for the `friendly`
faction.  If you need another faction, say `hostile`, do 

    make pngs FACTION=hostile 
    
for example. 

## License 

Creative Commons ShareAlike, NoAttribution International Version 4.0
