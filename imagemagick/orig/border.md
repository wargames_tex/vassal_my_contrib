## How do I add border

**Goal:** to add border of selected width.

Relevant reading:

-   ImageMagick docs, [*Adding/Removing Image
    Edges*](https://www.imagemagick.org/Usage/crop/#border).

To add a border use `bordercolor` and `border` commands:

    magick <input-file> -bordercolor <color> ^
                        -border <x width x y width><output-file>

If vertical and horizontal borders are to be of the same width you may
use shorthand `-border <width>`.

**Example**

          magick input\input.png -bordercolor #000000 -border 4 output\output.png

**Explanation**

-   `-bordercolor #000000` sets border color to black;
-   `-border 4` is a shorthand form of `-border 4x4`.

**Result**

![](border_001.png)

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](http://www.vassalengine.org/forum/memberlist.php?mode=viewprofile&u=5042)

Date this document was created: 2020-08-29

Date this document was last modified: 2020-09-02
