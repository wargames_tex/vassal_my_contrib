## How do I add dropshadow

**Goal:** to add drop shadow to images.

Relevant reading:

-   ImageMagick docs, [*Generating Shadows*](https://www.imagemagick.org/Usage/blur/#shadow)
-   ImageMagick docs, [*Layering Multiple Images*](https://www.imagemagick.org/Usage/layers/#layers)

To add drop shadows use following set of instructions:

    magick <input-image> ^
    ^( +clone -background #11111e -shadow percent-opacityxBlur}+shiftX+shiftY ^) ^
    +swap -background none -layers merge <output-image>

The above code also does work for irregular shapes so we may expect it
to get good results for [prettified counters](pretty.md) too:

    magick input\prettyA.png
    ^( +clone -background #11111e -shadow 60x0+4+4 ^) ^
    +swap -background none -layers merge output\output.png

**Explanation**

-   `+clone` clones `input-file` and adds it to image sequence;
-   `-background #11111e` sets the shadow colour;
-   `-shadow 60x0+4+4` generates shadow, 60% opaque, non-blurred (`0`
    set as `Blur`), shifted by 4 pixels horizontally and vertically;

![](drop_001.png)

-   `+swap` swaps images in the image sequence;
-   `-background none` sets background transparent;
-   `-layers merge` combines both images into one, expanding the
    canvas if required, so that it is big enough to fit all images
    with set offsets. In our case, the canvas will be big enough to
    accommodate the image and its shadow (which we shifted by 4 pixels
    with respect to original image).

**Result**

![](drop_002.png)

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)

Date this document was created: 2020-09-02

Date this document was last modified: 2020-09-06
