## Make your counters look pretty (A)

-   [Bevel](bevel.md)
-   [clip](rounded.md) and
-   [add outline](outline.md) to your counters

for hellishly handsome results:

    magick input\input.png -raise 4 +antialias ^
    ( +clone -size %%[fx:w]x%%[fx:h] ^
    canvas:transparent -delete -2 -fill red ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 8,8" ) ^
    +swap -gravity center -compose In -composite ^
    ( +clone -size %%[fx:w+6]x%%[fx:h+6] ^
    canvas:transparent -delete -2 -fill black ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 10,10" ) ^
    +swap -gravity center -compose Over -composite output\output.png

**Result**

![](pretty_001.png)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)

Date this document was created: 2020-08-31

Date this document was last modified: 2020-09-01
