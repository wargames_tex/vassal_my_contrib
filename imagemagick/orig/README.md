# Counter image manipulation ImageMagick on Windows

This site is dedicated to automation of otherwise tedious process of
creating VASSAL modules. It covers very esoteric areas of IT expertise
such as:

-   automation of cutting counter sheets into individual pieces;
-   prettifying counters;

## <a name="assumptions"></a> Assumptions 

- I assume use of Windows operating system;
- I use – in great quantities – [ImageMagick](https://imagemagick.org)
  which is command line image manipulating computer program. It is
  particularly useful for batch manipulation of images;
- I assume readers' familiarity with batch programming (creating `.BAT`
  scripts). 

## Start here

### <a name="crop"></a> Automation of Cropping

Wargame counters in games usually come in sheets, with each sheet
containing several dozens of counters. To create a module--among other
things--one needs to cut these into individual counters. This can
be--and in fact is--tedious, boring and time consuming to the point of
discouraging potential module developers from creating modules at all.

This article addresses, at least to some extent, this issue, providing
you with tools and knowledge required to let you cut the counter
sheets in 5% time you would spend otherwise.

Consider counter sheet from Ardennes '44 (following images are used
only as samples; in real life situation you would use original images,
not scans, for module implementation; for educational purposes these
are more than enough, though):

Front:

![](crop_001.jpeg)

Back:

![](crop_002.jpeg)

Open these in your favorite graphic editor and cut top left stripe
from the front sheet and corresponding stripe from the back sheet
(which will be top right stripe, mind you):

Front (`front-stripe.jpg`)

![](crop_003.png)

Back: (`back-stripe.jpg`)

![](crop_004.png)

As there are 9 counters in one row, width of stripe in pixel should be
divisible by 9 (549px in my case) to ensure all the counters have same
width. Counters of non-uniform width is nothing [ImageMagick can't
handle](#trim) of course, but for purpose of this tutorial let's
assume we want them to be equal.

With your favorite text editor create a Windows batch file containing
following commands:

    magick front-tripe.png -crop 9x2@ output\front-^%%02d.png
    magick back-stripe.png -flop -crop 9x2@ -flop output\back-^%%02d.png

Run it to cut stripes into individual counters:

![](crop_005.png)

![](crop_006.png)

Note: `^%%` is a Windows idiom that within `BAT` resolves to `%`. `%`
has a special meaning in Windows batch programming. In Linux
environments these commands would be a bit different.

Notice--and this is of crucial importance--that both front and back of
the same counter have a common suffix:

![](crop_007.png)

### <a name="windows"></a> Batch processing files in Windows

Most convenient way to mass-process files in Windows is to use `FOR`
loop.

Windows inherited batch scripting from MS-DOS, old operating system,
with all its inconsistencies and peculiar syntax. To use a `FOR` loop
in Windows add below code to your `.BAT` file:

    SETLOCAL EnableDelayedExpansion

    FOR %%G IN (input\*.png) DO (
    ...do stuff
    )

**Explanation**

1.  `%G` is name of a variable that stores name of the file from `input`
    folder. `*.png` that follows filters out everything but PNGs.
2.  `SETLOCAL EnableDelayedExpansion` is required for Windows to
    properly expand values in the loop. If we do not declare this
    option, `%%G` will be set to name of file that was found
    first. More on this peculiar mechanism can be found
    [here](https://ss64.com/nt/delayedexpansion.html). A notable
    quote:

  > The `SET` command was first introduced with MS-DOS 2.0 in March
  > 1983, at that time memory and CPU were very limited and the
  > expansion of variables once per line was enough. Delayed Expansion
  > was introduced some 16 years later in 1999 by which time millions
  > of batch files had been written using the earlier
  > syntax. Retaining immediate expansion as the default preserved
  > backwards compatibility with existing batch file.

![](windows_001.jpeg)

**Example**

Script below adds black 4 pixel border to all PNG images from `input`
directory. Results are saved to `output` folder, changing file name
from `filename.png` to `filename-border.png`.

    SETLOCAL EnableDelayedExpansion

    FOR %%G IN (input\*.png) DO (
            magick ^"%%G^" -bordercolor #000000 -border 4 ^"output\%%~nG-border.png^" 
    )

**Explanation**

1.  things may go wrong if you don't proof your script against
    whitespaces in the filenames. To prevent script go amiss, put quotes
    around filenames. In batch scripting quotes need to be escaped.
    Contrary to POSIX `\` in Windows you escape a character with a
    preceding `^`. In other words `^"%%G^"` puts quotes around filename
    stored in variable `%G`.
2.  `%%~nG` is Windows idiom that expands to name of currently
    processed file. `%%~xG` expands to file extension so we could
    write `%%~nxG` if we wanted to preserve same filename and
    extension. But in our case, as we add `-border` suffix, it made
    more sense to get just the filename. More on this variable
    expanding can be read
    [here](http://www.embeddedframeworks.com/cmds/nt/syntax-args.html).

## Using ImageMagick to prettify counters

### <a name="intermediate"></a> Intermediate steps


#### <a name="bevel"></a> How do I add beveled border

**Goal:** add a bevelled border to counter for a "button-like" look.

Relevant reading:

-   ImageMagick docs, ["Raise or Sunk Borders"](https://www.imagemagick.org/Usage/transform/#raise).

To add a bevelled border use `-raise` command:

          magick <input-file> -raise <width> <output-file>

**Example**

    magick input\input.png -raise 4 output\output.png

**Result**

![](bevel_001.png)

You may of course use `-raise` twice (or thrice, \...) for more
pronounced effect:

          magick input\input.png -raise 4 -raise 4 output\output.png

**Result**

![](bevel_002.png)

Combination of [plain](#border) and bevelled border results with a
look that could probably work for most VASSAL modules if you are not
for something more fancy:

          magick input\input.png -raise 4 -bordercolor #000000 -border 2 output\output.png

**Result**

![](bevel_003.png)

#### <a name="border"></a> How do I add border

**Goal:** to add border of selected width.

Relevant reading:

-   ImageMagick docs, [*Adding/Removing Image
    Edges*](https://www.imagemagick.org/Usage/crop/#border).

To add a border use `bordercolor` and `border` commands:

    magick <input-file> -bordercolor <color> ^
                        -border <x width x y width><output-file>

If vertical and horizontal borders are to be of the same width you may
use shorthand `-border <width>`.

**Example**

          magick input\input.png -bordercolor #000000 -border 4 output\output.png

**Explanation**

-   `-bordercolor #000000` sets border color to black;
-   `-border 4` is a shorthand form of `-border 4x4`.

**Result**

![](border_001.png)

#### <a name="outline"></a> How do I add outline

**Goal:** to add outline around irregular ([rounded
counter?](rounded)) shape

Relevant reading:

-   ImageMagick docs, [*Difference Morphology
    Methods*](https://www.imagemagick.org/Usage/morphology/#difference).

**Approach A**

I admit I do not exactly know why it works. But it works so if that is
enough for you don't let your urge to fight ignorance prevent you from
using it.

    magick <input-file>^
    ( -clone 0 -fill <color> -colorize 100  )^
    ( -clone 0 -alpha extract -virtual-pixel black -morphology edgein octagon )^
    -compose over -composite <output-file>

**Example**

    magick input\rounder-corners.png^
    ( -clone 0 -fill #000000 -colorize 100  )^
    ( -clone 0 -alpha extract -virtual-pixel black -morphology edgein octagon )^
    -compose over -composite output\output.png

**Result**

![](outline_001.png)

`<color>` can be in fact any color so be creative:

![](outline_002.png)

**Approach B**

This time we create a rounded rectangle of dimensions slightly bigger
than the counter itself and then we overlay the bigger rectangle with
the counter.

    magick input\rounder-corners.png +antialias ^
    ( +clone -size %%[fx:w+6]x%%[fx:h+6] ^
    canvas:transparent -delete -2 -fill black ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 18,18" ) ^
    +swap -gravity center -composite output\output.png

**Explanation**

-   `+antialias` switches off antialiasing; should you leave it on,
    the rounded corners will be softened, giving somewhat blurry
    look. Some of you may like, some of you (myself included) not. I
    prefer it without antialiasing;
-   `+clone` clones `input-file` and adds it to image sequence;
-   `-size %%[fx:w+6]x%%[fx:h+6]` sets the image size to be slightly
    (by 3 pixels per each side) bigger than cloned one;
-   `canvas:transparent` creates new image out of thin air with
    transparent background;
-   `-delete -2` deletes last but one image from the sequence (this
    happens to be the one that we `+clone`d, as per second bullet
    above;
-   `-fill black` sets fill colour to black. Black will be the colour
    of the outline; if you wish to use another colour, that\'s the
    place to change it;
-   `-draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 18,18"` draws
    rectangle with rounded corners. Two things worth emphasizing:
    `%[fx:w]`, which is ImageMagick idiom resolved to image width, now
    refers to new image that is 6 pixels wider and 6 pixels higher.
    Also, the radius that we use this time (`18`) is slightly larger
    of what was used in [tutorial on rounding corners](#rounded).
    This is because we want the outer border be of slightly larger
    curvature so that outline is of uniform width;
-   `+swap` swaps two images in the sequence; in our case we just swap
    black rounded rectangle (which should be placed on the bottom)
    with rounded counter (which should go on top);
-   `-gravity center` aligns centres of the images;
-   `-composite` overlaps one image over another. With no method
    specified with `-compose` ImageMagick simply puts one image on
    another.

**Result**

![](outline_003.png)

If we didn't factor the large curvature of the external border, the
results would be somewhat less eye-pleasing:

![](outline_004.png)

#### <a name="drop"></a> How do I add dropshadow

**Goal:** to add drop shadow to images.

Relevant reading:

-   ImageMagick docs, [*Generating
    Shadows*](https://www.imagemagick.org/Usage/blur/#shadow)
-   ImageMagick docs, [*Layering Multiple
    Images*](https://www.imagemagick.org/Usage/layers/#layers)

To add drop shadows use following set of instructions:

    magick <input-image> ^
    ^( +clone -background #11111e -shadow percent-opacityxBlur}+shiftX+shiftY ^) ^
    +swap -background none -layers merge <output-image>

The above code also does work for irregular shapes so we may expect it
to get good results for [prettified counters](#pretty) too:

    magick input\prettyA.png
    ^( +clone -background #11111e -shadow 60x0+4+4 ^) ^
    +swap -background none -layers merge output\output.png

**Explanation**

-   `+clone` clones `input-file` and adds it to image sequence;
-   `-background #11111e` sets the shadow colour;
-   `-shadow 60x0+4+4` generates shadow, 60% opaque, non-blurred (`0`
    set as `Blur`), shifted by 4 pixels horizontally and vertically;

![](drop_001.png)

-   `+swap` swaps images in the image sequence;
-   `-background none` sets background transparent;
-   `-layers merge` combines both images into one, expanding the
    canvas if required, so that it is big enough to fit all images
    with set offsets. In our case, the canvas will be big enough to
    accommodate the image and its shadow (which we shifted by 4 pixels
    with respect to original image).

**Result**

![](drop_002.png)

#### <a name="cborder"></a> How do I make border color depend on counter color

**Goal:** to derive border colour colour of the counter.

Relevant reading:

-   ImageMagick docs, [*identify*](https://imagemagick.org/script/identify.php)
-   ImageMagick docs, [*format
    identify*](https://imagemagick.org/script/escape.php)
-   StackOverflow, [*How to set commands output as a variable in a batch
    file*](https://stackoverflow.com/questions/6359820/how-to-set-commands-output-as-a-variable-in-a-batch-file)
-   ImageMagick docs, [*Extracting Image Colors*](https://www.imagemagick.org/Usage/quantize/#extract)
-   SS64: Command line reference, [IF: Conditionally perform a
    command*](https://ss64.com/nt/if.html)

Consider counters of two sides in a game. You wish to automate process
of [adding border](#border) but you want each side to have border of
different colour.

With ImageMagick it is possible to read colour value from specific
pixel, modify it and use it as border colour. This method, however,
rarely yields eye-pleasing results (i.e. it is not easy to come up with
such a colour modification that would, first, give appealing outcome,
two, give appealing outcome for all counter types); for a better effect
it\'s advised you use Windows script to set border colour based on a
pixel value read from input image.

We will use these two files as input:

![](cborder_001.png)

We want grey counter to have black border and dark red for the other.

We will use this script:

    FOR %%G IN (input\*.png) DO (

            FOR /F "tokens=* USEBACKQ" %%F IN (`magick %%G ^
    -crop 1x1+1+1 -format "%%[fx:int(255*r+.5)],%%[fx:int(255*g+.5)],%%[fx:int(255*b+.5)]" info:`) DO (
                    SET pcolor=%%F
            )

            IF "!pcolor!"=="161,162,172" SET bcolor=#000000
            IF "!pcolor!"=="181,161,143" SET bcolor=#B14246
            
            magick %%G -bordercolor !bcolor! -border 4 output\%%~nGOutput.png

    )

**Explanation**

-   `FOR %%G IN (input\*.png) DO ( ... )` iterates through all PNGs in
    `input` folder;
-   `` FOR /F "tokens=* USEBACKQ" %%F IN (`command`) DO ( ... ) `` is
    Windows way of assigning command output to a variable (`%F` in our
    case).
-   `magick %%G -crop 1x1+1+1 -format
    "%%[fx:int(255*r+.5)],%%[fx:int(255*g+.5)],%%[fx:int(255*b+.5)]"
    info:` is a convoluted way of reading pixel located at coordinates
    of (1,1) and writing its RGB components to the output. It ensures
    that ImageMagick will not output name of the colour, should it be
    one of [hundreds](https://imagemagick.org/script/color.php) that
    ImageMagick knows;
-   `SET pcolor=%%F` assigns command output to variable `pcolor` (as in
    "pixel color");
-   `IF "!pcolor!"=="161,162,172" SET bcolor=#000000` sets variable
    `bcolor` (as in "border color") to value `#000000` if RGB
    components of read pixel colour are equal to specified value. To
    ensure proper parsing, Windows requires string variables and values
    to be enclosed in quotes;
-   `magick %%G -bordercolor !bcolor! -border 4 output\%%~nG-border.png`
    adds 4 pixel border of colour `bcolor` to input image. In Windows,
    to use variable with assigned value you enclose it in `!`.

![](cborder_002.jpeg)

**Result**

![](cborder_003.png)

#### <a name="rounded"></a> How do I round counter corners 

**Goal:** to round corners of a counter.

Relevant reading:

-   ImageMagick docs, [*Duff-Porter Alpha Composition
    Methods*](https://imagemagick.org/Usage/compose/#duff-porter)
-   ImageMagick docs, [*Primitive Draw
    Commands*](https://imagemagick.org/Usage/draw/#primitives)
-   ImageMagick docs, [*FX special effects image operator*](https://imagemagick.org/script/fx.php)

The concept is to draw a square with rounded corners and logically
multiply it against source counter:

    magick <input-file> +antialias ^
    ( +clone -size %[fx:w]x%[fx:h] canvas:transparent  -delete -2 -fill red -draw "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] <radius,radius>" ) ^
    +swap -gravity center -compose SrcIn -composite <output-file>

**Example**

    magick input\input.png +antialias ^
    ( +clone -size %[fx:w]x%[fx:h] canvas:transparent -delete -2 -fill red -draw "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] 16,16" ) ^
    +swap -gravity center -compose In -composite output\output.png

![](rounded_001.png)

**Explanation:**

-   `+antialias` switches off antialiasing; should you leave it on,
    the rounded corners will be softened, giving somewhat blurry
    look. Some of you may like, some of you (myself included) not. I
    prefer it without antialiasing;
-   `+clone` clones `input-file` and adds it to image sequence;

![](rounded_002.png)

-   `-size %%[fx:w]x%%[fx:h]` sets dimensions of a to-be-created new
    image to be exactly the same as the `input-file`. `%[fx:w]` is
    ImageMagick idiom returning image width, `%[fx:h]` returns image
    height;
-   `canvas:transparent` creates new image out of thin air with
    transparent background;

![](rounded_003.png)

-   `-delete -2` deletes last but one image from the sequence (this
    happens to be the one that we `+clone`d, as per second bullet
    above;

![](rounded_004.png)

-   `-fill red` sets the foreground colour to red. In this case it is
    entirely unimportant as our newly created image would be,
    eventually, places beneath the actual counter, so the colour could
    not be seen anyway;
-   `-draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] <radius,radius>"`
    draws rounded rectangle filling entirely the whole drawing area
    (from 0 to `width-1` horizontally and 0 to `height-1` vertically);
    `<radius, radius>` sets the curvature of the corners;

![](rounded_005.png)

-   `+swap` swaps two images in the sequence; in our case we just swap
    the background with the foreground. Red rounded rectangle becomes
    first image in the queue with `input-file` taking its place;

![](rounded_006.png)

![](rounded_007.png)

-   `-gravity center` aligns centres of the images;
-   `-compose SrcIn` specifies method of composing two overlaying
    images;
-   `-composite` overlaps one image over another using `SrcIn` as
    method for filtering overlapping pixels.

![](rounded_008.png)

Alternatively: 
[https://www.imagemagick.org/Usage/thumbnails/#rounded](https://www.imagemagick.org/Usage/thumbnails/#rounded)

#### <a name="trim"></a> How do I trim counter edges

**Goal**: to trim counter edges.

Relevant reading:

-   ImageMagick docs, [*Crop relative to
    Gravity*](https://www.imagemagick.org/Usage/crop/#crop_gravity)
-   ImageMagick docs, [*Shave, removing edges from an
    image*](https://www.imagemagick.org/Usage/crop/#shave)

**Approach A**

If all images have same width and heigth it is perfectly OK to "trim
from outside" i.e. instruct ImageMagick to remove border of specific
width:

          magick <input-file> -shave WxH <output-file>

**Approach B**

If images don't have same width and height (i.e. you have
[auto-cropped counters from counter sheets](#crop)), trimming them
from outside could possible result with non-uniform-sized output. In
such case it is better to \"crop from inside\":

          magick <input-file> -gravity center -crop WxH+0+0 <output-file>

### <a name="pretty"></a> Make them pretty!

-   [Bevel](#bevel)
-   [clip](#rounded), and
-   [add outline](#outline) to your counters

for hellishly handsome results:

    magick input\input.png -raise 4 +antialias ^
    ( +clone -size %%[fx:w]x%%[fx:h] ^
    canvas:transparent -delete -2 -fill red ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 8,8" ) ^
    +swap -gravity center -compose In -composite ^
    ( +clone -size %%[fx:w+6]x%%[fx:h+6] ^
    canvas:transparent -delete -2 -fill black ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 10,10" ) ^
    +swap -gravity center -compose Over -composite output\output.png

**Result**

![](pretty_001.png)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf).

Date this document was created: 2020-08-28

Date this document was last modified: 2020-09-09

