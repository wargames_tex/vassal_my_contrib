## Automation of Cropping

Wargame counters in games usually come in sheets, with each sheet
containing several dozens of counters. To create a module--among other
things--one needs to cut these into individual counters. This can
be--and in fact is--tedious, boring and time consuming to the point of
discouraging potential module developers from creating modules at all.

This article addresses, at least to some extent, this issue, providing
you with tools and knowledge required to let you cut the counter
sheets in 5% time you would spend otherwise.

Consider counter sheet from Ardennes '44 (following images are used
only as samples; in real life situation you would use original images,
not scans, for module implementation; for educational purposes these
are more than enough, though):

Front:

![](crop_001.jpeg)

Back:

![](crop_002.jpeg)

Open these in your favorite graphic editor and cut top left stripe
from the front sheet and corresponding stripe from the back sheet
(which will be top right stripe, mind you):

Front (`front-stripe.jpg`)

![](crop_003.png)

Back: (`back-stripe.jpg`)

![](crop_004.png)

As there are 9 counters in one row, width of stripe in pixel should be
divisible by 9 (549px in my case) to ensure all the counters have same
width. Counters of non-uniform width is nothing [ImageMagick can't
handle](trim.md) of course, but for purpose of this tutorial let's
assume we want them to be equal.

With your favorite text editor create a Windows batch file containing
following commands:

    magick front-tripe.png -crop 9x2@ output\front-^%%02d.png
    magick back-stripe.png -flop -crop 9x2@ -flop output\back-^%%02d.png

Run it to cut stripes into individual counters:

![](crop_005.png)

![](crop_006.png)

Note: `^%%` is a Windows idiom that within `BAT` resolves to `%`. `%`
has a special meaning in Windows batch programming. In Linux
environments these commands would be a bit different.

Notice--and this is of crucial importance--that both front and back of
the same counter have a common suffix:

![](crop_007.png)
