# Counter image manipulation ImageMagick on Windows

This site is dedicated to automation of otherwise tedious process of
creating VASSAL modules. It covers very esoteric areas of IT expertise
such as:

-   automation of cutting counter sheets into individual pieces;
-   prettifying counters;

## Assumptions 

- I assume use of Windows operating system;
- I use – in great quantities – ImageMagick which is command line
  image manipulating computer program. It is particularly useful for
  batch manipulation of images; 
- I assume readers' familiarity with batch programming (creating `.BAT`
  scripts). 

## Start here


-   [Automation of Cropping](crop.md)
-   [Batch processing files in Windows](windows.md)

## Using ImageMagick to prettify counters

### Intermediate steps

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

#### Make them pretty!

-   [Make your counters look pretty (A)](pretty.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf).

Date this document was created: 2020-08-28

Date this document was last modified: 2020-09-09

