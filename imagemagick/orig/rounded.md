## How do I round counter corners

**Goal:** to round corners of a counter.

Relevant reading:

-   ImageMagick docs, [*Duff-Porter Alpha Composition
    Methods*](https://imagemagick.org/Usage/compose/#duff-porter)
-   ImageMagick docs, [*Primitive Draw
    Commands*](https://imagemagick.org/Usage/draw/#primitives)
-   ImageMagick docs, [*FX special effects image operator*]
    (https://imagemagick.org/script/fx.php)

The concept is to draw a square with rounded corners and logically
multiply it against source counter:

    magick <input-file> +antialias ^
    ( +clone -size %[fx:w]x%[fx:h] canvas:transparent  -delete -2 -fill red -draw "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] <radius,radius>" ) ^
    +swap -gravity center -compose SrcIn -composite <output-file>

**Example**

    magick input\input.png +antialias ^
    ( +clone -size %[fx:w]x%[fx:h] canvas:transparent -delete -2 -fill red -draw "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] 16,16" ) ^
    +swap -gravity center -compose In -composite output\output.png

![](rounded_001.png)

**Explanation:**

-   `+antialias` switches off antialiasing; should you leave it on,
    the rounded corners will be softened, giving somewhat blurry
    look. Some of you may like, some of you (myself included) not. I
    prefer it without antialiasing;
-   `+clone` clones `input-file` and adds it to image sequence;

![](rounded_002.png)

-   `-size %%[fx:w]x%%[fx:h]` sets dimensions of a to-be-created new
    image to be exactly the same as the `input-file`. `%[fx:w]` is
    ImageMagick idiom returning image width, `%[fx:h]` returns image
    height;
-   `canvas:transparent` creates new image out of thin air with
    transparent background;

![](rounded_003.png)

-   `-delete -2` deletes last but one image from the sequence (this
    happens to be the one that we `+clone`d, as per second bullet
    above;

![](rounded_004.png)

-   `-fill red` sets the foreground colour to red. In this case it is
    entirely unimportant as our newly created image would be,
    eventually, places beneath the actual counter, so the colour could
    not be seen anyway;
-   `-draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] <radius,radius>"`
    draws rounded rectangle filling entirely the whole drawing area
    (from 0 to `width-1` horizontally and 0 to `height-1` vertically);
    `<radius, radius>` sets the curvature of the corners;

![](rounded_005.png)

-   `+swap` swaps two images in the sequence; in our case we just swap
    the background with the foreground. Red rounded rectangle becomes
    first image in the queue with `input-file` taking its place;

![](rounded_006.png)

![](rounded_007.png)

-   `-gravity center` aligns centres of the images;
-   `-compose SrcIn` specifies method of composing two overlaying
    images;
-   `-composite` overlaps one image over another using `SrcIn` as
    method for filtering overlapping pixels.

![](rounded_008.png)

Alternatively: 
[https://www.imagemagick.org/Usage/thumbnails/#rounded](https://www.imagemagick.org/Usage/thumbnails/#rounded)

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)

Date this document was created: 2020-08-31

Date this document was last modified: 2020-08-31
