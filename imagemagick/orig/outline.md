## How do I add outline

**Goal:** to add outline around irregular ([rounded
counter?](rounded)) shape

Relevant reading:

-   ImageMagick docs, [*Difference Morphology
    Methods*](https://www.imagemagick.org/Usage/morphology/#difference).

**Approach A**

I admit I do not exactly know why it works. But it works so if that is
enough for you don't let your urge to fight ignorance prevent you from
using it.

    magick <input-file>^
    ( -clone 0 -fill <color> -colorize 100  )^
    ( -clone 0 -alpha extract -virtual-pixel black -morphology edgein octagon )^
    -compose over -composite <output-file>

**Example**

    magick input\rounder-corners.png^
    ( -clone 0 -fill #000000 -colorize 100  )^
    ( -clone 0 -alpha extract -virtual-pixel black -morphology edgein octagon )^
    -compose over -composite output\output.png

**Result**

![](outline_001.png)

`<color>` can be in fact any color so be creative:

![](outline_002.png)

**Approach B**

This time we create a rounded rectangle of dimensions slightly bigger
than the counter itself and then we overlay the bigger rectangle with
the counter.

    magick input\rounder-corners.png +antialias ^
    ( +clone -size %%[fx:w+6]x%%[fx:h+6] ^
    canvas:transparent -delete -2 -fill black ^
    -draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 18,18" ) ^
    +swap -gravity center -composite output\output.png

**Explanation**

-   `+antialias` switches off antialiasing; should you leave it on,
    the rounded corners will be softened, giving somewhat blurry
    look. Some of you may like, some of you (myself included) not. I
    prefer it without antialiasing;
-   `+clone` clones `input-file` and adds it to image sequence;
-   `-size %%[fx:w+6]x%%[fx:h+6]` sets the image size to be slightly
    (by 3 pixels per each side) bigger than cloned one;
-   `canvas:transparent` creates new image out of thin air with
    transparent background;
-   `-delete -2` deletes last but one image from the sequence (this
    happens to be the one that we `+clone`d, as per second bullet
    above;
-   `-fill black` sets fill colour to black. Black will be the colour
    of the outline; if you wish to use another colour, that\'s the
    place to change it;
-   `-draw "roundrectangle 0,0 %%[fx:w-1],%%[fx:h-1] 18,18"` draws
    rectangle with rounded corners. Two things worth emphasizing:
    `%[fx:w]`, which is ImageMagick idiom resolved to image width, now
    refers to new image that is 6 pixels wider and 6 pixels higher.
    Also, the radius that we use this time (`18`) is slightly larger
    of what was used in [tutorial on rounding corners](rounded.md).
    This is because we want the outer border be of slightly larger
    curvature so that outline is of uniform width;
-   `+swap` swaps two images in the sequence; in our case we just swap
    black rounded rectangle (which should be placed on the bottom)
    with rounded counter (which should go on top);
-   `-gravity center` aligns centres of the images;
-   `-composite` overlaps one image over another. With no method
    specified with `-compose` ImageMagick simply puts one image on
    another.

**Result**

![](outline_003.png)

If we didn't factor the large curvature of the external border, the
results would be somewhat less eye-pleasing:

![](outline_004.png)

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)

Date this document was created: 2020-08-31

Date this document was last modified: 2020-08-31
