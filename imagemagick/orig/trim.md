## How do I trim counter edges

**Goal**: to trim counter edges.

Relevant reading:

-   ImageMagick docs, [*Crop relative to Gravity*](https://www.imagemagick.org/Usage/crop/#crop_gravity)
-   ImageMagick docs, [*Shave, removing edges from an image*](https://www.imagemagick.org/Usage/crop/#shave)

**Approach A**

If all images have same width and heigth it is perfectly OK to "trim
from outside" i.e. instruct ImageMagick to remove border of specific
width:

          magick <input-file> -shave WxH <output-file>

**Approach B**

If images don't have same width and height (i.e. you have
[auto-cropped counters from counter sheets](crop.md)), trimming them
from outside could possible result with non-uniform-sized output. In
such case it is better to \"crop from inside\":

          magick <input-file> -gravity center -crop WxH+0+0 <output-file>

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I round counter corners](rounded.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)

Date this document was created: 2020-08-29

Date this document was last modified: 2020-09-02

