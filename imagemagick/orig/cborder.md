## How do I make border color depend on counter color

**Goal:** to derive border colour colour of the counter.

Relevant reading:

-   ImageMagick docs, [*identify*](https://imagemagick.org/script/identify.php)
-   ImageMagick docs, [*format
    identify*](https://imagemagick.org/script/escape.php)
-   StackOverflow, [*How to set commands output as a variable in a batch
    file*](https://stackoverflow.com/questions/6359820/how-to-set-commands-output-as-a-variable-in-a-batch-file)
-   ImageMagick docs, [*Extracting Image Colors*](https://www.imagemagick.org/Usage/quantize/#extract)
-   SS64: Command line reference, [IF: Conditionally perform a
    command*](https://ss64.com/nt/if.html)

Consider counters of two sides in a game. You wish to automate process
of [adding border](border.md) but you want each side to have border of
different colour.

With ImageMagick it is possible to read colour value from specific
pixel, modify it and use it as border colour. This method, however,
rarely yields eye-pleasing results (i.e. it is not easy to come up with
such a colour modification that would, first, give appealing outcome,
two, give appealing outcome for all counter types); for a better effect
it\'s advised you use Windows script to set border colour based on a
pixel value read from input image.

We will use these two files as input:

![](border_001.png)

We want grey counter to have black border and dark red for the other.

We will use this script:

    FOR %%G IN (input\*.png) DO (

            FOR /F "tokens=* USEBACKQ" %%F IN (`magick %%G ^
    -crop 1x1+1+1 -format "%%[fx:int(255*r+.5)],%%[fx:int(255*g+.5)],%%[fx:int(255*b+.5)]" info:`) DO (
                    SET pcolor=%%F
            )

            IF "!pcolor!"=="161,162,172" SET bcolor=#000000
            IF "!pcolor!"=="181,161,143" SET bcolor=#B14246
            
            magick %%G -bordercolor !bcolor! -border 4 output\%%~nGOutput.png

    )

**Explanation**

-   `FOR %%G IN (input\*.png) DO ( ... )` iterates through all PNGs in
    `input` folder;
-   `` FOR /F "tokens=* USEBACKQ" %%F IN (`command`) DO ( ... ) `` is
    Windows way of assigning command output to a variable (`%F` in our
    case).
-   `magick %%G -crop 1x1+1+1 -format
    "%%[fx:int(255*r+.5)],%%[fx:int(255*g+.5)],%%[fx:int(255*b+.5)]"
    info:` is a convoluted way of reading pixel located at coordinates
    of (1,1) and writing its RGB components to the output. It ensures
    that ImageMagick will not output name of the colour, should it be
    one of [hundreds](https://imagemagick.org/script/color.php) that
    ImageMagick knows;
-   `SET pcolor=%%F` assigns command output to variable `pcolor` (as in
    "pixel color");
-   `IF "!pcolor!"=="161,162,172" SET bcolor=#000000` sets variable
    `bcolor` (as in "border color") to value `#000000` if RGB
    components of read pixel colour are equal to specified value. To
    ensure proper parsing, Windows requires string variables and values
    to be enclosed in quotes;
-   `magick %%G -bordercolor !bcolor! -border 4 output\%%~nG-border.png`
    adds 4 pixel border of colour `bcolor` to input image. In Windows,
    to use variable with assigned value you enclose it in `!`.

![](border_002.jpeg)

**Result**

![](border_003.png)

### Related reading

-   [How do I add beveled border](bevel.md)
-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf).

Date this document was created: 2020-09-06

Date this document was last modified: 2020-09-08
