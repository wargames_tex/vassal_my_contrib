## How do I add beveled border

**Goal:** add a bevelled border to counter for a "button-like" look.

Relevant reading:

-   ImageMagick docs, ["Raise or Sunk Borders"](https://www.imagemagick.org/Usage/transform/#raise).

To add a bevelled border use `-raise` command:

          magick <input-file> -raise <width> <output-file>

**Example**

    magick input\input.png -raise 4 output\output.png

**Result**

![](bevel_001.png)

You may of course use `-raise` twice (or thrice, \...) for more
pronounced effect:

          magick input\input.png -raise 4 -raise 4 output\output.png

**Result**

![](bevel_002.png)

Combination of [plain](border.md) and bevelled border results with a
look that could probably work for most VASSAL modules if you are not
for something more fancy:

          magick input\input.png -raise 4 -bordercolor #000000 -border 2 output\output.png

**Result**

![](bevel_003.png)

### Related reading

-   [How do I add border](border.md)
-   [How do I add outline](outline.md)
-   [How do I add dropshadow](drop.md)
-   [How do I make border color depend on counter color](cborder.md)
-   [How do I round counter corners](rounded.md)
-   [How do I trim counter edges](trim.md)

------------------------------------------------------------------------

This site is maintained by
[grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf).

Date this document was created: 2020-08-29

Date this document was last modified: 2020-08-30

