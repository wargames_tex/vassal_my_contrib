# Counter image manipulations 

The script [`piece_manip.sh`](https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/imagemagick/piece_manip.sh) (Right-click and select _Save link as ..._)  allows for a number of manipulations of counter images.  The syntax is in general 

    ./piece_manip.sh [OPTIONS] INPUT ... 
    
where `OPTIONS` specify which operations to do, and `INPUT ...` is a number of input files.  Try also 

    ./piece_manip.sh --long-help 
    
for information on operations.   If no output file is specified via option `-o` or `--output`, then for input file `INPUT.EXT`, the default output file name is `INPUT_out.png`. 

The script uses [ImageMagick](https://imagemagick.org/), and is inspired by [grouchysmurf](https://forum.vassalengine.org/u/grouchysmurf)'s, now defunct, web-page [`vassal.neocities.org`](https://web.archive.org/web/20221130130010/https://vassal.neocities.org/index.html) (Wayback machine). Credit to that user for finding various useful operations.

This script is not meant to be omnipotent.  Many more possible and sensible operations could be defined, and much more fancy stuff can be done with [ImageMagick](https://imagemagick.org/).  Please refer to the document of ImageMagick, not least the extensive list of [examples](https://usage.imagemagick.org/).

## Overview 

| Original | [`--bevel`](#bevel) | [`--border`](#border) | [`--color`](#color) | [`--crop`](#crop) | [`--drop`](#drop) |  [`--outline`](#outline) | [`--rounded`](#rounded) | [`--shade`](#shade) | [`--shave`](#shave) | [Combined](#fancy) |
|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| ![](counter1.png)| ![](counter1_bevel.png)| ![](counter1_border.png)| ![](counter1_color.png)| ![](counter1_crop.png)| ![](counter1_drop.png)|  ![](counter1_outline.png)| ![](counter1_rounded.png)| ![](counter1_shade.png)| ![](counter1_shave.png)| ![](counter1_fancy.png)|
| ![](counter2.png)| ![](counter2_bevel.png)| ![](counter2_border.png)| ![](counter2_color.png)| ![](counter2_crop.png)| ![](counter2_drop.png)| ![](counter2_outline.png)| ![](counter2_rounded.png)| ![](counter2_shade.png)| ![](counter2_shave.png)| ![](counter2_fancy.png)| 

## Some common notes 

### Colours 

Any colour that [Imagemagicks knows about](https://imagemagick.org/script/color.php) can be used, or alternatively the colour can be given using HTML colour codes (`#RRGGBB`, where `RR`, `GG`, `BB`, are hexadecimal numbers between `0x00` and `0xFF`, and corresponds to the red, green, and blue channels, respectively).

### Sizes 

Sizes are _always_ given in image pixels.

### Copyright 

Typically, the publisher of the game holds the copyright on the materials of the game.  While copyright [can most likely _not_ be claimed over game mechanics and design](https://boardgamegeek.com/thread/493249/), it _can_ be enforced on _artistic_ and _original_ work.  Graphics, such as counters, maps, cards, etc. definitely fall in that category. 

Thus, if you modify or redistribute such materials - counters, maps, cards, etc. - you should make sure that the copyright holders have given you license to do so, and you should explicitly provide that license.  Otherwise, you are most likely liable for copyright infringement. 

## Crop images from counter sheet 


    ./piece_manip.sh --extract N M [FLIP] INPUT ...
    
where `INPUT` is `N` columns and `M` rows of pieces.  If `FLIP` is `true`, `yes`, `1`, `flip`, `flop`, or `back`, then the order of pieces are reversed.  This is useful when extracting images from the "back" counter sheet so as to reproduce the order of the images used on the front sheet. 

Output file names are 

    INPUT_NUM.png
    
where `NUM` is a serial number, starting at zero, zero-padded to 3 digits - e.g., if the input file name is `front_block.png`, then the 6th extracted image will be named `front_block_005.png`.

### Examples 

    ./piece_manip.sh --extract 9 2 front_block.png 
    ./piece_manip.sh --extract 9 2 1 back_block.png 
    
Input: 

Given the counter sheets 

| Front                 | Back                 |
|-----------------------|----------------------|
| ![](front_sheet.jpeg) | ![](back_sheet.jpeg) |                  

cut out the individual blocks, using for example [Gimp](https://gimp.org), as shown below

|                   |                      |
|-------------------|----------------------|
| `front_block.png` | ![](front_block.png) |
| `back_block.png`  | ![](back_block.png)  |

Output: 

|  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|
| ![](front_block_000.png) |  ![](front_block_001.png) |  ![](front_block_002.png) |  ![](front_block_003.png) |  ![](front_block_004.png) |  ![](front_block_005.png) |  ![](front_block_006.png) |  ![](front_block_007.png) |  ![](front_block_008.png) |
| ![](front_block_009.png) |  ![](front_block_010.png) |  ![](front_block_011.png) |  ![](front_block_012.png) |  ![](front_block_013.png) |  ![](front_block_014.png) |  ![](front_block_015.png) |  ![](front_block_016.png) |  ![](front_block_017.png) |
| ![](back_block_000.png) |  ![](back_block_001.png) |  ![](back_block_002.png) |  ![](back_block_003.png) |  ![](back_block_004.png) |  ![](back_block_005.png) |  ![](back_block_006.png) |  ![](back_block_007.png) |  ![](back_block_008.png) |
| ![](back_block_009.png) |  ![](back_block_010.png) |  ![](back_block_011.png) |  ![](back_block_012.png) |  ![](back_block_013.png) |  ![](back_block_014.png) |  ![](back_block_015.png) |  ![](back_block_016.png) |  ![](back_block_017.png) |

## Bevel <a name="bevel"></a> 

    ./piece_manip.sh --bevel [SIZE] INPUT ...
    
Add a bevel ("button" effect) of size SIZE pixel to images.  If `SIZE` is not specified it defaults to `4`

### Example 

    ./piece_manip.sh --bevel 4 counter1.png -o counter1_bevel.png
    ./piece_manip.sh --bevel 4 counter2.png -o counter2_bevel.png

| Input             | Output                  |
|-------------------|-------------------------|
| ![](counter1.png) | ![](counter1_bevel.png) |
| ![](counter2.png) | ![](counter2_bevel.png) |

## Border <a name="border"></a> 

    ./piece_manip.sh --border [SIZE [COLOUR]] INPUT ...
    
Add a `COLOUR`-coloured border of size SIZE pixel to images.  If `SIZE` is not specified it defaults to `4`. If `COLOUR` is not specified, it defaults to `black`.


### Example 

    ./piece_manip.sh --border 1 red counter1.png -o counter1_border.png
    ./piece_manip.sh --border 1 red counter2.png -o counter2_border.png

| Input             | Output                   |
|-------------------|--------------------------|
| ![](counter1.png) | ![](counter1_border.png) |
| ![](counter2.png) | ![](counter2_border.png) |

## Color <a name="color"></a> 

    ./piece_manip.sh --color [N [DITHER]] INPUT ...

Reduce the number of colours used by the image to `N` (defaults to 16).  If `DITHER` is `no`, `false`, `no-dither`, `0`, or `off`, then dithering is turned off (default is on).  The purpose of this operation is to reduce the image size.  For many images, the size can be reduced to about 25% of the original size. 

### Example 

    ./piece_manip.sh --color 5 5 -o counter1_color.png
    ./piece_manip.sh --color 5 5 -o counter2_color.png

| Input             | Size   | Output                  | Size  |
|-------------------|--------|-------------------------|-------|
| ![](counter1.png) | 17.8kb | ![](counter1_color.png) | 3.9kb |
| ![](counter2.png) | 20.6kb | ![](counter2_color.png) | 4.2kb |

## Crop <a name="crop"></a> 

    ./piece_manip.sh --crop GEOM INPUT ...

Crop piece to geometry `GEOM`.  `GEOM` has the format 

    WIDTHxHEIGHT[+/-X+/-Y]
    
where `WIDTH` and `HEIGHT` are the new width and height, respectively, and `X` and `Y` are the horizontal and vertical offsets, respectively, with respect to the centre of the image. 

### Example 

    ./piece_manip.sh --crop 100x100 -o counter1_crop.png
    ./piece_manip.sh --crop 100x100 -o counter2_crop.png

| Input             | Output                    |
|-------------------|---------------------------|
| ![](counter1.png) | ![](counter1_crop.png) |
| ![](counter2.png) | ![](counter2_crop.png) |

## Drop <a name="drop"></a> 

    ./piece_manip.sh --drop [SIZE [COLOUR [FADE [OPACITY]]]] INPUT ...
    
Add a `COLOUR`-coloured drop-shadow to images.  If `COLOUR` is not specified, it defaults to `gray7`.  If `FADE` is positive, then the shadow will fade of that many pixels.  `OPACITY` sets the opacity of the start of the drop-shadow.   `SIZE` defaults to `4`, `FADE` to 0, and `OPACITY` to 60. 

### Example 

    ./piece_manip.sh --drop 2 "#11111e" 4 counter1.png -o counter1_drop.png
    ./piece_manip.sh --drop 2 "#11111e" 4 counter2.png -o counter2_drop.png

| Input             | Output                 |
|-------------------|------------------------|
| ![](counter1.png) | ![](counter1_drop.png) |
| ![](counter2.png) | ![](counter2_drop.png) |

## Outline <a name="outline"></a> 

    ./piece_manip.sh --outline [COLOUR] INPUT ...
    
Add a `COLOUR`-coloured outline to images.  If `COLOUR` is not specified, it defaults to `black`.

### Example 

    ./piece_manip.sh --outline blue counter1.png -o counter1_outline.png
    ./piece_manip.sh --outline blue counter2.png -o counter2_outline.png

| Input             | Output                    |
|-------------------|---------------------------|
| ![](counter1.png) | ![](counter1_outline.png) |
| ![](counter2.png) | ![](counter2_outline.png) |

## Rounded <a name="rounded"></a> 

    ./piece_manip.sh --rounded [SIZE] INPUT ...

Make the corners of the images rounded.  The radius is set by SIZE. SIZE defaults to `16`.

### Example 

    ./piece_manip.sh --rounded 16 -o counter1_rounded.png
    ./piece_manip.sh --rounded 16 -o counter2_rounded.png

| Input             | Output                    |
|-------------------|---------------------------|
| ![](counter1.png) | ![](counter1_rounded.png) |
| ![](counter2.png) | ![](counter2_rounded.png) |

## Shade <a name="shade"></a>

    ./piece_manip.sh --shade [SIZE [ANGLE [ELEVATION]]] INPUT ...

Add a smooth bevel to the image of size `SIZE`. The shade is drawn as if the light-source is at the angle `ANGLE` and elevation `ELEVATION` (both in degrees, and defaults to 135 and 30, respectively).  Note that `ANGLE` is in the range `0` to `360` and `ELEVATION` in the range `0` to `90`, though the extremes should be avoided. 

### Example 

    ./piece_manip.sh --shade 5 -o counter1_shade.png
    ./piece_manip.sh --shade 5 -o counter2_shade.png

| Input             | Output                  |
|-------------------|-------------------------|
| ![](counter1.png) | ![](counter1_shade.png) |
| ![](counter2.png) | ![](counter2_shade.png) |

## Shave <a name="shave"></a> 

    ./piece_manip.sh --shave X Y INPUT ...

Shave off from left and right, top and bottom of the image.  `X` is the number of pixels to remove both from the left _and_ right edges, while `Y` is the number of pixels to remove from the top _and_ bottom edges.

### Example 

    ./piece_manip.sh --shave 5 5 -o counter1_shave.png
    ./piece_manip.sh --shave 5 5 -o counter2_shave.png

| Input             | Output                  |
|-------------------|-------------------------|
| ![](counter1.png) | ![](counter1_shave.png) |
| ![](counter2.png) | ![](counter2_shave.png) |

## Combining manipulations <a name="fancy"></a>

The above manipulations can be combined.  That is, one can specify multiple operations for a single image file.  The will then be applied in the order given.  Note that the order matters. 

### Example 

    ./piece_manip.sh         \
        --bevel 8            \
		--rounded 16 	     \
		--outline black	     \
		--drop 2 "#11111e" 4 \
        --color 16           \
		-o counter1_fancy.png counter1.png
    ./piece_manip.sh         \
        --bevel 8            \
		--rounded 16 	     \
		--outline black	     \
		--drop 2 "#11111e" 4 \
        --color 16           \
		-o counter2_fancy.png counter2.png


| Input             | Output                  |
|-------------------|-------------------------|
| ![](counter1.png) | ![](counter1_fancy.png) |
| ![](counter2.png) | ![](counter2_fancy.png) |


Note, the option `--color 16` reduces the final image size to just over 25% if that option had not been specified, and a roughly 40% of the original image.  If instead `--color 32` had been specified, then the final size would be roughly 50% or the original image size. 

## License 

The script `piece_manip.sh` is licensed under the GNU General Public License version 3, or later.  See also [`LICENSE`](LICENSE). 

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode 0) -->
<!-- eval: (visual-line-mode) -->
<!-- eval: (visual-fill-column-mode) -->
<!-- eval: (adaptive-wrap-prefix-mode) -->
<!-- End: -->
