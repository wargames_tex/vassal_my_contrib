#!/usr/bin/env python

# --------------------------------------------------------------------
def read_in(file):
    return file.readlines()

# --------------------------------------------------------------------
def embed(lines,outfile):
    from re import match, search, sub
    from pathlib import Path
    from base64 import b64encode

    out = []
    for line in lines:
        line = line[:-1]
        pos  = 0
        while True:
            matches = search(r'!\[\]\(([^\)]+)\)',
                             line[pos:])
            if not matches:
                break

            imgfn = Path(matches[1])
            

            with imgfn.open('rb') as imgfp:
                data = imgfp.read()
                
            print(f'Embedding {imgfn}')

            span = matches.span()
            #print(matches)
            #print(span)
            #print('Before:',line[:pos+span[0]])
            #print('After:',line[pos+span[1]:])
            b64  =  b64encode(data).decode('utf8')
            new  =  f'![](data:image/{imgfn.suffix};base64,{b64})'
            line =  line[:pos+span[0]] + new + line[pos+span[1]:]
            pos  += span[0]+len(new)
            #print(pos)
            #print(line[pos:])

            # line = sub(r'!\[\]\([^)]+\)',
            #            f'![](data:image/{imgfn.suffix};base64,{b64})',
            #            line)
            # print(line)
            # line = f'![](data:image/{imgfn.suffix};base64,{b64})'

        out.append(line)


    outfile.write('\n'.join(out))

# --------------------------------------------------------------------
def remove_output(out):
    from pathlib import Path

    p = Path(out.name)
    p.unlink(missing_ok=True)
    
# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pathlib import Path
    
    ap = ArgumentParser(description='Embed images')
    ap.add_argument('input',help='Input file',type=FileType('r'))
    ap.add_argument('output',help='Output file',type=FileType('w'),
                    nargs='?', default=None)

    args = ap.parse_args()

    if not args.output:
        inp = Path(args.input.name)
        oup = Path(inp.stem + '_embedded').with_suffix('.md')
        args.output = oup.open('w')


    try:
        embed(read_in(args.input),args.output)

        args.output.close()
    except Exception as e:
        remove_output(args.output)
        raise
    
 
# --------------------------------------------------------------------
#
# EOF
#

