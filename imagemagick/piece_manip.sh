#!/bin/bash

# --------------------------------------------------------------------
usage() {
    l=${1-0}
    cat <<-EOF
	Usage: $0 [OPTIONS] INPUT ...
        
	Options:
	  -h,--help		Usage help
	  -H,--long-help	Show full help
	  -o,--output NAME	Output file name
	  -v,--verbose		Be verbose
	  --<operation> ...

	If no output file is specified via option -o or --output, then
	for input file INPUT.EXT, the default output file name is
	INPUT_out.png.

	EOF

    if test $l -gt 0 ; then
        return
    fi
    
    cat <<-EOF
	For more on possible operations, try

	  $0 --long-help

	EOF
}

# --------------------------------------------------------------------
help() {
    usage 1
    cat <<-EOF
	Operations:
	
	  --extract N M [FLIP]
	
	        Create images from (part) of a counter sheet.  The
	        input image is assumed to be an N columns and M rows
	        counter sheet with no spacing between counters.
	
	        If FLIP is "true", "yes", or "1", then flip order of
	        extraction.  This is useful when extracting the "back"
	        side of counters to keep the same numbering as the
	        front side.
	
	        For input file INPUT, the images are written to
	        INPUT_NUM.png, where NUM is a 3-digit, zero-padded,
	        serial number (starting at 0).
	
	  --bevel [SIZE]
	
	        Add bevel to images.  The bevel is SIZE large.	SIZE
	        defaults to 4.	Note, the bevel is really
	        semi-transparent.

          --shade [SIZE [ANGLE [ELEVATION]]

	        Creates a shade on the piece.  The size of the shade
	        is controlled by SIZE (default 4) while the angle and
	        elevation of the light source is set by ANGLE and
	        ELEVATION, respectively, both in degrees (defautls to
	        135 and 30).
 
	  --border [SIZE [COLOUR]]
	
	        Add a border to images.	 The border is SIZE pixels
	        large, and has COLOUR.	If SIZE is not specified, then
	        it will be set to 4.  COLOUR defaults to black.	 Use
	        --outline below for non-rectangular or rounded corners
	        images.
	
	  --outline [COLOUR]
	
	        Add outline to images.	COLOUR specifies the colour
	        (defaults to black).  Use this, rather than --border
	        if the images are not rectangular or have rounded
	        corners.
	
	  --drop [SIZE [COLOUR [FADE [OPACITY]]]]

	        Add drop shadow images.	 The drop shadow will be SIZE
	        pixels big, and be COLOUR base colour.	If FADE is
	        given and not zero, then the shadow will fade over
	        that many pixels.  If OPACITY is given, then the
	        drop-shadow will have that opacity (0-100).
	
	        SIZE defaults to 4, COLOUR to grey7 (0x121212), FADE
	        to 0, and OPACITY to 60.
	
	        Note that the image grows by FADE top, bottom, left,
	        and right, and by SIZE to the bottom and right, only.
	
	  --rounced [SIZE]
	
	        Round the corners of the images.  The radius of the
	        rounding will be SIZE.	SIZE defaults to 16.
	
	  --crop WIDTH HEIGHT [X [Y]]

	        Crop image within the geometry.  WIDTH is the new
	        width, HEIGHT is the new HEIGHT, and +/-X and +/- Y
	        are the offset of that rectangle with respect to the
	        centre of the image.
	
	  --shave X [Y]
	
	        Remove pixels horizontally or vertically.  X
	        determines how many pixels are removed on the left and
	        right side of the image.  Y determines how many pixels
	        are removed at the top and bottom.  Both X and Y
	        defaults to 0.

	  --color [N [DITHER]] 

	        Reduce the number of colours used by the image to N
	        colours. N defaults to 16. If DITHER is 'yes', 'on',
	        '1', 'dither', (default) then the image will be
	        dithered when reducing number of colours.  If DITHER
	        is 'no', 'off', 'false', 'no-dither' or '0', then
	        dithering will be turned off.

	EOF
}

# --------------------------------------------------------------------
more()
{
    case $1 in
        -*) return 1 ;;
        *)
            if test -f $1 ; then
                return 1
            fi
            ;;
    esac
    return 0;
}

# --------------------------------------------------------------------
declare -a opers
inputs=""
verb=0
while test $# -gt 0 ; do
    case $1 in
        -h|--help)      usage ; exit 0 ;;
        -H|--long-help) help  ; exit 0 ;;
        -o|--output)    output=$2 ; shift ;;
        -v|--verbose)   verb=1 ;;
        -q|--quiet)     verb=0 ;;
        --extract)
            opers+=("extract") 
            extract_ncol=$2        ; shift
            extract_nrow=$2        ; shift
            extract_flip=0
            if more $2 ; then
                case $2 in
                    -*) ;;
                    1|flop|true|yes|flip|back) extract_flip=1  ;;
                    0|false|no|front)          extract_flip=0  ;;
                esac
                shift
            fi
            ;;
        --bevel)
            opers+=("bevel")
            if more $2 ; then bevel_size=$2   ; shift  ; fi
            ;;
        --shade)
            opers+=("shade")
            if more $2 ; then shade_size=$2   ; shift  ; fi
            if more $2 ; then shade_angle=$2   ; shift  ; fi
            if more $2 ; then shade_ela=$2   ; shift  ; fi
            ;;
         --border)
            opers+=("border")
            if more $2 ; then border_size=$2  ; shift  ; fi
            if more $2 ; then border_color=$2 ; shift  ; fi
            ;;
        --outline)
            opers+=("outline")
            if more $2 ; then outline_color=$2 ; shift  ; fi
            ;;
        --drop)
            opers+=("drop")
            if more $2 ; then drop_size=$2    ; shift  ; fi
            if more $2 ; then drop_color=$2   ; shift  ; fi
            if more $2 ; then drop_fade=$2    ; shift  ; fi
            if more $2 ; then drop_opacity=$2 ; shift  ; fi
            ;;
        --rounded)
            opers+=("rounded")
            if more $2 ; then rounded_size=$2 ; shift  ; fi
            ;;
        --crop)
            opers+=("crop") 
            crop_w=$2 ; shift
            crop_h=$2 ; shift
            if more $2 ; then crop_x=$2 ; shift  ; fi
            if more $2 ; then crop_y=$2 ; shift  ; fi
            ;;
        --shave)
            opers+=("shave")
            shave_x=$2 ; shift
            if more $2 ; then shave_y=$2 ; shift  ; fi            
            ;;
        --color|--colour)
            opers+=("color")
            color_n=$2 ; shift
            if more $2 ; then
                case $2 in
                    -*) ;;
                    1|on|true|yes|dither)     color_dither=1  ;;
                    0|off|false|no|no-dither) color_dither=0  ;;
                esac
                shift
            fi
            ;;
        --)
            ;;
        -*)
            echo "Unknown option: $1" > /dev/stderr
            exit
            ;;
        *)
            inputs="$inputs $1"
            ;;
    esac
    shift 
done

# --------------------------------------------------------------------
msg()
{
    if test $verb -lt 1 ; then
        return 0
    fi
    echo "${@}"
}

# --------------------------------------------------------------------
msg "Operations are: '${opers}'"
msg "Input files are: '${inputs}'"


# --------------------------------------------------------------------
run()
{
    msg " " magick "${@}"
    magick "${@}"
}

# --------------------------------------------------------------------
show()
{
    if test $verb -lt 1 ; then
        return
    fi
    if test x$outfiles != "x" ; then
        echo " Output files are "
        for file in $outfiles ; do
            echo "  $file"
        done
        return
    fi

    echo " Output file is $output"
}
            
# ====================================================================
#
# The arguments array we will build up
#
declare -a args

# --------------------------------------------------------------------
extract()
{
    echo "Extract with all options: ${@}"
    ncol=$1       ; shift
    nrow=$1       ; shift
    flip=${1-0}   ; shift

    flop=
    if test $flip -gt 0 ; then
        flop="-flop"
    fi
    echo "Extract with options ncol=$ncol nrow=$nrow"
    
    args+=(${flop} "-crop" "${ncol}x${nrow}@" ${flop})
}

# --------------------------------------------------------------------
bevel()
{
    size=${1-4}

    args+=("-raise" "${size}")
}

# --------------------------------------------------------------------
border()
{
    size=${1-4} ; shift 
    col=${1-black}

    # sample_x=1
    # sample_y=1
    # sample=$(printf "#%02x%02x%02x" `magick $inp -crop ${sample_x}x${sample_y}+1+1 -format "%[fx:int(255*r+.5)] %[fx:int(255*g+.5)] %[fx:int(255*b+.5)]" info:`)

    args+=("-bordercolor" "${col}" "-border" "${size}x${size}")
}

# --------------------------------------------------------------------
outline()
{
    col=${1-black}

    # First clone image and make it entirely black
    # Then clone the starting image again,
    # - extract alpha channel and make grayscale mask
    # - set virtual (or surrounding pixel) to be black
    # - using an octagon mask, and edge-in morphology, the
    # - edge of the image is found, roughly 2 to 3 pixels bigger.
    # The images are finally composed down to a single image
    args+=("(" "-clone" "-1"    # Copy input
           "-fill" "${col}"     # Fill with color 
           "-colorize" "100"    # Full intensity
           ")"
           "(" "-clone" "-1"    # Copy input
           "-alpha" "extract"   # Find alpha and put in pixel
           "-virtual-pixel" "black" # Black is virtual
           "-morphology" "edgein" "octagon" # Use an octagon to find the edge
           ")"
           "-compose" "over"   # Source over dest"
           "-composite")
}    

# --------------------------------------------------------------------
dropshadow()
{
    siz=${1-4}     ; shift
    col=${1-grey7} ; shift
    blu=${1-0}     ; shift 
    opa=${1-60}    ;
    # Copy input image,
    # fill it with with color
    # Add shadow
    # Swap images,
    # make background invisible
    # Merge layers
    args+=("(" "+clone"
           "-background" "${col}"                   # Set background color
           "-shadow"  "${opa}x${blu}+${siz}+${siz}" # Make shadow
           ")"
           "+swap" # Swap source and dest 
           "-background" "none" # No background
           "-layers" "merge" # Merge down, putting source on top of dest
           )
}

# --------------------------------------------------------------------
rounded()
{
    siz=${1-16}    ; shift

    # +antialias ^
    # - Set up mask 
    #   - Clone input,
    #   - make the clone (needed)
    #   - set size to be the same of image, and make transparent canvas image 
    #     and make it transparent
    #   - Delete the second to last image (clone above)
    #   - Set foreground fill to red (any colour really)
    #   - Draw (with red), a rounded rectangle the size of the image
    # - Switch order of image
    # - Set the composing anchor to be the centre of both images
    # - Compose down, using first image as mask
    args+=(#"("
           "(" "+clone" # Copy
           "-size" "%[fx:w]x%[fx:h]" "canvas:transparent"  # Little bigger
           "-delete" "-2" # Remove the copy 
           "-fill" "red"  # Set fill to red (any colour)
           # Draw rectangle with rounded corners 
           "-draw" "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] ${siz},${siz}"
           ")"
           "+swap" # Flip order 
           "-gravity" "center" # Align centres 
           "-compose" "In"  # Source over dest (really dest over source)
           "-composite"
           "-layers" "merge"
           # "-alpha" "set" # Set the alpha channel
           #")"
           #"+write" "rounded.png" 
          )
}

# --------------------------------------------------------------------
shade()
{
    size=${1-5} ; shift
    ang=${1-130} ; shift
    ela=${1-30}  ; shift
    
    args+=( #"+write" "before.png"
            "(" "+clone"             # Copy input
            "-fill" "gray50"         # Fill with gray
            "-colorize" "100"        # Full intensity
            "-bordercolor" "none"    # Border transparent
            "-border" "1x1"          # Add a border 
            "-alpha" "extract"       # Gray-scale mask of transparancy
            #"+write" "1-shape.png"
            ")"                      # orig shape
            "(" "+clone"             # Copy orig shape
            "-blur" "0x${size}"      # Blur the shape
            "-shade" "${ang}x${ela}" # Shade the mask
            "-shave" "1x1"           # Remove border again
            #"+write" "2-shape-shaded.png"
            ")"
            "(" "+clone"             # shaded shape
            "-fill" "gray50"         # Fill with gray
            "-colorize" "100"        # Full intensity
            #"+write" "3-gray.png"
            ")"                      # Gray image 
            "(" "-clone" "-1,-2"     # Copy shade and gray
            #"+write" "4-mask-input.png"
            "-auto-level"            # Fill range
            "-compose" "difference"  # Calculate difference
            "-composite"             # Do it
            "+channel"               # Reset channels
            "-evaluate-sequence" "max" # Find maximum of each channel
            "-auto-level"            # Normalize mask
            #"+write" "5-mask.png"  
            ")"                       # Mask on rim
            "-delete" "-2"
            "(" "-clone" "-3"        # Clone original"
            #"+write" "6-orig-copy.png"
            ")"                      # bulit mask
            "(" "-clone" "-1,-2"     # Copy rim-mask and orig mask
            "-alpha" "off"
            "-compose" "multiply"     # Multiply
            "-composite"              # Do it
            #"+write" "7-mask-merge.png"
            ")"
            #"+write" "8-after-merge.png"
            "-delete" "-5"           # Orig shape
            "-delete" "-3"           # Delete shade mask
            "-delete" "-2"           # Delete copy orig
            "("
            "-clone" "-2,-1"         # Copy shade and mask
            #"+write" "9-shades.png"
            #"-normalize"
            "-alpha" "off"           # Turn off alpha channel
            "-compose" "copy-opacity" # Copy opacity from mask to shade
            "-composite"             # Do it - now rim-shade
            #"+write" "a-shades.png"
            ")"
            #"+write" "b-after-shades.png"
            "-delete" "-3"           # Remove full shade
            "-delete" "-2"           # Remove mask
            "-compose" "over"        # Add rim-shade to source
            "-composite"
          )
    # args+=("-alpha" "extract")

    # Semi-works
    #
    # args+=( "(" "+clone" "-fill" "gray50" "-colorize" "100"
    #         "-bordercolor" "none" "-border" "1x1"
    #         "-alpha" "extract"
    #         "-blur" "0x${size}"
    #         "-shade" "${ang}x${ela}"
    #         "-shave" "1x1"
    #         "-alpha" "on" "-background" "gray50" "-alpha" "background"
    #         "-auto-level" #"-negate" #"-alpha" "copy" "-negate"
    #         "-function" "polynomial"  "3.5,-5.05,2.05,0.3"
    #         "(" "+clone" "-alpha" "extract" "-blur" "2" ")"
    #         "-channel" "RGB,sync" "-compose" "multiply" "-composite"
    #         "+channel" "+compose"
    #         #"-chop" "1x1"
    #         ")"
    #         "(" "-clone" "-2,-1" "-alpha" "opaque" 
    #         "-compose" "hardlight" "-composite" ")"
    #         "-delete" "-2"
    #         "-compose" "in" "-composite"
    #         #"-layers" "merge"
    #       )
    
}

# --------------------------------------------------------------------
crop()
{
    w=$1      ; shift
    h=$1      ; shift
    x=${1-0}  ; shift
    y=${1-0}  ; shift

    args+=("-gravity" "center" "-crop" "${w}x${h}+${x}+${y}")
}

# --------------------------------------------------------------------
shave()
{
    x=${1-0}  ; shift
    y=${1-0}  ; shift

    args+=("-shave" "${x}x${y}")
}

# --------------------------------------------------------------------
color()
{
    n=${1-16} ; shift
    d=${1-0} 
    if test $d -lt 1 ; then
        d="+dither"
    else
        d=""
        #d='-dither'
    fi
    args+=(${d} "-colors" "${color_n}")
}

# --------------------------------------------------------------------
# Build argument list
is_extract=0
for oper in ${opers[@]} ; do
    msg " Operation: ${oper}"
        
    case $oper in
        extract)
            if test ${#opers[@]} -gt 1 || test ${#args[@]} -gt 0 ; then
                echo "-extract cannot be used with other operations" \
                     > /dev/stderr
                exit 1
            fi
            extract ${extract_ncol} ${extract_nrow} ${extract_flip}
            is_extract=1
            ;;
        bevel)   bevel ${bevel_size}                          ;;
        shade)
            if test "x$prev" == "xrounded" ; then
                args+=("--split--")
            fi
            shade ${shade_size} ${shade_angle} ${shade_ela} ;;
        border)  border ${border_size} ${border_color}        ;;
        outline) outline ${outline_color}                     ;;
        drop)    dropshadow ${drop_size} ${drop_color} \
                            ${drop_fade} ${drop_opacity}      ;;
        rounded) rounded ${rounded_size}                      ;;
        crop)    crop ${crop_w} ${crop_h} ${crop_x} ${crop_y} ;;
        shave)   shave ${shave_x} ${shave_y}                  ;;
        fit)     fit ${fit_geom}                              ;;
        color)   color ${color_n} ${color_dither}             ;;
        *)
            echo "Unknown operation: ${oper}"
            ;;
    esac
    prev=$oper
done
msg "Arguments are " "${args[@]}"

# --------------------------------------------------------------------
find_split() {
    cur=$1
    n="${#args[@]}"

    for i in `seq $cur $n` ; do
        if test "x${args[$i]}" = "x--split--" ; then
            echo $i
            return 
        fi
        i=$(($i+1))
    done
    echo $n
}

# --------------------------------------------------------------------
for input in $inputs ; do
    # ----------------------------------------------------------------
    if test $is_extract -gt 0 ; then
        if test x$output == x ; then
            output=`echo $input | sed 's/\(.*\)\..*/\1/'`_%03d.png
        else
            output=`echo $output | sed 's/\(.*\)\..*/\1/'`_%03d.png
        fi
    fi
    if test x$output == x ; then
        output=`echo $input | sed 's/\(.*\)\..*/\1/'`_out.png
    fi
    
    msg "Input ${input} -> ${output}"

    # Loop over splits in the argument list
    #
    # We need this because a "shade" after "rounded" is broken for
    # some reason.  Now, we could have done the operations one at a
    # time, but that seeems a bit inefficient.
    cur=0
    n="${#args[@]}"
    while true ; do
        next=`find_split $cur`
        msg " " "$cur -> $next"
        msg " " magick "${input}" "${args[@]:$cur:$next}" "$output"
        magick "${input}" "${args[@]:$cur:$next}" "$output"

        cur=$(($next+1))
        if test $cur -ge $n ; then
           break
        fi
        input=$output
    done
    
    msg "done"
done
#
# EOF
#


