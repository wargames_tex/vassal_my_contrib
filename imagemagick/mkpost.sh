#!/bin/bash
#
# Image mapping
#
#
# ![back_block.png](upload://gr7itsNHXLTPjtG95M0j6MXmGnA.png)
# ![back_block_000.png](upload://sBHVNUrBH0o51z2wFQjGtRhxaY9.png)
# ![back_block_001.png](upload://kRt5hQP04wzHMdDG647ywDGRoyv.png)
# ![back_block_002.png](upload://6yjb78aSSywR5E42DhnT9Ebutgd.png)
# ![back_block_003.png](upload://u1YrNqDCPduAoidbGZqLEAhU4IQ.png)
# ![back_block_004.png](upload://zaKaXu3MOZRZ9pD8yEnz3B5B7yP.png)
# ![back_block_005.png](upload://yFvW9kOtlXMjdcDkuot9EDrkQCz.png)
# ![back_block_006.png](upload://ypj7JQoIk26s2hBt5Fj4bekQboN.png)
# ![back_block_007.png](upload://2nQg0vMVZNKQKlVF1g6jRGCSm8w.png)
# ![back_block_008.png](upload://zfHiW0KeQlLRLFs12tsumNKQeJm.png)
# ![back_block_009.png](upload://AgASqXwfBNki8PbazsY5DrpYBz.png)
# ![back_block_010.png](upload://fCJTtQG5nXfYVAxys9PDUYt5xaM.png)
# ![back_block_011.png](upload://oicmWQSQNBypFSgo1wyYdNsD7NC.png)
# ![back_block_012.png](upload://ojcEfRA0QJpic888qL8AFfYehW1.png)
# ![back_block_013.png](upload://hXlcYPOmFp83gkRcMBCAVWGiavL.png)
# ![back_block_014.png](upload://wGJ7FEooW8TTDldbmbfayQNZKMk.png)
# ![back_block_015.png](upload://q70BFSg44lq748tG4uBKG8aEAK3.png)
# ![back_block_016.png](upload://7sY4iLKTH28NBGqhjjTSMwTuJAR.png)
# ![back_block_017.png](upload://qFx7hdvNj19A9BwX9WP576m62Y9.png)
# ![back_sheet.jpeg](upload://ouwYxqwnThwb74QZD1RIfKnxH6f.jpeg)
# ![counter1.png](upload://mqCiR4uH429bq3nBOMbIMORskLo.png)
# ![counter1_bevel.png](upload://n6EmzfMIp8wjZcEGC25NKy1nAaa.png)
# ![counter1_border.png](upload://pCOBbu0YIBTBW0ydrpzZdRCTcGp.png)
# ![counter1_color.png](upload://5aAE77fOhBv6zQbX0EgkrP7RMjV.png)
# ![counter1_crop.png](upload://mU8Ap2uBH4gNDRtEC7C4aXAhLvc.png)
# ![counter1_drop.png](upload://vimX61eVKa4B1TvUYWdpJK0ySgg.png)
# ![counter1_fancy.png](upload://shSjZCwaJ0yzZfOrEQAerkD9A1U.png)
# ![counter1_fancy_32.png](upload://nflxRn7jpYeSap02iLCPpndO2vR.png)
# ![counter1_fancy_full.png](upload://hzsv5SZoxl7ADdghTlbIA2L4nxk.png)
# ![counter1_outline.png](upload://enQkXBASwrO5TKpFthTAJ5h77nv.png)
# ![counter1_rounded.png](upload://yzh8QGOomTuTUMdcyBaxW8d651H.png)
# ![counter1_shade.png](upload://3iopGXIPxsLGs6o8DqM6Uc6gbUV.png)
# ![counter1_shave.png](upload://mzSt99q48UIRBya4tHwAoCXTP4x.png)
# ![counter2.png](upload://lxkByaspSSyo6bwytfV0C5qWvIw.png)
# ![counter2_bevel.png](upload://wwg8cLRAa7bxsfsXoyqLCHu0yyt.png)
# ![counter2_border.png](upload://bNMfdBuf7ljqDzc4ZRUTVJtjeHM.png)
# ![counter2_color.png](upload://xBxyL1StiC1JCU9nYSdIYSGK1yj.png)
# ![counter2_crop.png](upload://zRwZXBaXFR4FrA1dIc5UQwQ0izM.png)
# ![counter2_drop.png](upload://yh0V3o6HtFJiQwfHEJpyKmPwtH2.png)
# ![counter2_fancy.png](upload://mfnJrxQgLbLvDz4gpK5IA0aYhLO.png)
# ![counter2_fancy_32.png](upload://d1bk8cOKiNKOVuXUeqqxetfjL2s.png)
# ![counter2_fancy_full.png](upload://naJZ0japO3UH6s3TJ2hg0R7uy3R.png)
# ![counter2_fancy.png](upload://qzzXrG7fHkUIrBrQrTci7PdipSN.png)
# ![counter2_outline.png](upload://n7tsgfpPpf6vlcaapnm1Ld03zZP.png)
# ![counter2_rounded.png](upload://jr4cNrut2kc1cWFWWSDA5h7OM0o.png)
# ![counter2_shade.png](upload://mB3aptG1qFXcAIUbI7wZKBnckmO.png)
# ![counter2_shave.png](upload://h0ZbcLwgVAM4C39opzCzBottNzQ.png)
# ![front_block.png](upload://t1u7TKlHcL81zqlsYou3D2H8uZa.png)
# ![front_block_000.png](upload://feeNukLrRkMXIVTS0EurlsS0DAi.png)
# ![front_block_001.png](upload://h9IEpGXL1ApX3M9OaBqJ3722Xmj.png)
# ![front_block_002.png](upload://T1qB8itTTZA2cI06KTLBdrWsYv.png)
# ![front_block_003.png](upload://yMh5Kw548LTMYkjg1a1rXObKxZH.png)
# ![front_block_004.png](upload://9MVPRznwbqLozA6pa4sVWzUAdh8.png)
# ![front_block_005.png](upload://jqmEN2IQWkUkUgU9WLIW19wKHZp.png)
# ![front_block_006.png](upload://uSyf1lcG1DFfQfwsMZbgCZ4M2Q1.png)
# ![front_block_007.png](upload://lnflxERMVnJTPI4qLRCNwOpYHWX.png)
# ![front_block_008.png](upload://oOL0Op2ItHUeKQrEu90phc1tPR8.png)
# ![front_block_009.png](upload://bDgzWeLBbIcTXo8PxOULt6aS4eN.png)
# ![front_block_010.png](upload://rYPkg6yJK5rM0xvMR788WntxheX.png)
# ![front_block_011.png](upload://ifofNnRU5Tia8Nh61W1xiiUW1j8.png)
# ![front_block_012.png](upload://xZAhKAX6fBdOYPaR2OtZ0wggYSv.png)
# ![front_block_013.png](upload://xWzsX0R0WF4Rl0nEJHes5O0s6Hb.png)
# ![front_block_014.png](upload://7LlzsrYty90uxPVcWzHW1GBgsva.png)
# ![front_block_015.png](upload://foLD3G3fx1ab32X8Wc7b2AOwzHI.png)
# ![front_block_016.png](upload://jLpgIKbfrAVShcLRZKHxCuNB0MQ.png)
# ![front_block_017.png](upload://3czGp6lVSTlAhvfHLHfTL3Tx5Zp.png)
# ![front_sheet.jpeg](upload://nMkcsBKi1YxbnTxCTv8t5v1nztW.jpeg)

process() {
    in=$1
    out=$2 
    sed \
        -e 's,!\[\](back_block.png),!\[back_block.png\](upload://gr7itsNHXLTPjtG95M0j6MXmGnA.png),' \
        -e 's,!\[\](back_block_000.png),!\[back_block_000.png\](upload://sBHVNUrBH0o51z2wFQjGtRhxaY9.png),' \
        -e 's,!\[\](back_block_001.png),!\[back_block_001.png\](upload://kRt5hQP04wzHMdDG647ywDGRoyv.png),' \
        -e 's,!\[\](back_block_002.png),!\[back_block_002.png\](upload://6yjb78aSSywR5E42DhnT9Ebutgd.png),' \
        -e 's,!\[\](back_block_003.png),!\[back_block_003.png\](upload://u1YrNqDCPduAoidbGZqLEAhU4IQ.png),' \
        -e 's,!\[\](back_block_004.png),!\[back_block_004.png\](upload://zaKaXu3MOZRZ9pD8yEnz3B5B7yP.png),' \
        -e 's,!\[\](back_block_005.png),!\[back_block_005.png\](upload://yFvW9kOtlXMjdcDkuot9EDrkQCz.png),' \
        -e 's,!\[\](back_block_006.png),!\[back_block_006.png\](upload://ypj7JQoIk26s2hBt5Fj4bekQboN.png),' \
        -e 's,!\[\](back_block_007.png),!\[back_block_007.png\](upload://2nQg0vMVZNKQKlVF1g6jRGCSm8w.png),' \
        -e 's,!\[\](back_block_008.png),!\[back_block_008.png\](upload://zfHiW0KeQlLRLFs12tsumNKQeJm.png),' \
        -e 's,!\[\](back_block_009.png),!\[back_block_009.png\](upload://AgASqXwfBNki8PbazsY5DrpYBz.png),' \
        -e 's,!\[\](back_block_010.png),!\[back_block_010.png\](upload://fCJTtQG5nXfYVAxys9PDUYt5xaM.png),' \
        -e 's,!\[\](back_block_011.png),!\[back_block_011.png\](upload://oicmWQSQNBypFSgo1wyYdNsD7NC.png),' \
        -e 's,!\[\](back_block_012.png),!\[back_block_012.png\](upload://ojcEfRA0QJpic888qL8AFfYehW1.png),' \
        -e 's,!\[\](back_block_013.png),!\[back_block_013.png\](upload://hXlcYPOmFp83gkRcMBCAVWGiavL.png),' \
        -e 's,!\[\](back_block_014.png),!\[back_block_014.png\](upload://wGJ7FEooW8TTDldbmbfayQNZKMk.png),' \
        -e 's,!\[\](back_block_015.png),!\[back_block_015.png\](upload://q70BFSg44lq748tG4uBKG8aEAK3.png),' \
        -e 's,!\[\](back_block_016.png),!\[back_block_016.png\](upload://7sY4iLKTH28NBGqhjjTSMwTuJAR.png),' \
        -e 's,!\[\](back_block_017.png),!\[back_block_017.png\](upload://qFx7hdvNj19A9BwX9WP576m62Y9.png),' \
        -e 's,!\[\](back_sheet.jpeg),!\[back_sheet.jpeg\](upload://ouwYxqwnThwb74QZD1RIfKnxH6f.jpeg),' \
        -e 's,!\[\](counter1.png),!\[counter1.png\](upload://mqCiR4uH429bq3nBOMbIMORskLo.png),' \
        -e 's,!\[\](counter1_bevel.png),!\[counter1_bevel.png\](upload://n6EmzfMIp8wjZcEGC25NKy1nAaa.png),' \
        -e 's,!\[\](counter1_border.png),!\[counter1_border.png\](upload://pCOBbu0YIBTBW0ydrpzZdRCTcGp.png),' \
        -e 's,!\[\](counter1_color.png),!\[counter1_color.png\](upload://5aAE77fOhBv6zQbX0EgkrP7RMjV.png),' \
        -e 's,!\[\](counter1_crop.png),!\[counter1_crop.png\](upload://mU8Ap2uBH4gNDRtEC7C4aXAhLvc.png),' \
        -e 's,!\[\](counter1_drop.png),!\[counter1_drop.png\](upload://vimX61eVKa4B1TvUYWdpJK0ySgg.png),' \
        -e 's,!\[\](counter1_fancy.png),!\[counter1_fancy.png\](upload://shSjZCwaJ0yzZfOrEQAerkD9A1U.png),' \
        -e 's,!\[\](counter1_fancy_32.png),!\[counter1_fancy_32.png\](upload://nflxRn7jpYeSap02iLCPpndO2vR.png),' \
        -e 's,!\[\](counter1_fancy_full.png),!\[counter1_fancy_full.png\](upload://hzsv5SZoxl7ADdghTlbIA2L4nxk.png),' \
        -e 's,!\[\](counter1_outline.png),!\[counter1_outline.png\](upload://enQkXBASwrO5TKpFthTAJ5h77nv.png),' \
        -e 's,!\[\](counter1_rounded.png),!\[counter1_rounded.png\](upload://yzh8QGOomTuTUMdcyBaxW8d651H.png),' \
        -e 's,!\[\](counter1_shade.png),!\[counter1_shade.png\](upload://3iopGXIPxsLGs6o8DqM6Uc6gbUV.png),' \
        -e 's,!\[\](counter1_shave.png),!\[counter1_shave.png\](upload://mzSt99q48UIRBya4tHwAoCXTP4x.png),' \
        -e 's,!\[\](counter2.png),!\[counter2.png\](upload://lxkByaspSSyo6bwytfV0C5qWvIw.png),' \
        -e 's,!\[\](counter2_bevel.png),!\[counter2_bevel.png\](upload://wwg8cLRAa7bxsfsXoyqLCHu0yyt.png),' \
        -e 's,!\[\](counter2_border.png),!\[counter2_border.png\](upload://bNMfdBuf7ljqDzc4ZRUTVJtjeHM.png),' \
        -e 's,!\[\](counter2_color.png),!\[counter2_color.png\](upload://xBxyL1StiC1JCU9nYSdIYSGK1yj.png),' \
        -e 's,!\[\](counter2_crop.png),!\[counter2_crop.png\](upload://zRwZXBaXFR4FrA1dIc5UQwQ0izM.png),' \
        -e 's,!\[\](counter2_drop.png),!\[counter2_drop.png\](upload://yh0V3o6HtFJiQwfHEJpyKmPwtH2.png),' \
        -e 's,!\[\](counter2_fancy.png),!\[counter2_fancy.png\](upload://mfnJrxQgLbLvDz4gpK5IA0aYhLO.png),' \
        -e 's,!\[\](counter2_fancy_32.png),!\[counter2_fancy_32.png\](upload://d1bk8cOKiNKOVuXUeqqxetfjL2s.png),' \
        -e 's,!\[\](counter2_fancy_full.png),!\[counter2_fancy_full.png\](upload://naJZ0japO3UH6s3TJ2hg0R7uy3R.png),' \
        -e 's,!\[\](counter2_fancy.png),!\[counter2_fancy.png\](upload://qzzXrG7fHkUIrBrQrTci7PdipSN.png),' \
        -e 's,!\[\](counter2_outline.png),!\[counter2_outline.png\](upload://n7tsgfpPpf6vlcaapnm1Ld03zZP.png),' \
        -e 's,!\[\](counter2_rounded.png),!\[counter2_rounded.png\](upload://jr4cNrut2kc1cWFWWSDA5h7OM0o.png),' \
        -e 's,!\[\](counter2_shade.png),!\[counter2_shade.png\](upload://mB3aptG1qFXcAIUbI7wZKBnckmO.png),' \
        -e 's,!\[\](counter2_shave.png),!\[counter2_shave.png\](upload://h0ZbcLwgVAM4C39opzCzBottNzQ.png),' \
        -e 's,!\[\](front_block.png),!\[front_block.png\](upload://t1u7TKlHcL81zqlsYou3D2H8uZa.png),' \
        -e 's,!\[\](front_block_000.png),!\[front_block_000.png\](upload://feeNukLrRkMXIVTS0EurlsS0DAi.png),' \
        -e 's,!\[\](front_block_001.png),!\[front_block_001.png\](upload://h9IEpGXL1ApX3M9OaBqJ3722Xmj.png),' \
        -e 's,!\[\](front_block_002.png),!\[front_block_002.png\](upload://T1qB8itTTZA2cI06KTLBdrWsYv.png),' \
        -e 's,!\[\](front_block_003.png),!\[front_block_003.png\](upload://yMh5Kw548LTMYkjg1a1rXObKxZH.png),' \
        -e 's,!\[\](front_block_004.png),!\[front_block_004.png\](upload://9MVPRznwbqLozA6pa4sVWzUAdh8.png),' \
        -e 's,!\[\](front_block_005.png),!\[front_block_005.png\](upload://jqmEN2IQWkUkUgU9WLIW19wKHZp.png),' \
        -e 's,!\[\](front_block_006.png),!\[front_block_006.png\](upload://uSyf1lcG1DFfQfwsMZbgCZ4M2Q1.png),' \
        -e 's,!\[\](front_block_007.png),!\[front_block_007.png\](upload://lnflxERMVnJTPI4qLRCNwOpYHWX.png),' \
        -e 's,!\[\](front_block_008.png),!\[front_block_008.png\](upload://oOL0Op2ItHUeKQrEu90phc1tPR8.png),' \
        -e 's,!\[\](front_block_009.png),!\[front_block_009.png\](upload://bDgzWeLBbIcTXo8PxOULt6aS4eN.png),' \
        -e 's,!\[\](front_block_010.png),!\[front_block_010.png\](upload://rYPkg6yJK5rM0xvMR788WntxheX.png),' \
        -e 's,!\[\](front_block_011.png),!\[front_block_011.png\](upload://ifofNnRU5Tia8Nh61W1xiiUW1j8.png),' \
        -e 's,!\[\](front_block_012.png),!\[front_block_012.png\](upload://xZAhKAX6fBdOYPaR2OtZ0wggYSv.png),' \
        -e 's,!\[\](front_block_013.png),!\[front_block_013.png\](upload://xWzsX0R0WF4Rl0nEJHes5O0s6Hb.png),' \
        -e 's,!\[\](front_block_014.png),!\[front_block_014.png\](upload://7LlzsrYty90uxPVcWzHW1GBgsva.png),' \
        -e 's,!\[\](front_block_015.png),!\[front_block_015.png\](upload://foLD3G3fx1ab32X8Wc7b2AOwzHI.png),' \
        -e 's,!\[\](front_block_016.png),!\[front_block_016.png\](upload://jLpgIKbfrAVShcLRZKHxCuNB0MQ.png),' \
        -e 's,!\[\](front_block_017.png),!\[front_block_017.png\](upload://3czGp6lVSTlAhvfHLHfTL3Tx5Zp.png),' \
        -e 's,!\[\](front_sheet.jpeg),!\[front_sheet.jpeg\](upload://nMkcsBKi1YxbnTxCTv8t5v1nztW.jpeg),' \
        < $in > $out
}

process README.md post.md
