#!/bin/bash

# --------------------------------------------------------------------
usage() {
    l=${1-0}
    cat <<-EOF
	Usage: $0 [OPTIONS] INPUT ...
        
	Options:
	  -h,--help		Usage help
	  -H,--long-help	Show full help
	  -o,--output NAME	Output file name
	  -v,--verbose		Be verbose
	  --<operation> ...

	If no output file is specified via option -o or --output, then
	for input file INPUT.EXT, the default output file name is
	INPUT_out.png.

	EOF

    if test $l -gt 0 ; then
        return
    fi
    
    cat <<-EOF
	For more on possible operations, try

	  $0 --long-help

	EOF
}

# --------------------------------------------------------------------
help() {
    usage 1
    cat <<-EOF
	Operations:
	
	  --extract N M [FLIP]
	
	        Create images from (part) of a counter sheet.  The
	        input image is assumed to be an N columns and M rows
	        counter sheet with no spacing between counters.
	
	        If FLIP is "true", "yes", or "1", then flip order of
	        extraction.  This is useful when extracting the "back"
	        side of counters to keep the same numbering as the
	        front side.
	
	        For input file INPUT, the images are written to
	        INPUT_NUM.png, where NUM is a 3-digit, zero-padded,
	        serial number (starting at 0).
	
	  --bevel [SIZE]
	
	        Add bevel to images.  The bevel is SIZE large.	SIZE
	        defaults to 4.	Note, the bevel is really
	        semi-transparent.
	
	  --border [SIZE [COLOUR]]
	
	        Add a border to images.	 The border is SIZE pixels
	        large, and has COLOUR.	If SIZE is not specified, then
	        it will be set to 4.  COLOUR defaults to black.	 Use
	        --outline below for non-rectangular or rounded corners
	        images.
	
	  --outline [COLOUR]
	
	        Add outline to images.	COLOUR specifies the colour
	        (defaults to black).  Use this, rather than --border
	        if the images are not rectangular or have rounded
	        corners.
	
	  --drop [SIZE [COLOUR [FADE [OPACITY]]]]

	        Add drop shadow images.	 The drop shadow will be SIZE
	        pixels big, and be COLOUR base colour.	If FADE is
	        given and not zero, then the shadow will fade over
	        that many pixels.  If OPACITY is given, then the
	        drop-shadow will have that opacity (0-100).
	
	        SIZE defaults to 4, COLOUR to grey7 (0x121212), FADE
	        to 0, and OPACITY to 60.
	
	        Note that the image grows by FADE top, bottom, left,
	        and right, and by SIZE to the bottom and right, only.
	
	  --rounced [SIZE]
	
	        Round the corners of the images.  The radius of the
	        rounding will be SIZE.	SIZE defaults to 16.
	
	  --crop WIDTH HEIGHT [X [Y]]

	        Crop image within the geometry.  WIDTH is the new
	        width, HEIGHT is the new HEIGHT, and +/-X and +/- Y
	        are the offset of that rectangle with respect to the
	        centre of the image.
	
	  --shave X [Y]
	
	        Remove pixels horizontally or vertically.  X
	        determines how many pixels are removed on the left and
	        right side of the image.  Y determines how many pixels
	        are removed at the top and bottom.  Both X and Y
	        defaults to 0.

	  --color [N [DITHER]] 

	        Reduce the number of colours used by the image to N
	        colours. N defaults to 16. If DITHER is 'yes', 'on',
	        '1', 'dither', (default) then the image will be
	        dithered when reducing number of colours.  If DITHER
	        is 'no', 'off', 'false', 'no-dither' or '0', then
	        dithering will be turned off.

	EOF
}

# --------------------------------------------------------------------
more()
{
    case $1 in
        -*) return 1 ;;
        *)
            if test -f $1 ; then
                return 1
            fi
            ;;
    esac
    return 0;
}

# --------------------------------------------------------------------
opers=""
inputs=""
verb=0
while test $# -gt 0 ; do
    case $1 in
        -h|--help)      usage ; exit 0 ;;
        -H|--long-help) help  ; exit 0 ;;
        -o|--output)    output=$2 ; shift ;;
        -v|--verbose)   verb=1 ;;
        -q|--quiet)     verb=0 ;;
        --extract)
            opers="${opers} extract" 
            extract_ncol=$2        ; shift
            extract_nrow=$2        ; shift
            extract_flip=0
            if more $2 ; then
                case $2 in
                    -*) ;;
                    1|flop|true|yes|flip|back) extract_flip=1  ;;
                    0|false|no|front)          extract_flip=0  ;;
                esac
                shift
            fi
            ;;
        --bevel)
            opers="$opers bevel"
            if more $2 ; then bevel_size=$2   ; shift  ; fi
            ;;
        --border)
            opers="$opers border"
            if more $2 ; then border_size=$2  ; shift  ; fi
            if more $2 ; then border_color=$2 ; shift  ; fi
            ;;
        --outline)
            opers="$opers outline"
            if more $2 ; then outline_color=$2 ; shift  ; fi
            ;;
        --drop)
            opers="$opers drop"
            if more $2 ; then drop_size=$2    ; shift  ; fi
            if more $2 ; then drop_color=$2   ; shift  ; fi
            if more $2 ; then drop_fade=$2    ; shift  ; fi
            if more $2 ; then drop_opacity=$2 ; shift  ; fi
            ;;
        --rounded)
            opers="$opers rounded"
            if more $2 ; then rounded_size=$2 ; shift  ; fi
            ;;
        --crop)
            opers="$opers crop" 
            crop_w=$2 ; shift
            crop_h=$2 ; shift
            if more $2 ; then crop_x=$2 ; shift  ; fi
            if more $2 ; then crop_y=$2 ; shift  ; fi
            ;;
        --shave)
            opers="$opers shave"
            shave_x=$2 ; shift
            if more $2 ; then shave_y=$2 ; shift  ; fi            
            ;;
        --color|--colour)
            opers="$opers color"
            color_n=$2 ; shift
            if more $2 ; then
                case $2 in
                    -*) ;;
                    1|on|true|yes|dither)     color_dither=1  ;;
                    0|off|false|no|no-dither) color_dither=0  ;;
                esac
                shift
            fi
            ;;
        --)
            ;;
        -*)
            echo "Unknown option: $1" > /dev/stderr
            exit
            ;;
        *)
            inputs="$inputs $1"
            ;;
    esac
    shift 
done

# --------------------------------------------------------------------
msg()
{
    if test $verb -lt 1 ; then
        return 0
    fi
    echo "${@}"
}

# --------------------------------------------------------------------
msg "Operations are: '${opers}'"
msg "Input files are: '${inputs}'"


# --------------------------------------------------------------------
run()
{
    msg " " magick "${@}"
    magick "${@}"
}

# --------------------------------------------------------------------
show()
{
    if test $verb -lt 1 ; then
        return
    fi
    if test x$outfiles != "x" ; then
        echo " Output files are "
        for file in $outfiles ; do
            echo "  $file"
        done
        return
    fi

    echo " Output file is $output"
}
            

# --------------------------------------------------------------------
extract()
{
    inp=$1        ; shift
    dir=$1        ; shift
    ncol=$1       ; shift
    nrow=$1       ; shift
    flip=${1-0}   ; shift
    

    out=${dir}/extract_%03d.png

    flop=
    if test $flip -gt 0 ; then
        flop="-flop"
    fi

    run ${inp} ${flop} -crop ${flop} ${ncol}x${nrow}@ ${flop} ${out}
    
    outfiles=`ls ${dir}/extract_*`
}

# --------------------------------------------------------------------
bevel()
{
    inp=$1	; shift
    out=$1	; shift 
    size=${1-4}

    run ${inp} -raise ${size} ${out}
}

# --------------------------------------------------------------------
border()
{
    inp=$1	; shift
    out=$1	; shift 
    size=${1-4} ; shift 
    col=${1-black}

    # sample_x=1
    # sample_y=1
    # sample=$(printf "#%02x%02x%02x" `magick $inp -crop ${sample_x}x${sample_y}+1+1 -format "%[fx:int(255*r+.5)] %[fx:int(255*g+.5)] %[fx:int(255*b+.5)]" info:`)
    
    run ${inp} -bordercolor "${col}" -border ${size}x${size} ${out}
}

# --------------------------------------------------------------------
outline()
{
    inp=$1	; shift
    out=$1	; shift 
    col=${1-black}

    # First clone image and make it entirely black
    # Then clone the starting image again,
    # - extract alpha channel and make grayscale mask
    # - set virtual (or surrounding pixel) to be black
    # - using an octagon mask, and edge-in morphology, the
    # - edge of the image is found, roughly 2 to 3 pixels bigger.
    # The images are finally composed down to a single image 
    run ${inp} \
           \( -clone 0 \
           -fill ${col} \
           -colorize 100 \) \
           \( -clone 0 \
           -alpha extract \
           -virtual-pixel black \
           -morphology edgein octagon \) \
           -compose over \
           -composite \
           ${out}
}    

# --------------------------------------------------------------------
dropshadow()
{
    inp=$1	   ; shift
    out=$1	   ; shift 
    siz=${1-4}     ; shift
    col=${1-grey7} ; shift
    blu=${1-0}     ; shift 
    opa=${1-60}    ;
    # Copy input image,
    # fill it with with color
    # Add shadow
    # Swap images,
    # make background invisible
    # Merge layers
    run ${inp} \
           \( +clone \
           -background "${col}" \
           -shadow ${opa}x${blu}+${siz}+${siz} \
           \) \
           +swap \
           -background none \
           -layers merge \
           ${out}
}

# --------------------------------------------------------------------
rounded()
{
    inp=$1	   ; shift
    out=$1	   ; shift 
    siz=${1-16}    ; shift

    # +antialias ^
    # - Set up mask 
    #   - Clone input,
    #   - make the clone (needed)
    #   - set size to be the same of image, and make transparent canvas image 
    #     and make it transparent
    #   - Delete the second to last image (clone above)
    #   - Set foreground fill to red (any colour really)
    #   - Draw (with red), a rounded rectangle the size of the image
    # - Switch order of image
    # - Set the composing anchor to be the centre of both images
    # - Compose down, using first image as mask 
    run ${inp} \
           \( +clone \
           -size %[fx:w]x%[fx:h] canvas:transparent \
           -delete -2 \
           -fill red \
           -draw "roundrectangle 0,0 %[fx:w-1],%[fx:h-1] ${siz},${siz}" \) \
        +swap \
        -gravity center \
        -compose In \
        -composite \
        ${out}
}

# --------------------------------------------------------------------
crop()
{
    inp=$1    ; shift
    out=$1    ; shift 
    w=$1      ; shift
    h=$1      ; shift
    x=${1-0}  ; shift
    y=${1-0}  ; shift

    run $inp -gravity center -crop "${w}x${h}+${x}+${y}" $out
}

# --------------------------------------------------------------------
shave()
{
    inp=$1    ; shift
    out=$1    ; shift 
    x=${1-0}  ; shift
    y=${1-0}  ; shift

    run $inp -shave "${x}x${y}" $out
}

# --------------------------------------------------------------------
color()
{
    inp=$1    ; shift
    out=$1    ; shift 
    n=${1-16} ; shift
    d=${1-0} 
    if test $d -lt 1 ; then
        d="+dither"
    else
        d=""
        #d='-dither'
    fi
    
    run $inp ${d} -colors "${color_n}" $out
}

# --------------------------------------------------------------------
for input in $inputs ; do
    # ----------------------------------------------------------------
    if test "x$output" = "x" ; then
        output=`echo $input | sed 's/\(.*\)\..*/\1/'`_out.png
    fi
    msg "Input ${input} -> ${output}"

    tmpd=`mktemp -d`
    for oper in $opers ; do
        
        msg " Operation: ${oper}"
        tmpo=$tmpd/${oper}.png
        outfiles=""
        
        case $oper in
            extract)
                extract ${input} ${tmpd} ${extract_ncol} ${extract_nrow}
                output=`echo $input | sed 's/\(.*\)\..*/\1/'`
                ;;
            bevel)
                bevel ${input} ${tmpo} ${bevel_size}
                ;;
            border)
                border ${input} ${tmpo} ${border_size} ${border_color}
                ;;
            outline)
                outline ${input} ${tmpo} ${outline_color}
                ;;
            drop)
                dropshadow ${input} ${tmpo} ${drop_size} ${drop_color} \
                           ${drop_fade} ${drop_opacity}
                ;;
            rounded)
                rounded ${input} ${tmpo} ${rounded_size} 
                ;;
            crop)
                crop ${input} ${tmpo} ${crop_w} ${crop_h} ${crop_x} ${crop_y} 
                ;;
            shave)
                shave ${input} ${tmpo} ${shave_x} ${shave_y}
                ;;
            fit)
                fit ${input} ${tmpo} ${fit_geom}
                ;;
            color)
                color ${input} ${tmpo} ${color_n} ${color_dither}
                ;;
            *)
                echo "Unknown operation: ${oper}"
                ;;
        esac

        noutfiles=
        if test "x$outfiles" != "x" ; then
            for out in $outfiles ; do
                outn=`basename $out | sed "s/\(.*\)_\(.*\)/${output}_\2/"`
                mv $out $outn
                noutfiles="$noutfiles $outn"
            done
            break
            outfiles="$noutfiles"
            show 
        fi
        input=$tmpo
    done
    if test -f $tmpo ; then
        mv $tmpo $output
        show
    fi
done
#
# EOF
#


