#!/usr/bin/env python

# --------------------------------------------------------------------
def do_desc(text,verbose=False):
    from tempfile import mkstemp
    from subprocess import Popen, PIPE
    from os import unlink
    

    tmp, tmpnam = mkstemp(text=True)
    with open(tmp,'w') as tmpfile:
        tmpfile.write(text)

    cmd = ['pandoc',
           '--from', 'html',
           '--to', 'markdown-simple_tables',
           tmpnam]
    out,err = Popen(cmd, stdout=PIPE,stderr=PIPE).communicate()

    unlink(tmpnam)

    return out.decode()

# --------------------------------------------------------------------
def do_one(path):
    from json import load, loads
    
    ret = {}
    with open(path,'r') as inp:
        d = inp.read()
        if len(d) <= 0:
            return {}
        
        j = loads(d)

    nam = list(j.keys())[0]
    
    bgg = path.with_suffix('.bgg')
    if bgg.exists():
        with open(bgg,'r') as inp:
            d = inp.read()
            b = loads(d) if len(d) > 0 else None

        if b:
            bg = b.get('boardgame',{})
            if 'poll' in bg:
                del bg['poll']
            bg['description'] = do_desc(bg.get('description',{})
                                        .get('text',''))
            j[nam]['bgg'] = bg


    rdm = path.with_suffix('.md')
    if rdm.exists():
        with open(rdm,'r') as inp:
            r = inp.read()

        j[nam]['readme'] = r

    return j

# --------------------------------------------------------------------
def do_all():
    from pathlib import Path
    from json import dump
    
    pathlist = Path(".").glob("*.json")

    ret = {}
    
    for path in sorted(pathlist):
        try:
            print(f'{path}...',end='')
            db = do_one(path)
            print(f'{"OK" if len(db) > 0 else "nothing"}')
            ret.update(db)
        except Exception as e:
            print(f'When merging "{path}": {e}')


    with open('../pages_bgg.json','w') as outp:
        dump(ret,outp,indent=2)


if __name__ == '__main__':
    do_all()


        
