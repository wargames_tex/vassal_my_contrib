#!/usr/bin/env python

from json import load

with open('files_meta.json','r') as f:
    j = load(f)
 

v = {f['version']: f['semVersion'] for f in j.values() if 'version' in f}

o=0
print(f'| {"From file":20s} {"Parsed":20s} '*2+'|')
print(f'|{"-"*43}'*2+'|\n|',end='',flush=True)

for i,(r,s) in enumerate(v.items()):
    l = f' {r:20s} {s:20s} |'
    if o+len(l) > 100:
        print('\n|',end='',flush=True)
        o = 0
    o += len(l)
    print(l,end='',flush=True)
