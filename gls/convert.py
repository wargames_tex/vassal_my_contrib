#!/usr/bin/env python

from mwparserfromhell import parse
from json import dumps

# --------------------------------------------------------------------
def do_check_version(version,raw,name):
    '''Check a version number

    Parameters
    ----------
    version : str
        The version number to parse
    fileName : str
        The file we got the version number from

    Returns
    -------
    semVersion : str
        Semantic version number or "unknown"
    '''
    from semver import VersionInfo

    if not version or version == 'unknown':
        return 'unknown'
    
    try:
        return str(VersionInfo.parse(version))
    except Exception as e:
        pass
        # print(f'{name}: Bad version "{raw}" ("{version}"): {e}')
     
    return 'unknown'

# --------------------------------------------------------------------
def do_version(version,name):
    '''Try to parse a version number

    Parameters
    ----------
    version : str
        The version number to parse
    fileName : str
        The file we got the version number from

    Returns
    -------
    semVersion : str
        Semantic version number or "unknown"
    '''
    from re import match, IGNORECASE
    from semver import VersionInfo

    # First, strip spaces 
    base = version.strip()
    if len(base) <= 0:
        return base

    # Pre-release defaults to empty
    pre  = ''
    
    # Strip leading stuff which isn't a number
    m = match(r'(^[^-0-9._]*) *(.*)',base)
    if m is not None and len(m.groups()) >= 1:
        pre  = ('-'+m[1].strip().replace('-','')) if len(m[1])>0 else ''
        base = m[2].strip()
        
    # Then see if we have a leading "version" 
    m = match(r'(v|version|ver) *([-0-9._ ]+.*)', base, IGNORECASE)
    if m is not None and len(m.groups()) > 1:
        base = m[2].strip()

    # Replace '-', '_', and ',' with '.', and strip leading/trailing whitespace
    base = base\
        .replace('_','.')\
        .replace('-','.')\
        .replace(',','.')\
        .strip()

    # See if we have something that looks like a release 
    m = match(r'([-0-9.]+)([^0-9.]+.*)',base)
    if m is not None and len(m.groups()) > 1:
        base = m[1].replace(' ','_')
        pre  = '-'+m[2].strip().replace('-','')

    # If the first character is a dot, prepend a zero 
    if base.startswith('.'):
        base = '0'+base

    # Now remove '.0' from string.  
    while True:
        m = match(r'(.*)\.0(.*)',base)
        if m is None: break
        base = '.'.join(m.groups())
        #print('->',base)

    # Remove leading and trailing dots,
    # replace double dots with a 0 inbetween 
    base = base.strip('.')
    base = base.replace('..','.0.')
    #print('=>',base)
    
    # Check how many dots we have and pad or remove as needed 
    if base.count('.') >= 3:
        spl  = base.split('.')
        #print('~',spl)
        pre  = '-'+'_'.join(spl[3:])
        base = '.'.join(spl[:3])
        #print('~~',base,pre)
    elif base.count('.') == 2:
        pass
    elif base.count('.') == 1:
        base += '.0'
    elif base.count('.') == 0:
        base += '.0.0'

    pre = pre\
        .replace(' ','')\
        .replace('(','')\
        .replace(')','')\
        .replace('+','')\
        .replace('_','')
    
    # Form our final estimate 
    test = base + pre

    # Try to parse as a semantic version number
    return do_check_version(test,version,name)

# --------------------------------------------------------------------
def do_gameinfo(code,title,filename,verbose=False):
    '''Parse the game information, extracted from the `GameInfo` template.

    Parameters
    ----------
    code : wikitext
        The input WikiText
    verbose : boolean
        Whether to be verbose

    Returns
    -------
    db : dict
        Dictionary of values 
    '''
    if verbose: 
        print(f'Doing game info')
        
    gi = code.filter_templates(matches=lambda n : n.name=='GameInfo')
    if not gi or len(gi) < 1:
        raise RuntimeError(f'No GameInfo for {title} in {filename}')

    gi = gi[0]

    ret = {k: str(gi.get(k)).replace(f'{k}=','') for k in
           ['image',
            'publisher',
            'year',
            'era',
            'topic',
            'series',
            'scale',
            'players',
            'length']
           if gi.has(k)}

    code.replace(gi,'')

    return ret

# --------------------------------------------------------------------
def do_emails(text,verbose=False):
    '''Process emails

    Parameters
    ----------
    text : wikitext
        Input text

    Returns
    -------
    db : dict
        Dictionary of addresses and names
    '''
    if verbose:
        print(f'Doing emails')
        
    main = parse(text)

    eml = main.filter_templates()
    return [{'name': str(e.params[1]) if len(e.params) > 1 else '',
             'address': str(e.params[0])}
            for e in eml
            if e.name == 'email' and str(e.params[0]) != 'someguy@example.com'
            ]
    
# --------------------------------------------------------------------
def do_modules(code,title,verbose=False):
    '''Process modules on page

    Parameters
    ----------
    code : wikitext
        The WikiText of the page
    verbose : boolean
        Whether to be verbose

    Returns
    -------
    db : dict
        Dictionary of values
    '''
    if verbose:
        print(f'Doing modules')
        
    # --- Old style maintainers and contributors ---------------------
    tmpl = code.filter_templates(matches=lambda n :
                                 n.name == 'ModuleContactInfo',
                                 recursive=False)

    maintainers  = None
    contributors = None
    main         = {'address': 'unknown', 'name': 'Unknown'}

    for tm in tmpl:
        maintainers  = do_emails(str(tm.get('maintainer'))
                                if tm.has('maintainer') else '')
        contributors = do_emails(str(tm.get('contributors')
                                     if tm.has('contributors') else ''))
        if len(maintainers) > 0:
            main = maintainers[0]

    for tm in tmpl:
        code.replace(tm, '')
            

    # --- Module table (old or new) ----------------------------------
    names = ['ModuleFilesTable2', # 0 
             'ModuleVersion2',    # 1
             'ModuleFile2',       # 2
             'ModuleFilesTable',  # 3
             'ModuleVersion',     # 4
             'ModuleFile'         # 5
             ]
    tmpl = code.filter_templates(matches=lambda n : n.name in names,
                                 recursive=False)

    
    cur   = None
    ver   = 'unknown'
    projs = None
    last  = 'default'
    pkg   = None
    for tm in tmpl:
        # --- Got a table ---
        if tm.name in names[0::3]:
            projs = {}
            continue

        # --- If not a table, but table wasn't seen ----
        if projs is None:
            raise RuntimeError(f'{tm.name} seen before {",".join(names[0::3])}')

        # --- A version header ----
        # 
        # will try to parse the version info as a version.  If that
        # succeeds, then we will not use it (we will trust the file
        # version information instead).  If the version info could not
        # be parsed as a version number, then assume its the title of
        # the next package.
        if tm.name in names[1::3]:
            # cur      = []
            last     = tm.get('version').replace('version=','') \
                if tm.has('version') else 'default'
            ver      = do_version(last,title)
            # print(last,ver)
            continue

        # --- Check if we got a version header ---
        # if cur is None:
        #     raise RuntimeError(f'No current version')

        # --- Get the file information ---
        db = {k: str(tm.get(k)).replace(f'{k}=','').replace('\u200e','')
              for k in
              ['filename',
               'decription',    
               'date',         # These three fields 
               'size',         # are largely redundant and will be 
               'compatibility' # taken from file information 
               ]
              if tm.has(k)}
        
        db['maintainers'] = do_emails(str(tm.get('maintainer'))
                                      if tm.has('maintainer') else '')
        db['contributors'] = do_emails(str(tm.get('contributors')
                                           if tm.has('contributors') else ''))
        if len(db['maintainers']) <= 0 and maintainers is not None:
            db['maintainers'] = maintainers
        if len(db['contributors']) <= 0 and contributors is not None:
            db['contributors'] = contributors
            

        # --- Get the main author ---
        author = db['maintainers'][0] \
            if len(db['maintainers'])>0 else main

        # --- Get or create a new project (named by maintainer) ---
        addr = author['address']
        proj = projs.get(addr, None)
        if proj is None:
            proj        = {'user': author, 'packages': {}}
            projs[addr] = proj


        # --- Next package name ---
        pkg = last if ver == 'unknown' else 'default'
        if pkg not in proj['packages']:
            # print(f'New package {pkg}')
            proj['packages'][pkg] = []

        # --- Add file information to package ---
        proj['packages'][pkg].append(db)

    # --- Remove templates ---
    for tm in tmpl:
        code.replace(tm, '')
        
    return projs

# --------------------------------------------------------------------
def do_gallery(code,verbose=False):
    if verbose:
        print(f'Do gallery')
        
    tags = code.filter_tags(matches = lambda n: n.tag == 'gallery')

    if not tags:
        return []

    def extract(e):
        fields = e.split('|')
        img    = fields[0].replace('Image:','')
        alt    = '' if len(fields) < 2 else fields[1]

        return {'img': img, 'alt': alt}
            
    ret = [
        extract(e)
        for tag in tags
        for e in tag.contents.split('\n')
        if e != ''
    ]
    for tag in tags:
        code.replace(tag, '')

    return ret

# --------------------------------------------------------------------
def do_players(code,verbose=False):
    tags = code.filter_tags(matches = lambda n: n.tag == 'div')

    if not tags:
        return []

    ret = [
        do_emails(tag.contents) for tag in tags
        if tag.contents != ''
    ]

    for tag in tags:
        code.replace(tag, '')

    return ret

# --------------------------------------------------------------------
def do_readme(code,verbose=False):
    from tempfile import mkstemp
    from subprocess import Popen, PIPE
    from os import unlink
    

    tmp, tmpnam = mkstemp(text=True)
    with open(tmp,'w') as tmpfile:
        tmpfile.write(str(code))

    cmd = ['pandoc',
           '--from', 'mediawiki',
           '--to', 'markdown-simple_tables',
           tmpnam]
    out,err = Popen(cmd, stdout=PIPE,stderr=PIPE).communicate()

    unlink(tmpnam)

    return out.decode().replace(r'\|}','').replace(r'\_\_NOTOC\_\_','')

# --------------------------------------------------------------------
def do_remove_headings(code):
    to_remove = [
        'Comments',
        'Module Information',
        'Files',
        'Players',
        'Screen Shots',
        'Screenshots'
    ]

    headings =  code.filter_headings(matches='|'.join(to_remove))
    for h in headings:
        code.remove(h)

    return code


# --------------------------------------------------------------------
def do_remove_cruft(code):
    try:
        code.remove('__NOTOC__')
    except ValueError:
        pass

    secs = code.get_sections(matches='Files')
    for sec in secs:
        try:
            sec.remove('|}')
        except ValueError:
            pass

    return code

# --------------------------------------------------------------------
def do_print_item(d,indent='',prefix=''):
    if isinstance(d,dict):
        #print('')
        do_print_dict(d,indent+' ')
    elif isinstance(d,list):
        #print('')
        do_print_list(d,indent+' ')
    else:
        print(f'{indent}{prefix}{d}',end='')
    
# --------------------------------------------------------------------
def do_print_list(db,indent=''):
    print('')
    for v in db:
        do_print_item(v,indent,'-')
    print('')

# --------------------------------------------------------------------
def do_print_dict(db,indent='',prefix=''):
    print('')
    for k,d in db.items():
        print(f"{indent}{prefix}'{k}':",end='')
        do_print_item(d,indent)
        #print('')
            
# --------------------------------------------------------------------
def do_print(db,indent=''):
    do_print_item(db,indent)
    print('')
    
# --------------------------------------------------------------------
def convert(inp,md,js,verbose=False):
    from pprint import pprint
    from mwparserfromhell import parse
    from json import load
    
    content  = load(inp)
    title    = content['parse']['title']
    pageid   = content['parse']['pageid']
    text     = content['parse']['wikitext']['*']
    if text.startswith('#REDIRECT'):
        return
    if 'Category:DeleteMe' in text:
        return
    if 'Category:Banned' in text:
        return
    # print(text)
    
    code     = parse(text)
    gameinfo = do_gameinfo(code,title,inp.name,verbose)
    modules  = do_modules(code,title,verbose)
    gallery  = do_gallery(code,verbose)
    players  = do_players(code,verbose)
    code     = do_remove_cruft(code)
    code     = do_remove_headings(code)
    readme   = do_readme(code,verbose)

    owners   = {}
    if modules is not None:
        for email, project in modules.items():
            maintainer = project['user']
            if maintainer['address'] not in owners:
                owners[maintainer['address']] = maintainer['name']
        
            # pprint(project['packages'])
            for name, package in project['packages'].items():
                for file in package:
                    for maintainer in file['maintainers']:
                        if maintainer['address'] not in owners:
                            owners[maintainer['address']] = maintainer['name']
                    for contributor in file['contributors']:
                        if contributor['address'] not in owners:
                            owners[contributor['address']] = contributor['name']
        
    db       = { title.replace('Module:','') :
                 { 'game': gameinfo,
                   'contributors': [{'name': v, 'address': e}
                                    for e, v in owners.items()],
                   'projects': modules,
                   'gallery': gallery,
                   'players': players } }
    # do_print(db)#,compact=True,width=80)
    # pprint(db)
    
    
    js.write(dumps(db,indent=2))
    md.write(readme)

# --------------------------------------------------------------------
def do_ensure_output(file,default):
    '''Check that file is open.
    If not, make sure it exists, and then open it
    '''
    from pathlib import Path
    if file is not None:
        return file

    return open(default,'w')

# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pathlib import Path
    
    ap = ArgumentParser(description='Convert')
    ap.add_argument('input',type=FileType('r'),
                    help='Input media wiki')
    ap.add_argument('readme',type=FileType('w'),
                    help='Output markdow',nargs='?',default=None)
    ap.add_argument('json',type=FileType('w'),
                    help='Output JSON',nargs='?',default=None)


    args = ap.parse_args()

    path = Path(args.input.name)

    args.readme = do_ensure_output(args.readme,path.with_suffix('.md'))
    args.json   = do_ensure_output(args.json,  path.with_suffix('.json'))

    #try:
    convert(args.input,args.readme,args.json)
    #except Exception as e:
    #    from sys import stderr 
    #    print(f'When processing {args.input.name}: {e}',file=stderr)



