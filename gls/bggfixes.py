#!/usr/bin/env python


def do_one(bgg,js):
    from json import loads
    
    with open(bgg,'r') as b:
        l = b.read()
        if len(l) > 0:
            return {}

    with open(js,'r') as j:
        b = j.read()
        if len(b) <= 0:
            return {}
        d = loads(b)

    nam = list(d.keys())[0]
    return {nam: {
        'year': d[nam]['game']['year'],
        'new': '',
        'iden': 0,
        'exact': False
    }}

def do_all():
    ret = {}

    from pathlib import Path

    pathlist = Path('.').glob('*.json')
    for path in pathlist:
        ret.update(do_one(path.with_suffix('.bgg'),path))

    #from pprint import pprint
    #pprint(ret)
    from json import dump
    with open('missing.db','w') as m:
        dump(ret,m,indent=2,sort_keys=True)

if __name__ == '__main__':
    do_all()

    
