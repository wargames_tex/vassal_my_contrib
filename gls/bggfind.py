#!/usr/bin/env python


def do_one(bgg,js,md):
    from json import loads
    from re import match
    
    with open(bgg,'r') as b:
        l = b.read()
        if len(l) > 0:
            return {}

    with open(js,'r') as j:
        b = j.read()
        if len(b) <= 0:
            return {}
        d = loads(b)

    nam = list(d.keys())[0]
    with open(md,'r') as m:
        for l in m.readlines():
            m = match(r'.*https://boardgamegeek.com/boardgame/([0-9]+)/',l)
            if not m:
                continue

            print(f'{js} - {m[1]:8s} - {nam}')


def do_all():
    ret = {}

    from pathlib import Path

    pathlist = Path('.').glob('*.json')
    for path in pathlist:
        do_one(path.with_suffix('.bgg'),path,
               path.with_suffix('.md'))

    #from pprint import pprint
    #pprint(ret)
    from json import dump
    with open('missing.db','w') as m:
        dump(ret,m,indent=2,sort_keys=True)

if __name__ == '__main__':
    do_all()

    
