#!/usr/bin/env python
'''This script will loop over files named in the input JSON file,
try to download modules, logs, saves, and extensions, and extract
information from the file.

Note that downloaded files are newer saved on disk.
  
If the file to download exists in local current directory, then the
information will be read from there and no download will be made. 

Files that failed to be downloaded are written to `to_download`.  Use this
with f.ex. `wget` to retrieve the files to local directory

    wget -i to_download

or

    cat to_download | parallel wget -nc -nv

or

    head -n 10 to_download | parallel --bar --color wget -nc -nv

 
The retrieved information is written to a new JSON file `files_meta.json`.
Entries that are already present in the new file will be skipped, as
will files that are not a module, log, save, or extension.

The script will also try to parse version information from the
data and format that version into a semantic version string.  If the
parsing fails, then an entry is written to another JSON file
`files_override.json`.   The user can edit that file to correct the
version number.

If the script finds a corrected version number in `files_override.json`,
then it will use that semantic version number.

Thus:
- Files that lack information or cannot be retrieved can manually
  be added to `files_meta.json` and the script will use that manual
  entry
- Version numbers that cannot be parsed can manually be added to
  `files_override.json` and the script will use that entry.

The stored information in written JSON file `files_meta.json` contains
- name
- version
- semantic version
- file size in bytes
- Vassal version (where applicable)
- description
- ... and so on

The script can be executed multiple times, setting an increasing limit on the
number of downloads (option `-n`).   In that way, not everything needs to be
done in one go. 
'''
# --------------------------------------------------------------------
class MissingFile(Exception):
    def __init__(self,msg=''):
        super(MissingFile,self).__init__(msg)

# --------------------------------------------------------------------
class PossibleVirus(Exception):
    def __init__(self,msg=''):
        super(PossibleVirus,self).__init__(msg)
        
# --------------------------------------------------------------------
def do_check_version(version,raw,fileName):
    '''Check a version number

    Parameters
    ----------
    version : str
        The version number to parse
    fileName : str
        The file we got the version number from

    Returns
    -------
    semVersion : str
        Semantic version number or "unknown"
    '''
    from semver import VersionInfo

    if not version or version == 'unknown':
        return 'unknown'
    
    try:
        return str(VersionInfo.parse(version))
    except Exception as e:
        print(f'{fileName}: Bad version "{raw}" ("{version}"): {e}')
     
    return 'unknown'

# --------------------------------------------------------------------
def do_version(version,fileName):
    '''Try to parse a version number

    Parameters
    ----------
    version : str
        The version number to parse
    fileName : str
        The file we got the version number from

    Returns
    -------
    semVersion : str
        Semantic version number or "unknown"
    '''
    from re import match, IGNORECASE
    from semver import VersionInfo

    # First, strip spaces 
    base = version.strip()
    if len(base) <= 0:
        return base

    # Pre-release defaults to empty
    pre  = ''
    
    # Strip leading stuff which isn't a number
    m = match(r'(^[^-0-9._]*) *(.*)',base)
    if m is not None and len(m.groups()) >= 1:
        pre  = ('-'+m[1].strip().replace('-','')) if len(m[1])>0 else ''
        base = m[2].strip()
        
    # Then see if we have a leading "version" 
    m = match(r'(v|version|ver) *([-0-9._ ]+.*)', base, IGNORECASE)
    if m is not None and len(m.groups()) > 1:
        base = m[2].strip()

    # Replace '-', '_', and ',' with '.', and strip leading/trailing whitespace
    base = base\
        .replace('_','.')\
        .replace('-','.')\
        .replace(',','.')\
        .strip()

    # See if we have something that looks like a release 
    m = match(r'([-0-9.]+)([^0-9.]+.*)',base)
    if m is not None and len(m.groups()) > 1:
        base = m[1].replace(' ','_')
        pre  = '-'+m[2].strip().replace('-','')

    # If the first character is a dot, prepend a zero 
    if base.startswith('.'):
        base = '0'+base

    # Now remove '.0' from string.  
    while True:
        m = match(r'(.*)\.0(.*)',base)
        if m is None: break
        base = '.'.join(m.groups())
        #print('->',base)

    # Remove leading and trailing dots,
    # replace double dots with a 0 inbetween 
    base = base.strip('.')
    base = base.replace('..','.0.')
    #print('=>',base)
    
    # Check how many dots we have and pad or remove as needed 
    if base.count('.') >= 3:
        spl  = base.split('.')
        #print('~',spl)
        pre  = '-'+'_'.join(spl[3:])
        base = '.'.join(spl[:3])
        #print('~~',base,pre)
    elif base.count('.') == 2:
        pass
    elif base.count('.') == 1:
        base += '.0'
    elif base.count('.') == 0:
        base += '.0.0'

    pre = pre\
        .replace(' ','')\
        .replace('(','')\
        .replace(')','')\
        .replace('+','')\
        .replace('_','')
    
    # Form our final estimate 
    test = base + pre

    # Try to parse as a semantic version number
    return do_check_version(test,version,fileName)


# --------------------------------------------------------------------
async def do_dom(input,filenames,size,fail=True):
    '''Process zip file and extract information from
    first file that matches one of the given file names

    Parameters
    -----------
    input : bytes
        The Zip file downloaded
    filenames : list of str
        List of file names to look for.  First found is used
    size : int
        Size of the input

    Returns
    -------
    doc : xml.minidom.Document
        Information read from XML file
    '''
    from zipfile import ZipFile
    from xml.dom.minidom import parseString
    from sys import stderr
    
    # Unzip the data 
    with ZipFile(input,'r') as zipIn:
        # See if we have one of the saught files 
        filename = None
        namelist = zipIn.namelist()
        for candidate in filenames:
            if candidate in namelist:
                filename = candidate
                break
        else:
            e = f'None of {",".join(filenames)} found'
            if fail:
                raise MissingFile(e)
            print(' '*(6+4+6),e,file=stderr)
            return None,size

        # Open the file and parse the XML 
        with zipIn.open(filename,'r') as file:
            doc = parseString(file.read())

    return doc,size

# --------------------------------------------------------------------
async def do_get(session,url,filenames,timeout,verbose=False,fail=True):
    '''Download a file

    Parameters
    ----------
    session : ClientSession
        Session to use
    url : str
        URL to download
    filenames : list of str
        Names of files to look for
    timeout : int
        Timeout in number of seconds 
    verbose : boolean
        Be verbose

    Returns
    -------
    dom : xml.minidom.Document
        Read XML document or None
    size : int
        Size of read data
    '''
    from io import BytesIO
    from sys import stderr
    from aiohttp import ClientTimeout
    from pathlib import Path
    from urllib.parse import urlparse
    from zipfile import BadZipFile
    time_out = ClientTimeout(total=timeout)

    parts = urlparse(url)
    path  = Path(Path(parts.path).name)
    if path.exists():
        if verbose:
            print(' '*(6+4+6),f'Reading local file {path}')
        with open(path,'rb') as file:
            content = file.read()
            size    = len(content)

    else:
        try:
            # Get the file with a timeout of 10 min
            async with  session.get(url,timeout=time_out) as ret:
                if ret.status != 200:
                    raise RuntimeError(f'Failed to get {url}')
                
                ctype   = ret.content_type
                # if verbose:
                #     print(f'{url} {ctype}')
                content = await ret.read()
                size    = len(content)
                #ret.close()

                if ctype not in ['application/zip',
                                 'application/java-archive']:
                    raise PossibleVirus(f'Not ZIP ({ctype}), possible virus')

        except PossibleVirus:
            raise
        except Exception as e:
            raise RuntimeError(f'Failed to download {url}: {e}') from e

    try:
        # Get the saught XML file and parse it 
        with BytesIO(content) as bytesIn:
            return await do_dom(bytesIn,filenames,size,fail)
    except PossibleVirus as e:
        raise
    except MissingFile as e:
        raise
    except BadZipFile as e:
        raise
    except Exception as e:
        raise RuntimeError(f'Failed to parse {url}: {e}') from e
            
    
# --------------------------------------------------------------------
async def do_vmod(session,url,timeout,verbose):
    '''Process a VMOD or extension

    Parameters
    ----------
    session : ClientSession
        Session to use
    url : str
        URL to download
    timeout : int
        Timeout in number of seconds 
    verbose : boolean
        Be verbose

    Returns
    -------
    db : dict
        Dictionary of read values
    size : int
        Size of downloaded file
    '''
    from sys import stderr
    from zipfile import BadZipFile
    
    try:
        doc,size = await do_get(session,
                                url,
                                ['buildFile','buildFile.xml'],
                                timeout,
                                verbose)

        attr = doc.childNodes[0].attributes
        if attr is None:
            print(doc.childNodes[0])
            attr = doc.childNodes[1].attributes
            # raise MissingFile('No attributes on top node')
        
        db = {k:v for k,v in attr.items()}
        db['size'] = size

        return db

    except BadZipFile as e:
        print(f'{url} is a bad ZIP file: {e}',file=stderr)
        return {'size': 0,
                'type': 'broken',
                'note': 'Bad zip file'}

    except MissingFile as e:
        print(f'{url} is missing file: {e}',file=stderr)
        return {'size': 0,
                'note': 'Missing file',
                'type': 'broken'}
    except PossibleVirus as e:
        print(f'{url} is possibly a virus: {e}',file=stderr)
        return {'size': 0,
                'type': 'possible virus',
                'note': 'Not ZIP file, possible VIRUS'}

# --------------------------------------------------------------------
async def do_tag(dom,name,default=''):
    '''Extract tag value (CTEXT) from DOM

    Parameters
    ----------
    dom : xml.minidom.Document
        Document
    name : str
        Name of tag(s) to process

    Returns
    -------
    db : dict
        Dictionary of name and value, or empty 
    '''
    if dom is None:
        return {name:default}
    
    elems = dom.getElementsByTagName(name)
    if elems is None: return {}
    if len(elems) != 1:
        print(f'Too many or few {name} elements')
        return {name:default}

    children = elems[0].childNodes
    if len(children) <= 0:
        print(f'Tag {name} has no children')
        return {name:default}

    if len(children) > 1:
        print(f'Tag {name} has too many children')

    if children[0].nodeType != children[0].TEXT_NODE:
        print(f'Child of {name} is not text')
        return {name:default}

    value = children[0].nodeValue.strip()

    return {name: value}
        
    
# --------------------------------------------------------------------
async def do_vlog(session,url,timeout,verbose):
    '''Process a downloaded log or save file

    Parameters
    ----------
    session : ClientSession
        Session to use
    url : str
        URL to download
    timeout : int
        Timeout in number of seconds 
    verbose : boolean
        Be verbose

    Returns
    -------
    db : dict
        Dictionary of read values
    size : int
        Size of downloaded file
    '''
    from sys import stderr
    try:
        doc,size = await do_get(session,
                                url,
                                ['moduledata','moduleData'],
                                timeout,
                                verbose,
                                fail=False)
        db  = {'size':size}
    
        for name,default in [('version'      , '0.0.0'),
                             ('VassalVersion', '0.0.0'),
                             ('dateSaved'    , '0'),
                             ('description'  , ''),
                             ('name'         , '')]:
            db.update(await do_tag(doc,name))

        return db
    
    except MissingFile as e:
        print(f'{url} is missing file: {e}',file=stderr)
        return {'size': 0,
                'type': 'broken',
                'note': 'Missing file'}
    except PossibleVirus as e:
        print(f'{url} is possibly a virus: {e}')
        return {'size': 0,
                'type': 'possible virus',
                'note': 'Not ZIP file, possible VIRUS'}
        

# --------------------------------------------------------------------
def pretty_size(size):
    '''Format size in human-readable format

    Parameters
    ----------
    size : int
        Size in bytes

    Returns
    -------
    text : str
        Size formatted
    '''
    for power,name in {30:'GB',20:'MB',10:'kB'}.items():
        if size > 2**power:
            return f'{size/2**power:.2f}{name}'

    return f'{size:d}B'

# --------------------------------------------------------------------
async def get_type(url):
    '''Deduce type of file from URL (by suffix) - not the best,
    but does save some downloads
    '''
    from urllib.parse import urlparse
    from pathlib import Path
    
    parts = urlparse(url)
    path  = Path(parts.path)
    types = {'.vmod': 'module',
             '.vmdx': 'extension',
             '.vext': 'extension',
             '.vlog': 'log',
             '.vsav': 'save'
             }

    # Specific overrides 
    if path.name in ['BOW_1_0.vmod',
                     'Monopanem_final_bueno_-copia.vmod',
                     'MoscowEmbVassSetUp.vmod',
                     'MosEmbVassSetupOct41.vmod'
                     ]:
        return types['.vsav']
    
    return types.get(path.suffix.lower(), 'other')
    
# --------------------------------------------------------------------
async def do_one(session,cnt,tot,fileSpec,data,typ,timeout,verbose):
    '''Get one file
    
    Parameters
    ----------
    session : ClientSession
        Session to use
    cnt : int
        current number of file
    tot : int
        Total number of file
    fileSpec : str
        File specifier used as key
    data : dict
        Existing data, if any
    verbose : boolean
        Be verbose

    Returns
    -------
    fileSpec : str
        Key
    db : dict
        Dictionary of values
    '''
    url = data['url']
    try:
        if typ in ['module', 'extension']:
            db   = await do_vmod(session,url,timeout,verbose)
            
        elif typ in ['log', 'save']:
            db  = await do_vlog(session,url,timeout,verbose)
            
        else:
            # if verbose:
            #     print(f'{cnt:6d} of {tot:6d} Skipping {fileSpec}')
            data['type'] = 'other'
            return fileSpec,data

        if len(db) <= 0:
            raise RuntimeError(f'When getting {url}: No data returned')

        if 'type' not in db:
            db['type'] = typ

        size               = db.get('size',0)
        if verbose:
            print(f'{cnt:6d} of {tot:6d} Get {fileSpec} => '
                  f'({pretty_size(size)})')

        data.update(db)

        return fileSpec,data
    
    except Exception as e:
        from sys import stderr
        print(f'{cnt:6d} of {tot:6d} When getting {fileSpec}: ',file=stderr)
        print(' '*(6+4+6),e,file=stderr)
        return fileSpec,None
    
# --------------------------------------------------------------------
async def do_all(input,max,existing,overrides,limit,timeout,verbose):
    '''Do all files (or up to maximum)
    
    Parameters
    ----------
    input : file
        File to read file entries from (JSON)
    max : int
        Maximum number of files to process
    existing : file
        File containing already parsed information (JSON)
    overrides : file
        File containing version overrides
    verbose : boolean
        Be verbose
    '''
    from asyncio import TaskGroup
    from json import load, dump
    from urllib.parse import urlparse
    from pathlib import Path
    from requests import Session
    from aiohttp import TCPConnector, ClientSession
    
    files    = load(input)
    tot      = len(files)
    try:
        ret  = load(existing)
    except Exception as e:
        print(f'Failed to read existing files',e)
        return
        #ret  = {}
    try:
        over = load(overrides)
    except Exception as e:
        print(f'Failed to read overloads',e)
        return
        #over = {}
    if len(ret) <= 0:
        print(f'No exising files')
    if verbose:
        print(f'Got {len(ret)} existing entries, out of {tot}')

    to_get = {}
    for cnt, (fileSpec, data) in enumerate(files.items()):
        if max > 0 and cnt >= max:
            break

        # See if we already processed this file 
        if fileSpec in ret:
            data = ret[fileSpec]
            # if verbose:
            #     print(f'{cnt:6d} of {tot:6d} Skipping {fileSpec}')
            continue

        typ = await get_type(data['url'])
        data['type'] = typ
        if not typ in ['module','extension','save','log']:
            # if verbose:
            #     print(f'{cnt:6d} of {tot:6d} Skipping {fileSpec}')
            continue

        # print(f'{typ:6s}: {data["url"]}')
        to_get[fileSpec] = data

    nget = len(to_get)
    conn = TCPConnector(limit=limit)
    async with ClientSession(connector=conn) as session:
        #session = Session()

        # Create tasks 
        tasks = []        
        async with TaskGroup() as tg:
            for cnt, (fileSpec, data) in enumerate(to_get.items()):
                # if cnt >= max:
                #     break
                # 
                # # See if we already processed this file 
                # if fileSpec in ret:
                #     data = ret[fileSpec]
                #     # if verbose:
                #     #     print(f'{cnt:6d} of {tot:6d} Skipping {fileSpec}')
                # 
                #     continue
                # 
                # typ = await get_type(data['url'])
                # if not typ in ['module','extension','save','log']:
                #     # if verbose:
                #     #     print(f'{cnt:6d} of {tot:6d} Skipping {fileSpec}')
                #         
                #     data['type'] = typ
                #     continue
                    
                
                if verbose:
                    print(f'{cnt:6d} of {nget:6d} Will download {data["url"]}')
                # Create task to download and investigate efile
                typ = data['type']
                tasks.append(tg.create_task(do_one(session,
                                                   cnt,
                                                   nget,
                                                   fileSpec,
                                                   data,
                                                   typ,
                                                   timeout,
                                                   verbose)))
            if verbose:
                print(f'A total of {len(tasks)} downloads')
            
        # Get data from tasks 
        fail = {}
        for task in tasks:
            fileSpec,data = task.result()
            if data is None:
                fail[fileSpec] = files.get(fileSpec,{}).get('url',None)
                continue
            
            ret[fileSpec] = data


    # Fill in semantic version were we do not have one
    badVers = []
    for fileSpec,data in ret.items():
        # check if we have an override of semantic version
        # If not, see if we have one already
        # If not, try to deduce it from version given
        # Otherwise, attach to overrides (not other files)
        typ           = data.get('type','other')
        if typ != 'other':
            vers          = data.get('version','')
            semVers       = over.get(fileSpec,{})\
                                .get('semVersion',
                                     data.get('semVersion','unknown'))
            semVers       = do_check_version(semVers,vers,fileSpec)
            
            if semVers == 'unknown':
                semVers = do_version(vers,fileSpec)

            if semVers != 'unknown':
                data['semVersion'] = semVers
            elif typ != 'other':
                badVers.append(fileSpec)
                over[fileSpec] = {'version': vers,
                                  'semVersion': 'unknown'}

            # if verbose:
            #     print(f'{fileSpec:60s} {vers:16s} -> {semVers}')
                

    # Friendly message about missing versions
    print(f'Out of {len(tasks)} {len(badVers)} had bad version')
    for f in badVers:
        print(f'- {f}')
        
    # Friendly message about failures 
    print(f'Out of {len(tasks)} ({max} of {tot}) {len(fail)} failed')
    for f in fail.keys():
        print(f'- {f}')

    if len(fail) > 0:
        urls = [u for u in fail.values() if u is not None]
        with open('to_download','w') as output:
            output.write('\n'.join(urls))
        print('Do\n\n  cat to_download | parallel --bar --color wget -nv -nc'
              '\n\nto download these files locally')

    # Write out processes files 
    outname = existing.name
    existing.close()
    with open(outname,'w') as output:
        dump(ret, output,indent=4,sort_keys=True)

    # Write out overrides 
    outname = overrides.name
    overrides.close()
    with open(outname,'w') as over_out:
        dump(over, over_out,indent=4,sort_keys=True)

# --------------------------------------------------------------------
def do_ensure_file(file,default):
    '''Check that file is open.
    If not, make sure it exists, and then open it
    '''
    from pathlib import Path
    if file is None:
        oname = default
    else:
        oname = file.name

    opath = Path(oname)
    if not opath.exists():
        with open(opath,'w') as ofile:
            ofile.write('{}')

    if not file:
        file = open(opath,'r')

    return file

# --------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType, RawDescriptionHelpFormatter
    from asyncio import run
    
    ap = ArgumentParser(description='Get file meta data',
                        formatter_class=RawDescriptionHelpFormatter,
                        epilog=__doc__)
    ap.add_argument('input',help='Input JSON',type=FileType('r'))
    ap.add_argument('output',help='Output JSON',type=FileType('r'),
                    nargs='?',default=None)
    ap.add_argument('overrides',help='Version overrides',type=FileType('r'),
                    nargs='?',default=None)
    ap.add_argument('-m','--max',type=int,default=0,
                    help='Maximum number of files to get')
    ap.add_argument('-v','--verbose',action='store_true',
                    help='Be verbose')
    ap.add_argument('-n','--limit',type=int,
                    help='Maximum number of connections (0 means unlimited)',
                    default=0)
    ap.add_argument('-t','--timeout',type=int,
                    help='Timeout in number of seconds',default=10*60)

    args = ap.parse_args()

    args.output    = do_ensure_file(args.output,'files_meta.json')
    args.overrides = do_ensure_file(args.overrides,'files_override.json')

    run(do_all(args.input,
               args.max,
               args.output,
               args.overrides,
               args.limit,
               args.timeout,
               args.verbose))
                    
