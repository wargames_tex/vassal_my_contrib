#!/usr/bin/env python

# --------------------------------------------------------------------
def open_users(filename='users.json',verbose=False):
    '''Open the users file and return read list'''
    from json import load

    if verbose:
        print(f'Reading users from {filename}')
        
    with open(filename,'r') as file:
        return load(file)


# --------------------------------------------------------------------
def make_user_db(l,verbose=False):
    '''Make a dictionary that maps id to name, email, user-name'''
    if verbose:
        fld = ['name','email','username']
        err = {u['id']:','.join([k for k in fld
                                 if u[k] == '']) for u in l}
        msg = '\n'.join([f'- {iden:6d} missing {val:8s} '+
                         '\t'.join([u[k] for k in fld])
                         for (iden,val),u in zip(err.items(),l) if val != ''])
        print(msg)
               
    return {int(u['id']):{
        'name':u['name'],
        'email':u['email'].lower(),
        'user':u['username'].lower()}
           for u in l}

        

# --------------------------------------------------------------------
def map_user_by_email(db,verbose=False):
    '''Make a dictionary that maps email to id'''
    return {u['email']: k for k,u in db.items() }

# --------------------------------------------------------------------
def map_user_by_user(db,verbose=False):
    '''Make a dictionary that maps user-name to id'''
    return {u['user']: k for k, u in db.items() }

# --------------------------------------------------------------------
def get_users(filename='users.json',verbose=False):
    '''Get database of users, map from email, map from user-name'''
    l  = open_users       (filename,verbose)
    db = make_user_db     (l,       verbose)
    be = map_user_by_email(db,      verbose)
    bu = map_user_by_user (db,      verbose)
    bu['pdietrich'] = bu['pdietrich2']
    bu['fallback']  = bu['uckelman']
    be['fallback']  = be['uckelman@nomic.net']
    
    return db, be, bu

# ====================================================================
def open_files(filename='files_meta.json',verbose=False):
    '''Read in file information from JSON'''
    from json import load

    if verbose:
        print(f'Open "{filename}"')
    with open(filename,'r') as inp:
        return load(inp)

# --------------------------------------------------------------------
def make_file_db(d, user_to_id, offset=1,verbose=False):
    '''Create the file database, and return it,
    plus short file name to ID map'''
    from urllib.parse import urlparse
    from pathlib import Path
    
    db   = {}
    s2id = {}
    u2id = {}
    unknown = {}
    
    for i, (k, fd) in enumerate(d.items()):
        short = k.replace('File:','')
        url   = fd['url']
        typ   = fd.get('type','')
        usr   = fd['user'].lower().replace(' ','_')
        vsl   = fd.get('VassalVersion','')
        ver   = fd.get('semVersion','')
        siz   = fd.get('size',0)
        fid   = i + offset
        uid   = user_to_id.get(usr,-1)

        upa   = Path(urlparse(url).path).name
        
        if uid < 0:
            if verbose:
                print(f'{short} has unknown user: {usr}')
            unknown[usr] = unknown.get(usr,0)+1
            # Assign to Joel Uckelman (admin)
            uid          = user_to_id.get('fallback',usr)
            
        if typ == '':
            if short.endswith('.vsav'):
                typ = 'save'
            else:
                if verbose:
                    print(f'{short} has no type')

        db[fid] = {'url': url,
                   'version': ver,
                   'vassal_version': vsl,
                   'type': typ,
                   'size': siz,
                   'user_id': uid }
        s2id[short] = fid
        u2id[upa]   = fid

    if verbose:
        print(f'Got {len(unknown)} unknown user names')
        for u,c in sorted(unknown.items()):
            print(f'- {u:24s}: {c:3d}')
        
    return db, s2id, u2id       

    
# --------------------------------------------------------------------
def get_files(filename,user_to_id,verbose=False):
    '''Do the files stuff'''
    d = open_files(filename,verbose=verbose)
    db, s2id, u2id = make_file_db(d,user_to_id,verbose)

    return db, s2id, u2id

# ====================================================================
def open_pages(filename='pages_bgg.json',verbose=False):
    from json import load

    if verbose:
        print(f'Reading in pages from "{filename}"')
    with open(filename,'r') as inp:
        return load(inp)

# --------------------------------------------------------------------
def find_or_add(name,name_to_id,db,verbose=False):
    if not name:
        return None

    name  = name.strip().strip('\n').strip('\t').replace('  ',' ')
    lname = name.lower()
    iden  = name_to_id.get(lname,None)
    if iden is None:
        iden              = len(db)+1
        name_to_id[lname] = iden
        db[iden]          = []

        if verbose:
            print(f'Adding {iden:6d}={name}')

    db[iden].append(name)
    
    return iden

# --------------------------------------------------------------------
def parse_range(what):
    for sep in ['-', ' to ']:
        parts = what.split(sep)
        if len(parts) <= 1:
            continue
        
        min = parts[0].strip()
        max = parts[1].strip()
        try:
            min = int(min)
            max = int(max)
            break
        except:
            pass
    else:
        min = 0
        max = 0
            
    return min,max

# --------------------------------------------------------------------
def add_game_tag(tag,d,tag_to_id,tags,verbose=False):
    val = d.get(tag,None)
    if val is None:
        return

    tval = tag.lower()+':'+val.strip()
    return find_or_add(tval,tag_to_id,tags,verbose=verbose)

# --------------------------------------------------------------------
def text_to_int(t):
    try:
        return int(t)
    except:
        pass
    return None

# --------------------------------------------------------------------
def file_id(name,name_to_id,url_to_id):
    if name.startswith('File:'): name = name[5:]
    return name_to_id.get(name,url_to_id.get(name,None))

# --------------------------------------------------------------------
def user_id(email,email_to_id):
    return email_to_id.get(email if email != 'unknown' else 'fallback',
                           email_to_id['fallback'])

# --------------------------------------------------------------------
def pub_name(name):
    override = {
        'Public Domain':             '(Public Domain)',
        'Self-Published':            '(Self-Published)',
        '(Looking for a publisher':  '(Unpublished)',
        '(Unknown)':                 '(Unpublished)',
        'unknown':                   '(Unpublished)',
        'unpublished':               '(Unpublished)',
        '2D6.EE':                    'Side Quest Games',
        '2D6.EE (Side Quest Games)': 'Side Quest Games',
        '3W':                        'World Wide Wargames',
        '3W (World Wide Wargames)':  'World Wide Wargames',
        '???':                                   '(Unpublished)',
        'ADC Blackfire Entertainment': 'ADC Blackfire Entertainment GmbH',
        'AMIGO': 'AMIGO Spiel + Freizeit GmbH',
        'Academy Games': 'Academy Games, Inc.',
        'Acies': 'Acies Edizioni',
        'Against the Odds': 'Against the Odds Magazine',
        'Albi': 'Albi Polska',
        'Amarillo Design Bureau': 'Amarillo Design Bureau, Inc.',
        'Asylum Games (Board Games)': 'Asylum Games',
        'Avalon Hill (Hasbro)': 'The Avalon Hill Game Co',
        'Avalon Hill': 'The Avalon Hill Game Co',
        'Azure Wish Enterprise (AWE)': 'Azure Wish Enterprise',
        'CEFA (Celulosa Fabril S. A.)': 'Celulosa Fabril S. A.',
        'CEFA': 'Celulosa Fabril S. A.',
        'Comand Post Games': 'Command Post Games',
        'Decision Games (I)': 'Decision Games',
        'Dynamic Games / Dynamic Design Industries': 'Dynamic Games',
        'E. S. Lowe': 'E. S. Lowe Company Inc.',
        'Flying Buffalo, Inc. (FBI)': 'Flying Buffalo, Inc.',
        'Game Designers Workshop':               "Game Designers' Workshop",
        "Game Designers' Workshop (GDW Games)":  "Game Designers' Workshop",
        'Game Journal':                          'Game Journal Magazine',
        'Games Factory':                         'Games Factory Publishing',
        'Gibsons': 'Gibsons Games',
        'Goliath Games': 'Goliath B.V.',
        'Griggling Games': 'Griggling Games, Inc.',
        'Grognard Simulations, Inc. (GSI)': 'Grognard Simulations, Inc.',
        'Hexagames (I)': 'Hexagames',
        'IV Studio (IV Games)': 'IV Games',
        'Ideal': 'Ideal Board Games',
        'Instytut Pamięci Narodowej (IPN) & Trefl':  'Instytut Pamięci Narodowej',
        'Instytut Pamięci Narodowej (IPN)':  'Instytut Pamięci Narodowej',
        'International Team (I)': 'International Team',
        'Waddington': 'John Waddington Ltd.',
        'Trefl': 'Instytut Pamięci Narodowej',
        'Just Games (I)': 'Just Games',
        'LPS': 'LPS, Inc.',
        'KYF Edition': 'KYF Editions',
        'Legion Wargames': 'Legion Wargames LLC',
        'Leonardo': 'Leonardo Games',
        "Lock 'n Load Publishing": "Lock 'n Load Publishing, LLC.",
        "Lock n Load Publishing": "Lock 'n Load Publishing, LLC.",        
        'Ludodelire': 'Ludodélire',
        'M.O.D. Games (UK Publisher)': 'M.O.D. Games',
        'MB spel': 'MB Spellen',
        'Mattel': 'Mattel, Inc.',
        'Monolith': 'Monolith Board Games',
        'No Turkeys': 'No Turkeys!',
        'OSG (Operational Studies Group)': 'Operational Studies Group',
        'Omega Games (I)': 'Omega Games',
        'PHALANX': 'Phalanx Games B.V.',
        'Phalanx Games': 'Phalanx Games B.V.',
        'Phalanx Games Deutschland': 'Phalanx Games B.V.',
        'Project Analysis': 'Project Analysis Corp.',
        'Ravensburger': 'Ravensburger Spieleverlag GmbH',
        'Revolution Games (I)':  'Revolution Games',
        'Revolution Games (II)': 'Revolution Games',
        'Roxley': 'Roxley Games',
        'SPI (Simulations Publications, Inc.)': 'Simulations Publications, Inc.',
        'Simulation Publications, Inc.': 'Simulations Publications, Inc.',
        'Tactical Studies Rules (TSR)': 'Tactical Studies Rules',
        'TSR': 'Tactical Studies Rules (TSR)',
        'Terran Games': 'Terran Games, Inc.',
        'The Game Crafter': 'The Game Crafter, LLC',
        'ThinkFun': 'ThinkFun Inc.',
        'Tomy Company, Ltd. (Takara Tomy)': 'Tomy Company, Ltd.',
        'Takara': 'Tomy Company, Ltd.',
        'Tomy': 'Tomy Company, Ltd.',
        'Tinderbox Games': 'Tinderbox Entertainment, LLC',
        'WizKids': 'WizKids Games',
        'WizKids (I)': 'WizKids Games',
        'Waddington': 'Waddington Sanders Ltd.',
        'Waddingtons': 'Waddington Sanders Ltd.',
        'Waddingtons Sanders Ltd': 'Waddington Sanders Ltd.',
        'Victory_Games': 'Victory Games',
        'Victory Games (I)': 'Victory Games',
        'Ventura Games': 'Ventura Inc.',
        'Vent0Nuovo Games': 'VentoNuovo Games',
        'Waddington': 'Waddington Sanders Ltd',
        'Wolf Fang P.H': 'Wolf Fang P.H.',
        'Worthington Games': 'Worthington Publishing, LLC',
        'Wargames Research Group': 'Wargames Research Centre Ltd.',
        'Two Hour Wargames (THW Game Design)': 'Two Hour Wargames',
        '2 Hour Wargames': 'Two Hour Wargames',
        'MultiMan Publishing': 'Multi-Man Publishing',
        'Smartass Games': 'Smartass Games Ltd.'
    }

    name = name.strip().strip('\n').strip('\t').replace('  ',' ')
    nam = override.get(name,name)
    if nam.lower().endswith('inc'): nam += '.'
    if nam.lower().endswith('ltd'): nam += '.'

    from re import match, IGNORECASE
    m = match(r'(.*[^,]) (Inc\.|LLC|Ltd\.)',nam,IGNORECASE)
    if m:
        nam = m[1]+', '+m[2]

    return nam

# --------------------------------------------------------------------
def do_game(iden,
            title,
            page,
            users,
            email_to_id,
            file_to_id,
            url_to_id,
            tag_to_id,
            pub_to_id,
            tags,
            publishers,
            verbose=False):


    game    = page.get('game', {})
    
    pub_id  = find_or_add(pub_name(game.get('publisher','(Unpublished)')),
                          pub_to_id, publishers,verbose)
    year    = game.get('year',   None)
    players = game.get('players',None)
    length  = game.get('length', None)
    age     = -1
    
    minplayers, maxplayers = parse_range(players)
    mintime,    maxtime    = parse_range(length)
    
    gtags  = [add_game_tag(tag,game,tag_to_id,tags,verbose)
              for tag in ['era','topic','series','scale']]
    bgg    = page.get('bgg',None)
    if not bgg:
        bid    = -1
        bdesc  = ''
        btitle = ''
        btags  = []
        bpub   = []
        if verbose:
            print(f'No BGG identifier for {title}')
    
        
    else:
        bid         = text_to_int(bgg.get('objectid',None))
        byear       = text_to_int(bgg.get('yearpublished',{}).get('text',None))
        bminplayers = text_to_int(bgg.get('minplayers',{})   .get('text',None))
        bmaxplayers = text_to_int(bgg.get('maxplayers',{})   .get('text',None))
        bmintime    = text_to_int(bgg.get('minplaytime',{})  .get('text',None))
        bmaxtime    = text_to_int(bgg.get('maxplaytime',{})  .get('text',None))
        bage        = text_to_int(bgg.get('age',{})          .get('text',-1))
        blength     = bgg.get('playingtime',None)
        btitles     = bgg.get('name',None)
        btitle      = ''
        if btitles:
            if isinstance(btitles,list):
                btitles = [t['text'] for t in btitles
                             if t.get('primary',False)]
                btitle  = btitles[0]
            else:
                btitle = btitles['text']
        
        def get(name):
            d = bgg.get(name,[])
            return [d] if isinstance(d,dict) else d

        v           = verbose
        bdesc       =  bgg.get('description',None)        
        bpub        =  [find_or_add(pub_name(p['text']),pub_to_id,publishers,v)
                        for p in get('boardgamepublisher')]
        btags       =  [find_or_add('category:'+c['text'],tag_to_id,tags,v)
                        for c in get('boardgamecategory')]
        btags       += [find_or_add('domain:'+d['text'],tag_to_id,tags,v)
                        for d in get('boardgamesubdomain')]
        btags       += [find_or_add('family:'+d['text'],tag_to_id,tags,v)
                        for d in get('boardgamesubdomain')]
        btags       += [find_or_add('mechanic:'+m['text'],tag_to_id,tags,v)
                        for m in get('boardgamemechanic')]
        btags       += [find_or_add('mechanic:'+m['text'],tag_to_id,tags,v)
                        for m in get('boardgamemechanic')]
        btags = [t for t in btags if t is not None]

        # Trust BGG more than VASSAL pages for year, number of
        # players, and playing time.
        if byear       is not None:     year       = byear
        if bminplayers is not None:     minplayers = bminplayers            
        if bmaxplayers is not None:     maxplayers = bmaxplayers            
        if bmintime    is not None:     mintime    = bmintime               
        if bmaxtime    is not None:     maxtime    = bmaxtime
        if bage        is not None:     age        = bage
        if blength     is not None:
            bl = text_to_int(blength)
            if bl is not None:
                length = f'{bl} minutes'

    pl      = page.get('players',[])
    if pl and isinstance(pl[0],list): pl = pl[0]

    def clean(l,test=lambda e: e is not None):
        return [e for e in l if test(e)]

    if age > 0:
        gtags.append(find_or_add(f'age:{age}',tag_to_id,tags,v))
    
    readme  = page.get('readme', None)
    contrib = clean([email_to_id.get(c.get('address',None),None)
                     for c in page.get('contributors',[])
                     if c.get('address','unknown') != 'unknown'])
    players = clean([email_to_id.get(p.get('address',None),None) for p in pl])
    gallery = clean([(file_id(i.get('img',None),file_to_id,url_to_id),
                      i.get('alt',None))
                     for i in page.get('gallery',[])],
                    test=lambda e : e[0] is not None)

    tags   = clean(gtags)+clean(btags)
    pub    = clean(set([pub_id]+bpub))

    game   = {iden:
              { 'title':           title,
                'image':           page.get('image',''),
                'publisher':       pub,
                'year':            year,
                'bgg_id':          bid,
                'min_players':     minplayers,
                'max_players':     maxplayers,
                'min_time':        mintime,
                'max_time':        maxtime,
                'bgg_title':       btitle,
                'bgg_description': bdesc,
                'contributors':    contrib,
                'tags':            tags,
                'readme':          readme,
                'gallery':         gallery,
                'players':         players} }

    projects = {}
    packages = {}
    gproj    = page.get('projects',{})
    if not gproj:
        return game, packages, projects
        
    for i,(name,project) in enumerate(gproj.items()):
        pid  = iden+100000*(i+1)
        uid  = user_id(project['user']['address'],email_to_id)
        unm  = project['user']['name']
        # print(uid,project['user']['address'])
        nam  = users.get(uid,{}).get('name',unm)
        if nam is None: nam = unm
        if nam is None: nam = 'Undefined'
        projects[pid] = {'game_id':     iden,
                         'user_id':     uid,
                         'name':        nam,
                         'description': '',
                         'tags':        ''}


        ppack = project.get('packages',{})
        if not ppack:
            if verbose:
                print(f'Project {nam} of {title} has no packages')
            continue
        
        for j,(name,files) in enumerate(ppack.items()):
            qid = pid+1000000*(j+1)

            mains = set([user_id(main['address'],email_to_id)
                         for file in files for main in file['maintainers']])
            cons  = set([user_id(cont['address'],email_to_id)
                         for file in files for cont in file['contributors']])
            fid   = clean([file_id(file['filename'],file_to_id,url_to_id)
                           for file in files])
            if len(mains) <= 0:
                mains = {user_id('fallback',email_to_id)}

            mains = mains-{uid}
            cons  = cons - mains - {uid}
                
            if len(mains) > 1 and verbose:
                print(f'Package {qid:9d} ({title}/{nam}) has '
                      f'{len(mains)} maintainers')
            if len(fid) < 1 and verbose:
                print(f'Package {qid:9d} ({title}/{nam}) has no files')
            
            packages[qid] = {'project_id': pid,
                             'maintainers':mains,
                             'contributors':cons,
                             'files': fid}
                            

    return game, projects, packages
                        
              
    
# --------------------------------------------------------------------
def make_game_db(pages,
                 users,
                 email_to_id,
                 file_to_id,
                 url_to_id,
                 offset=1,
                 verbose=False):

    games      = {}
    publishers = {}
    tags       = {}
    tag2id     = {}
    pub2id     = {}
    projects   = {}
    packages   = {}
    
    for i,(title,page) in enumerate(pages.items()):
        game,gproj,gpack = do_game(i+offset,
                                   title,
                                   page,
                                   users,
                                   email_to_id,
                                   file_to_id,
                                   url_to_id,
                                   tag2id,
                                   pub2id,
                                   tags,
                                   publishers,
                                   verbose)
        games   .update(game)
        projects.update(gproj)
        packages.update(gpack)

    return games, projects, packages, publishers, tags
    
# --------------------------------------------------------------------
def get_pages(filename,users,email_to_id,file_to_id,url_to_id,verbose=False):
    pages = open_pages(filename,verbose)

    return make_game_db(pages,
                        users,
                        email_to_id,
                        file_to_id,
                        url_to_id,
                        verbose=verbose)

    
# ====================================================================
def do_temp(verbose=False):
    udb, ue2id, uu2id          = get_users('users.json',
                                           verbose=verbose)
    fdb, fs2id, fu2id          = get_files('files_meta.json',
                                           uu2id,verbose=verbose)
    gdb, prdb, pjdb, pbdb, tdb = get_pages('pages_bgg.json',
                                           udb,ue2id,fs2id,fu2id,
                                           verbose=verbose)

    return udb, fdb, gdb, prdb, pjdb, pbdb, tdb


# --------------------------------------------------------------------
def do_majority(l):
    from numpy import unique
    u, c = unique(l,return_counts=True)
    return u[c.argmax()]

# --------------------------------------------------------------------
def do_tables(verbose=False):
    users, files, games, projects, packages, publishers, tags = \
        do_temp(verbose)

    # First, set identifiers on projects and packages
    game_table                = []  # id,name,year,publisher,
                                    # bgg_id,bgg_name,image,
                                    # min_players,max_players
                                    # min_time,max_time
                                    # readme,bgg_description
    game_tag_table            = []  # game,tag
    game_player_table         = []  # game,user
    game_contributor_table    = []  # game,user
    game_image_table          = []  # game,file,alt
    game_publisher_table      = []  # game,publisher
    project_table             = []  # id,game,owner,name,description
    project_tag_table         = []  # project,tag
    package_file_table        = []  # package,file
    package_table             = []  # id,project,description
    package_contributor_table = []  # package,user
    package_maintainer_table  = []  # package,user
    publisher_table           = [[iden,
                                  do_majority(name)]
                                 for iden,name in publishers.items()] # id,name
    tag_table                 = [[iden,
                                  do_majority(text)]
                                 for iden,text in tags.items()]       # id,text
    user_table                = [[iden,
                                  u['user'],
                                  u['email'],
                                  u['name']]
                                 for iden,u in users.items()] # id,user,
                                                              # email,name
    file_table                = [[iden,
                                  f['type'],
                                  f['url'],
                                  f['version'],
                                  f['vassal_version'],
                                  f['size'],
                                  f['user_id']]
                                 for iden,f in files.items()] # id,type
                                                              # url,version
                                                              # vassal
                                                              # size
                                                              # user
    project_map               = {}

    # Re-do project IDs
    for no, (iden,project) in enumerate(projects.items()):
        project_id  = no+1
        old_id      = iden
        user_id     = project['user_id']
        game_id     = project['game_id']
        name        = project['name']
        description = project['description']
        tags        = project['tags']
        
        project_map[old_id] = project_id
        project_table.append([project_id,game_id,user_id,name,description])

        for tag_id in tags:
            project_tag_table.append([project_id,tag_id])

    # Re-do package IDs, update project IDs, do project->file table
    # Do project->contributor (user), project->maintainer (user) 
    for no, (iden,package) in enumerate(packages.items()):
        package_id = no+1
        project_id = project_map.get(package['project_id'],-1)
        if project_id < 0:
            print(f'Package {iden}->{package_id} has no project!')
            continue
        
        package_table.append([package_id,project_id,''])

        for maintainer_id in package['maintainers']:
            package_maintainer_table.append([package_id,maintainer_id])

        for contributor_id in package['contributors']:
            package_contributor_table.append([package_id,contributor_id])

        for file_id in package['files']:
            package_file_table.append([package_id,file_id])

    # id,name,publisher,bgg_id,bgg_title,image,
    # min_players,max_players
    # min_time,max_time
    # readme,bgg_description
    #
    # Do games
    for game_id,game in games.items():
        name      = game['title']
        year      = game['year']
        bgg_id    = game['bgg_id']
        bgg_name  = game['bgg_title']
        image     = game['image']
        min_play  = game['min_players']
        max_play  = game['max_players']
        min_time  = game['min_time']
        max_time  = game['max_time']
        bgg_desc  = game['bgg_description']
        readme    = game['readme']
        
        game_table.append([game_id,name,year,bgg_id,bgg_name,image,
                           min_play, max_play,min_time,max_time,
                           readme,bgg_desc])

        for pub_id in game['publisher']:
            game_publisher_table.append([game_id,pub_id])

        for tag_id in game['tags']:
            game_tag_table.append([game_id,tag_id])

        for player_id in game['players']:
            game_player_table.append([game_id,player_id])

        for contributor_id in game['contributors']:
            game_contributor_table.append([game_id,contributor_id])

        for image_id,alt in game['gallery']:
            game_image_table.append([game_id,image_id,alt])

    return {'users':                user_table,
            'files':                file_table,
            'publishers':           publisher_table,
            'tags':                 tag_table,
            'games':	            game_table,                
            'game_tag':	            game_tag_table,            
            'game_player':	    game_player_table,         
            'game_contributor':	    game_contributor_table,    
            'game_image':	    game_image_table,
            'game_publisher':       game_publisher_table,
            'projects':	            project_table,             
            'project_tag':          project_tag_table,         
            'package_file':         package_file_table,        
            'packages':	            package_table,             
            'package_contributor':  package_contributor_table, 
            'package_maintainer':   package_maintainer_table
            }



if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from json import dump
    
    ap = ArgumentParser(description='Create tables')
    ap.add_argument('output',type=FileType('w'),
                    help='Output file to write',
                    default='tables.json',nargs='?')
    ap.add_argument('-v','--verbose',action='store_true',
                    help='Be verbose')


    args = ap.parse_args()

    tables = do_tables(args.verbose)

    dump(tables, args.output,indent=2)
    

    
