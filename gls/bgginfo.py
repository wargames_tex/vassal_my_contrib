#!/usr/bin/env python
#
# see also
#
#   https://boardgamegeek.com/wiki/page/BGG_XML_API2
#   https://boardgamegeek.com/thread/2374741/json-api
#
#
# for i in *.bgg ; do \
#   if test -s $i ; then continue ; fi ; \
#   j=`basename $i .bgg`.json ; \
#   if test -s $j ; then \
#     t=`head -n 2 $j | tail -n 1 | tr -d '"' | sed -e 's/^ *//' -e 's/: {//'`;
#     y=`grep year $j | sed 's/"year": "//' | tr -d '",'`; \
#     echo ../bgginfo.py -n "\"$t\"" -y $y --any -o $i; \
#   fi ; \
# done
#

# --------------------------------------------------------------------
def request(query,base='https://boardgamegeek.com/xmlapi/'):
    '''Do a request against BGG API

    Parameters
    ----------
    query : str
        Query string - need to be URL encoded

    Returns
    -------
    response : str
        Response text (XML)
    '''
    from requests import get
    
    url = base + query
    response = get(url)
    return response.text

# --------------------------------------------------------------------
def request_new(query,base='https://boardgamegeek.com/xmlapi/'):
    '''Do a request against BGG API

    Parameters
    ----------
    query : str
        Query string - need to be URL encoded

    Returns
    -------
    response : str
        Response text (XML)
    '''
    from urllib.request import urlopen

    #print(base+query)
    with urlopen(base + query) as f:
        s = f.read()
        #print(s)
        return s.decode()

# --------------------------------------------------------------------
def parse(text):
    '''Parse XML reponse text and return as dictonary

    Parameters
    ----------
    text : str
        XML response text

    Returns
    -------
    db : dict 
        Dictionary of reponse
    '''
    from xml.dom.minidom import parseString

    doc = parseString(text)
    top = doc.documentElement
    
    ret = {top.tagName:parseNode(top)}
    #print(ret)
    return ret['boardgames']

# --------------------------------------------------------------------
def parseNode(node):
    '''Parse a single node and recurse

    Node attributes are stored in returned dictionary.

    Child nodes are stored as dictionaries.  In case there's more than
    one child node of a particular type, then those nodes are stored
    in a list.

    Parameters
    ----------
    node : xml.dom.minidom.Node
        Node to parse

    Return
    ------
    db : dict
        Dictionary of attributes and children
    '''
    from xml.dom.minidom import Text, Document

    ret   = {}
    attrs = node.attributes
    for ind in range(len(attrs)):
        attribute = attrs.item(ind)
        ret[attribute.name] = attribute.value
        
    for child in node.childNodes:
        if isinstance(child,Text):
            data = child.data.strip('\t').strip('\n')
            if len(data) > 0:
                ret['text'] = data
            continue

        dct = parseNode(child)
        tag = child.tagName
        if tag in ret:
            if isinstance(ret[tag],dict):
                old = ret[tag]
                ret[tag] = [old]
            ret[tag].append(dct)
        else:
            ret[tag] = dct

    return ret


# --------------------------------------------------------------------
def get_info(name,iden,inp,year=None,exact=True,missing={}):
    '''Get information about a game, by name or identifier

    Names are looked up.  If year is given, then filter by year.  If
    exact is true, then match names exactly.

    Parameters
    ----------
    iden : int
        Game identifier
    name : str
        Name of game
    year : int
        Optional year to narrow searces with
    exact : boolean
        If true, match exact name

    Return
    ------
    db : dict 
        Dictionary of game
    opt : list
        List of options found

    '''
    from json import load
    from pprint import pprint
    
    if inp is not None:
        try:
            data = load(inp)
            name = list(data.keys())[0]
            year = data[name]['game']['year']
            name = name.strip('"')
            try:
                year = int(year)
            except:
                year = None
        except Exception as e:
            print(f'Failed to read JSON file {inp.name}: {e}')
            return None,None,None,None

    if name:
        db = missing.get(name,None)
        if db is not None:
            new   = db['new']
            #exact = db.get('exact',exact)
            yer   = db['year']
            iden  = db.get('iden',0)
            # print(f'Overriding {name} ({year}) with {new if len(new) > 0 else name} ({yer}) [{iden}]')
            try:
                year  = int(yer)
            except:
                pass
            if len(new) > 0: name = new
            if iden < 0:
                print(f'{name} disabled by iden={iden}')
                return None,None,None,None
            if iden == 0:    iden = None
            
        
    idens = [iden]
    if iden is None:
        from urllib.parse import quote_plus, quote, urlencode

        for ex in [exact,not exact]:
            query = urlencode({'search': name,
                               'exact': 1 if ex else 0},)
            info  = parse(request(f'search?{query}'))
            cand  = info.get('boardgame',None)

            # Nothing back
            if cand is None:
                # If we tried and exact match, retry with a loose match
                if ex:
                    print(f'No candidates on {name} ({year}), '
                          'with exact match, retry')
                    continue
                # Otherwise, fail 
                return None,None,name,year
            
            
            if isinstance(cand,list):
                idens = [c.get('objectid',None) for c in cand
                         if year is None or
                         year == int(c.get('yearpublished',{}).get('text',0))]
                if len(idens) < 1:
                    idens = [c.get('objectid',None) for c in cand
                             if year is None or
                             abs(int(c.get('yearpublished',{}).get('text',0))-year) < 2]
                if len(idens) < 1:
                    idens = [c.get('objectid',None) for c in cand]
                
            if isinstance(cand,dict):
                idens = [cand.get('objectid',None)]

            # If we got more than one candiate, but had loose match,
            # retry with a strict match 
            if len(idens) > 1 and not ex:
                print(f'Too many candiates on {name} ({year}): {len(idens)}, '
                      f'retry with exact={not ex}')
                continue
        
    ret = [parse(request(f'boardgame/{iden}'))
           for iden in idens if iden is not None]

    if len(ret) == 1:
        return ret[0],idens[0],name,year

    return ret,None,name,year
        
# ====================================================================
def show(info,iden,name,year,output):
    '''Print final result of the query'''
    from pprint import pprint
    from json import dump

    if info is None:
        print(f'No information on "{name}" ({year})/{iden}"')
        return
    
    if not isinstance(info,list):
        dump(info,output,indent=2)
        return
        
    print(f'Got back {len(info)} on "{name}" ({year})/{iden} replies, '
          'please narrrow/widden')
    for inf in info:
        # print(inf)
        dct = inf.get('boardgame',{})
        nam = dct.get('name',None)
        if isinstance(nam,list):
            nams = nam
            for n in nams:
                nam = n
                if 'primary' in n and n['primary'] == 'true':
                    break

        nam  = nam.get('text','<<?>>')
        iden = int(dct.get('objectid',0))
        year = int(dct.get('yearpublished',{}).get('text',0))
        print(f'{iden:8d}: {nam} ({year})')

# ====================================================================
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pprint import pprint
    
    ap = ArgumentParser(description='Get info from BGG')
    ap.add_argument('-i','--id',type=int,
                    help='Get by board game ID')
    ap.add_argument('-n','--name',type=str,
                    help='Get by name')
    ap.add_argument('-y','--year',type=int,
                    help='Narrow name search by year')
    ap.add_argument('-j','--json',type=FileType('r'),
                    help='Read information about game from JSON')
    ap.add_argument('-o','--output',type=FileType('w'),default='-',
                    help='Output JSON file to write')
    gr = ap.add_mutually_exclusive_group()
    gr.add_argument('--any', action='store_false',dest='exact',default=True,
                    help='Loose matching of name')
    gr.add_argument('--exact', action='store_true',
                    help='Exact matching of name')
    ap.add_argument('--override', type=FileType('r'),default=None,
                    help='Overrides')

    args = ap.parse_args()
    # print(vars(args))
    

    if args.name is None and args.id is None and args.json is None:
        raise RuntimeError('Please specify a name, id, or JSON file')

    from pathlib import Path
    from json import load

    miss = {}
    if args.override:
        miss    = load(args.override)
        # print(len(miss),'overrides')
            
    show(*get_info(args.name,
                   args.id,
                   args.json,
                   args.year,
                   args.exact,
                   miss),
         args.output)

# --------------------------------------------------------------------
    
