#!/usr/bin/env python

from json import load
from pprint import pprint

def show(inp):
    d = load(inp)

    pprint(d)

    t = d.get('parse',{}).get('wikitext',{}).get('*',None)
    if t:
        print(t)


if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Show WikiText')
    ap.add_argument('input',type=FileType('r'))

    args = ap.parse_args()

    show(args.input)

    

    
