#!/usr/bin/env python

# --------------------------------------------------------------------
def by_col(row,col,*iden):
    return row[col] in iden

# --------------------------------------------------------------------
def by_id(row,*iden):
    return by_col(row,0,*iden)

# --------------------------------------------------------------------
def by_ref(row,*iden):
    return by_col(row,1,*iden)

# --------------------------------------------------------------------
def find_rows(tab,*iden,key=by_id):
    ret = []
    for row in tab:
        if key(row,*iden):
            ret.append(row)

    return ret

# --------------------------------------------------------------------
def show_other(rel,tab,iden,head,indent,key=by_id,ref_key=1):
    print(f'{indent}{head}: ')
    if isinstance(iden,list):
        rows = find_rows(rel,*iden,key=key)
    else:
        rows = find_rows(rel,iden,key=key)
    for row in rows:
        el = find_rows(tab,row[ref_key])[0][1]
        print(f'{indent}  {el}')

# --------------------------------------------------------------------
def show_range(min,max,head,indent,unit=''):
    print(f'{indent}{head}: {min} ',end='')
    if min < max:
        print(f'to {max} {unit}')
    else:
        print(f'{unit}')


# --------------------------------------------------------------------
def show_user(usr,indent=''):
    nam = usr[3] if usr[3] is not None else '?'
    eml = '<'+(usr[2] if usr[2] is not None else '?')+'>'
    unm = usr[1]
    print(f'{indent} {unm:12s} {eml:30s} {nam}')
    
# --------------------------------------------------------------------
def show_users(rel,tab,lookup_id,head,indent):
    print(f'{indent}{head}:')
    rows = find_rows(rel,lookup_id)
    for row in rows:
        usr = find_rows(tab,row[1])[0]
        show_user(usr,indent+'  ')
    
# --------------------------------------------------------------------
def show_game(game,tabs,full=False,indent=''):
    game_id,\
        name,\
        year,\
        bgg_id,\
        bgg_name,\
        image,\
        min_play,\
        max_play,\
        min_time,\
        max_time,\
        readme,\
        bgg_desc  = game

    subind = indent+'  '
    print(f'{indent}Result[{game_id}]: {name} ({year})')
    print(f'{subind}BGG: {bgg_name} ({bgg_id})')
    show_range(min_play,max_play,'Players',subind,'')
    show_range(min_time,max_time,'Duration',subind,'minutes')
    show_other(tabs['game_publisher'],tabs['publishers'],game_id,
               'Publishers',subind)
    show_other(tabs['game_tag'],tabs['tags'],game_id,
               'Tags',subind)
    show_users(tabs['game_contributor'],tabs['users'],game_id,
               'Contributors',subind)

    project_rows = find_rows(tabs['projects'],game_id,key=by_ref)
    while True:
        n = len(project_rows)
        print(f'{subind}Projects: {n}')
        for no,project in enumerate(project_rows):
            print(f'{subind}  {no:3d}: {project[3]}[{project[0]}] - {project[4]}')

        sel = n if full else get_int('Which project do you want to see?',n)
        if sel < 0:
            break

        full  = full or sel == n
        which = list(range(n)) if full else [sel]

        for sel in which:
            show_project(project_rows[sel],tabs,subind+'  ',full=full)
        
        if full:
            break
        
# --------------------------------------------------------------------
def show_project(project,tabs,indent='',full=False):
    print(f'{indent}{project[3]}[{project[0]}]')
    print(f'{indent}  Summary: {project[4]}')

    mains = find_rows(tabs['users'],project[2])
    print(f'{indent}  Maintainer ({len(mains)}): ',end='')
    show_user(mains[0],'')
        
    package_rows = find_rows(tabs['packages'],project[0],key=by_ref)
    while True:
        n = len(package_rows)
        print(f'{indent}  Packages: {n}')
        for no,package in enumerate(package_rows):
            print(f'{indent}    {no:3d}: {package[0]} - {package[2]}')

        sel = n if full else get_int('Which package do you want to see?',n)
        if sel < 0:
            break

        full  = full or sel == n
        which = list(range(n)) if full else [sel]

        for sel in which:
            show_package(package_rows[sel],tabs,indent+'    ',full=full)

        if full:
            break

# --------------------------------------------------------------------
def show_package(package,tabs,indent,full=False):
    print(f'{indent}{package[0]}')
    print(f'{indent}  Summary: {package[2]}')

    file_rows = find_rows(tabs['package_file'],package[0])
    print(f'{indent}  Files: {len(file_rows)}')
    for file_row in file_rows:
        file = find_rows(tabs['files'],file_row[1])[0]
        print(f'{indent}    {file[3]} {file[2]}')

    show_users(tabs['package_maintainer'],tabs['users'],package[0],
               'Maintainers',indent+'  ')
    show_users(tabs['package_contributor'],tabs['users'],package[0],
               'Contributors',indent+'  ')

# --------------------------------------------------------------------
def show_publisher(publisher,tabs,full=False):
    print(f'{publisher[1]}:')

    show_other(tabs['game_publisher'],tabs['games'],publisher[0],
               'Games','',key=by_ref,ref_key=0)
                   
# --------------------------------------------------------------------
def get_int(prompt,max):
    from sys import stdin


    while True:
        print(f'{prompt} [0 to {max}] ',end='',flush=True)
        l = stdin.readline().strip().strip('\n')

        if not l:
            return -1
        
        try:
            ret = int(l)

            if ret > max:
                raise ValueError(f'Invalid value {l}>{max}')

            return ret
        except:
            print(f'Invalid input "{l}", try again (-1 to end)')
            

    return -1

# --------------------------------------------------------------------
def get_yesno(prompt):
    from sys import stdin
    
    print(f'{promp} [yN]',end='',flush=True)
    l = stdin.read().strip().strip('\n').lower()
    return l.startswith('y')

# --------------------------------------------------------------------
def get_string(prompt):
    from sys import stdin
    print(f'{prompt}? ',end='',flush=True)
    return stdin.readline().strip().strip('\n')

# --------------------------------------------------------------------
def game_search(tabs,full=False):

    what = get_string(f'Which game do you want to search for')
    if not what: return 

    cand  = []
    lwhat = what.lower()
    for g in tabs['games']:
        if lwhat in g[1].lower() or lwhat in g[5].lower():
            cand.append(g)

    n = len(cand)
    if n < 1:
        print(f'No candidate for "{what}"')


    if n > 1 and not full:
        print(f'Possible candidates are:')
        for i,c in enumerate(cand):
            print(f'{i:3d}: {c[1]}')

        i = get_int(f'Which game do you want to see?',len(cand))
        if i < 0:
            return

        full = full or n == i
        if not full:
            cand = cand[i:i+1]

    for c in cand:
        show_game(c,tabs,full)
        
                             
# --------------------------------------------------------------------
def publisher_search(tabs,full=False):
    what = get_string(f'Which publisher do you want to search for')
    if not what: return 

    cand  = []
    lwhat = what.lower()
    for p in tabs['publishers']:
        if lwhat in p[1].lower():
            cand.append(p)

    n = len(cand)
    if n < 1:
        print(f'No candidate for "{what}"')


    if n > 1 and not full:
        print(f'Possible candidates are:')
        for i,c in enumerate(cand):
            print(f'{i:3d}: {c[1]}')

        i = get_int(f'Which publisher do you want to see?',len(cand))
        if i < 0:
            return

        full = full or n == i
        if not full:
            cand = cand[i:i+1]

    for c in cand:
        show_publisher(c,tabs,full)
        
# --------------------------------------------------------------------
def tag_search(tabs,full=False):                             
    categories = {}
    cnt        = 0
    for iden,tag in tabs['tags']:
        cat, *val = tag.split(':')

        val = ':'.join(val)
        if not val:
             continue
         
        if cat not in categories:
            categories[cat] = []

        categories[cat].append((val,iden))
        cnt += 1

    for cat,vals in sorted(categories.items()):
        print(f'{cat}:')
        for val in sorted(vals):
            print(f'  {val[1]:6d} {val[0]}')

    choices = get_string('Enter tag numbers as space-separated list')
    try:
        tags = [int(choice) for choice in choices.split(' ')]
    except:
        raise ValueError(f'Invalid input: {choices}')

    print(f'Tags to search for:')
    for iden,tag in tabs['tags']:
        if iden in tags:
            print(f'  {tag}')
            
    typ = get_int('0: Inclusive (or) or 1: Exclusive (and)',1)
    if typ == 1:
        from functools import reduce 
        games = [[g[0]
                  for g in find_rows(tabs['game_tag'],tag,key=by_ref)]
                 for tag in tags
                 ]
        #print(games)
        games = list(reduce(lambda a,b: set(a) & set(b), games))
        rows  = find_rows(tabs['games'],*games)
        for row in rows:
            print(f'  {row[1]}')
        return
            
        
    show_other(tabs['game_tag'],tabs['games'],tags,
               'Games','',key=by_ref,ref_key=0)
        
    
# --------------------------------------------------------------------
if __name__ == '__main__':
    from sys import stdin
    from argparse import ArgumentParser, FileType
    from json import load
    
    ap = ArgumentParser(description='Query database')
    ap.add_argument('search',nargs='*',help='Games to search for')
    ap.add_argument('-i','--input',type=FileType('r'),
                    help='Tables files',default='tables.json')
    ap.add_argument('-f','--full',action='store_true',
                    help='Show full results')

    args = ap.parse_args()
    tabs = load(args.input)

    for name in args.search:
        try_search(name,tabs,args.full)

    searches = [('Game',      game_search), 
                ('Publisher', publisher_search),
                ('Tags',      tag_search)]
    while True:
        print('Search for ...')
        for no, choice in enumerate(searches):
            print(f'{no:3d}: {choice[0]}')
        choice = get_int('What do you want to search for',len(searches)-1)
        
        if choice < 0:
            break
        if choice >= len(searches):
            continue

        func = searches[choice][1]
        func(tabs,args.full)
    
