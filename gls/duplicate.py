#!/usr/bin/env python

from json import load

with open('pages_bgg.json','r') as inp:
    j = load(inp)
     
by_bgg = {}
for k,d in j.items():
    bgg = d.get('bgg',{}).get('objectid',None)
    if bgg is None:
        continue
    
    try:
        bgg = int(bgg)
    except:
        print(f'Fail on {k}')
        
    if bgg < 0:
        continue
    
    if bgg not in by_bgg:
        by_bgg[bgg] = []
        
    by_bgg[bgg].append(k)
    
for bgg,names in by_bgg.items():
    if len(names) > 1:
        print(f'{bgg:6d} {"\n       ".join(names)}')
