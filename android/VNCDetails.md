# The VNC steps in detail

If the [installation and setup of packages and VASSAL](README.md) script did not work, you can follow the steps outlined below

## Download VASSAL

Open your *regular* Android browser and go to

    https://vassalengine.org 

and select *Download VASSAL*. Be sure to take the `Linux` version.

This will put a file a la

    VASSAL-3.7.9-linux.tar.bz2 

in your normal `Download` folder (adjust the version number to the version you downloaded).

## Unpack VASSAL

Previously, you made sure that you can see the regular Android file system in Termux, so you can unpack VASSAL into your Termux environment directly from the Android file system. In the command prompt execute

    cd 
    tar -xjvf /sdcard/Download/VASSAL-3.7.9-linux.tar.bz2

Adjust the version number to the version you downloaded. To see which files you have in your Android `Download` folder, do

    ls /sdcard/Download/

After unpacking, you will now have the directory

    VASSAL-3.7.9 

in your Termux environment (adjust the version number to the version you got). Below, we will assume that you can find the most recent VASSAL version installed in `VASSAL-current`, so you should make a symbolic link from your unpacked VASSAL installation to that

    ln -s VASSAL-3.7.9 VASSAL-current

More on how to upgrade VASSAL below.

## Upgrade Termux Packages

Termux has many add-on packages that can be installed *inside* Termux. Go ahead and upgrade any of these that need upgrading:

    pkg update 
    pkg upgrade 

## Install the Java Runtime Environment in Termux

At the command prompt enter

    pkg install openjdk-17-x which nano

After this, you will have the JRE, that you need for VASSAL, installed, plus some utilities you will need.

- `openjdk-17-x` is the JRE version 17 (adjust for availability)
- `which` is small to that finds commands
- `nano` is a simple text editor

*Actually*, `openjdk-17-x` is the *Java Development Kit* (JDK), which *includes* a JRE. But it also has tools for developing Java applications. That is, you have installed a Java development environment on your device.

## Get X in Termux

Please also refer to [these guidelines][termux-x] for more information.

In the Termux command line type

    pkg install x11-repo 

to add a repository of X packages. Then do

    pkg install tigervnc xfce4 netsurf

to install a VNC server (`tigervnc`), a full desktop environment
(`xfce4`), and a simple web-browser (`netsurf`).

### Environments

Above we chose the desktop environment [Xfce][xfce4]` because it's lightweight.

One of the powers of Linux and other open-source platforms, such as Android, is that you have the option to chose what environment you want.  As described in the [Termux GUI guide][termux-x] on [desktops][termux-desktop] you have a variety of options to chose from, and each option can be configured in a variety of ways.

## Set-up the graphical environment

In your Termux app do

    mkdir -p ~/.vnc 
    nano ~/.vnc/xstartup

The first command creates the directory `.vnc` in your home directory, and the second edits the (new) file `xstartup` in that directory using the editor `nano`. In `nano` put in the lines

    #!/data/data/com.termux/files/usr/bin/sh
    xfce4-session &

The lines *must* be typed exactly as shown above. Then type `[ctrl]-x`, followed by `y` and `[return]` to save the file and close `nano`. See also [`~/.vnc/xstartup`][dot-vnc-xstartup]. Finally, make the script executable

    chmod a+x ~/.vnc/xstartup

Next, we will configure our VNC server. Do

    nano ~/.vnc/config

to make a new configuration file for our VNC server. In `nano` type in the *exact* lines

    # Configuration of tigervnc 
    # See also https://tigervnc.org/
    localhost
    depth=24
    geometry=2400x1080

and then `[ctrl]-x` followed by `y`, and then `[enter]` to save the file and quit `nano`. See also [`~/.vnc/config`][dot-vnc-config].

See more about this configuration file [above](#adjust-the-screen-resolution-and-configure-vnc-server).

## Launch the Graphical Environment

In your Termux app, execute

    vncserver :1 

The first time you execute this command, you will be prompted to set a password. Please pick a password that you can remember, but do not be too lax about it (i.e., do not make a password like `123456`), as up-to-no-good people *can* get full access to your device through the VNC server (though the it is set up to only allow connections from the device itself). Typically, VNC clients will allow you to store passwords for your connections, or can use some password wallet service to store them. Thus, you do not necessarily need to be able to remember this password indefinitely.

### Change VNC Password

If you want to change or reset the VNC password, do

    vncpasswd

and set a new password.

## Start the VNC Client

Open the regular Android VNC client app you installed, skipping past any introductory screens.

Next, create a new connection. Here, you should use

-   Host: `localhost`
-   Number: `1` (see more below)
-   Password: The password you created above.

With respect to the number: - The VNC server you started above is listening on port `5901`.

-   Some VNC clients expects you to enter the *full* port number, in which case you *must* put `5901` as the number.
-   Other VNC clients (e.g., RealVNC) expects you to put the *offset* relative to the base port `5900`, in which case you *must* put `1` as the number.

Now open the connection. You should see a Linux desktop. How you interact with the desktop depends on the VNC client. The VNC client app typically has a short introduction that allows you to get familiar with the interface. 

Alternatively, you may start the VNC client from Termux with

    termux-url-open vnc://localhost:5901

to launch your default VNC client.

## Launch VASSAL

Now we are ready to actually launch VASSAL.

Go back to your VNC client which should still be running its connection to Termux. If not, then restart the connection from the VNC client app.

Click the **Applications** menu in the top-left corner and select **Terminal Emulator**. This will open up a terminal window with a command prompt, similar to what you have in the Termux app.

In that command prompt, type

    cd VASSAL-current

to change directory into the VASSAL installation directory. Now type

    ./VASSAL.sh 

to start VASSAL. **Et voilá**, you have VASSAL running on your Android device.

Use the VASSAL application to find your VASSAL modules and open them as you would on a computer.

## Additional (optional) Set-up

These steps are done automatically by the [script method](#the-quick-but-experimental-way-10) of installation, and is not needed if that method was used.

### Desktop Launcher

To make a Desktop launcher icon for VASSAL in the Termux GUI, do in your Termux app

    cd ~/Desktop 
    nano VASSAL.desktop 

and enter the *exact* lines below

    [Desktop Entry]
    Type=Application
    MimeType=application/x-vassal-module;application/x-vassal-log;application/x-vassal-save
    Name=VASSAL
    Exec=/data/data/com.termux/files/home/VASSAL-current/VASSAL.sh
    Icon=/data/data/com.termux/files/home/VASSAL-current/VASSAL.svg
    Actions=Run;Edit;
    Categories=Game

    [Desktop Action Run]
    Name=Run
    Exec=/data/data/com.termux/files/home/VASSAL-current/VASSAL.sh -l %f
                                       
    [Desktop Action Edit]
    Name=Edit                                                     
    Exec=/data/data/com.termux/files/home/VASSAL-current/VASSAL.sh -e %f

and type `[ctrl]-x`, followed by `y` and `[enter]` to save the file. See also [`~Desktop/VASSAL.desktop`][desktop-vassal.desktop]. Now do

    chmod a+x Desktop/vassal.desktop

to make the file executable. You can now double-click the Termux GUI Desktop VASSAL icon to launch VASSAL.

### Associate `.vmod` and `.vlog` Files with VASSAL

In your Termux app, do

    mkdir ~/.local/share/mime/packages
    nano ~/.local/share/mime/packages/application-x-vassal.xml

and enter the *exact* line

    <?xml version="1.0"?>
    <mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
       <mime-type type="application/x-vassal-module">
         <comment>VASSAL module file</comment>
         <glob pattern="*.vmod"/>
         <icon name="application-x-vassal"/>
       </mime-type>
       <mime-type type="application/x-vassal-log">
         <comment>VASSAL log file</comment>
         <glob pattern="*.vlog"/>
         <icon name="application-x-vassal"/>
       </mime-type>
       <mime-type type="application/x-vassal-save">
         <comment>VASSAL save file</comment>
         <glob pattern="*.vsav"/>
         <icon name="application-x-vassal"/>
       </mime-type>
    </mime-info>

and type `[ctrl]-x` followed by `y` and `[enter]` to save the file and exit `nano`. See also [`~/.dot/local/share/mime/packages/application-x-vassal.xml`][dot-local-share-mime-packages-application-x-vassal.xml].

Then copy the desktop file to `~/.local/share/applications`

    mkdir -p ~/.local/share/applications
    cp ~/Desktop/VASSAL.desktop ~/.local/share/applications/
    
And copy the icon to `~/.local/share/icons/hicolor/scalable` 

    mkdir -p ~/.local/share/icons/hicolor/scalable/mimetypes
    cp ~/VASSAL-current/VASSAL.svg ~/.local/share/icons/hicolor/scalable/mimetypes/application-x-vassal.svg

Now, update the Application and Mime type data base

    update-desktop-database ~/.local/share/applications 
    update-mime-database ~/.local/share/mime

Now, double-tapping a `.vmod` file in the Termux GUI file manager will open up that module in VASSAL.

### Launch VASSAL from the Android Home Screen

For this to work, you need to install the [Termux:Widget][termux-widget] Android app.  Please follow the [*installation instructions*][termux-widget-install].  As for Termux, *do not* install the app from the Google Play store.

Next, open up your Termux app and do

    mkdir -p ~/.shortcuts

to make a directory for Termux home screen short cuts. We will make a short cut script named `VASSAL` in that directory. In the Termux app execute

    nano ~/.shortcuts/VASSAL

to open the (new) file `~/.shortcuts/VASSAL` in the `nano` editor. Type in the *exact* lines below

    #!/data/data/com.termux/files/usr/bin/bash

    xstart=$HOME/.vnc/xvassal
    oper=start

    read -n 1 -p "Do want to start? (Y/n) "
    case x$REPLY in
      xy|xY|x) oper=start ;;
      *) oper=stop ;;
    esac

    case $oper in
    start)
        echo "IMPORTANT: Enable Termux Wake-Lock through its notification"
        termux-wake-lock
        if test $? -ne 0 ; then 
            read -n 1 -p "Failed to aquire Wake-lock, exiting"
            exit 1
        fi

        vncserver \
            -xstartup $xstart \
            -autokill \
            -fg \
            :1 &

        vnc_pid=$!

        termux-open-url vnc://localhost:5901

        wait $vnc_pid

        termux-wake-unlock
        ;;
    stop)
        echo "Stopping the VNC server"
        vncserver -clean -kill :1
        ;;
    esac

Press `[ctrl]-x`, followed by `y` and `[enter]` to save the script and exit `nano`. See also [`~/.shortcuts/VASSAL`][dot-shortcuts-VASSAL].

This script will be executed from the Termux widget, so we need to make it executable

    chmod a+x ~/.shortcuts/VASSAL

Next, we will make a special X start-up script that will execute VASSAL in the GUI session. Do

    nano ~/.vnc/xvassal

and put in the *exact* lines

    #!/data/data/com.termux/files/usr/bin/bash

    xfce4-session &
    env bash $HOME/VASSAL-current/VASSAL.sh

followed by `[ctrl]-x`, followed by `y` and `[enter]` to save the script and exit `nano`. See also [`~/.dot/vnc/xvassal`][dot-vnc-xvassal], this script will be executed by the VNC server, so we need to make it executable

    chmod a+x ~/.vnc/xvassal

See [above](#adding-a-widget-to-the-home-screen-(optional)) for how to add the widget to the home screen

----

# Files for download

Below are downloadable versions of the scripts generated above. Remember to make them executable

    chmod a+x <file-name>

replacing `<file-name>` with the actual file name.

- [`~/.vnc/xstartup`][dot-vnc-xstartup]
- [`~/.vnc/xvassal`][dot-vnc-xvassal]
- [`~/.shortcuts/VASSAL`][dot-shortcuts-VASSAL]
- [`~/Desktop/VASSAL.desktop`][desktop-VASSAL.desktop]
- [`~/setup-vassal.sh`][setup-vassal.sh]

Below are links to other files

- [`~/.vnc/config`][dot-vnc-config]
- [`~/.local/share/mime/packages/application-x-vassal.xml`][dot-local-share-mime-packages-application-x-vassal.xml]. 

[dot-vnc-xstartup]: https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/dot-vnc-xstartup
[dot-vnc-xvassal]:  https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/dot-vnc-xvassal
[dot-vnc-config]:   https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/dot-vnc-config
[desktop-vassal.desktop]: https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/common/VASSAL.Desktop
[dot-local-share-mime-packages-application-x-vassal.xml]: https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/common/VASSAL.Desktop/dot-local-share-mime-packages-application-x-vassal-xml.txt
[dot-shortcuts-VASSAL]: https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/dot-shortcuts-VASSAL

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode 0) -->
<!-- eval: (visual-line-mode) -->
<!-- eval: (visual-fill-column-mode) -->
<!-- eval: (adaptive-wrap-prefix-mode) -->
<!-- End: -->
