# Emulator of Android 

See also 

    https://wiki.debian.org/AndroidTools
    
Process 

- Install SDK manager 

        sudo apt install default-jre sdkmanager
    
- Set home 

	    export ANDROID_HOME=`pwd`/sdk
	    mkdir -p $ANDROID_HOME
        
- Get the emulator 

	    sdkmanager "emulator" \
          "cmdline-tools;latest" \
          "platforms;android-31" \
          "platform-tools"
          
- Get images 

	    $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager \
          "system-images;android-31;default;x86_64"
          
- Create machine 

         $ANDROID_HOME/cmdline-tools/latest/bin/avdmanager \
           create avd \
           --tag default \
           --package "system-images;android-31;default;x86_64" \
           --sdcard 64M \
           --device "medium_tablet" \
           --name medium_table_31
           
- Enable keyboard, edit ~/.android/avd/medium_table_31.avd/config.ini 

        hw.keyboard = yes
        
- Run emulator 

	    $ANDROID_HOME/emulator/emulator @medium_table_31
    

- Download Termux from 
  - https://f-droid.org/packages/com.termux/
  - https://f-droid.org/en/packages/com.termux.widget/

        mv ~/Downloads/com.termux* . 
      
- Download AVNC from https://f-droid.org/en/packages/com.gaurav.avnc/
  or MultiVNC from https://f-droid.org/en/packages/com.coboltforge.dontmind.multivnc/
  or RealVNC from https://help.realvnc.com/hc/en-us/articles/360002762697-Where-can-I-find-the-APK-files-for-RealVNC-Viewer-and-RealVNC-Server-for-Android

        mv ~/Downloads/com.gaurav.avnc* . 
        mv ~/Downloads/com.coboltforge.dontmind.multivnc* . 
        mv ~/Downloads/com.realvnc.viewer.android* .
        
- Download Hacker's keyboard from
  https://f-droid.org/en/packages/org.pocketworkstation.pckeyboard/
  
        mv ~/Downloads/org.pocketworkstation.pckeyboard*.apk
      
- See attached devices 

        sdk/platform-tools/adb devices
      
- Install packages on device 

        sdk/platform-tools/adb install-multi-package com.termux*.apk 
        sdk/platform-tools/adb install com.gaurav.avnc*
        sdk/platform-tools/adb install org.pocketworkstation.pckeyboard*.apk
        sdk/platform-tools/adb install com.coboltforge.dontmind.multivnc*.apk
        sdk/platform-tools/adb install com.realvnc.viewer.android* .

- Upload set-up script 

        sdk/platform-tools/adb push ../setup-vassal.sh /sdcard/Download/
      
- On device, launch Termux and enable storage 

        termux-setup-storage 
       
- 
