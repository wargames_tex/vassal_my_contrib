<div data-theme-toc="true"> </div>

**N.B.:** This post is long, but mainly because the steps are thoroughly explained. The process is not all that long.

**N.B.:** Check if you need to re-run the setup to fix bugs [here](#update-setup).

# TL;DR 

- [Install Termux](#install-termux)
  - [Install Termux:X11](#install-termux-x11)
  - [Install Termux:Widget](#install-termux-widget)
- [Install VASSAL](#run-setup)
- [Run VASSAL](#running-vassal)


# Background

A bit of background first.

- Android is an operating system built on top of the [Linux kernel][linux].
- Android apps are typically written in the Java programming language and executed in [**Android Run-Time**][art] environment (ART).
  - This Android-specific run-time environment is *not* a real [**Java Runtime Environment**][jre] (JRE), and regular Java apps, in particular Graphical User Interface (GUI) Java apps cannot be executed by ART.
- VASSAL is a Java application, and uses the Java GUI services.
- The VASSAL app is executed in a Java Run-time Environment (JRE).
  - Java apps do not run directly *on the metal*, but are run through the JRE abstraction layer. This means a Java app, once build, can run on *any* system that can run the JRE.
  - VASSAL *requires* a true JRE, and the ART is *not* a real JRE, and VASSAL can therefore not be run *directly* on Android.

Thus, to run VASSAL on an Android device, we need to get a true JRE onto the device.

We will do that via the very powerful app [**Termux**][termux]. Remember, Android runs on a Linux kernel, and we can therefore set up an environment which uses the Linux kernel and run regular Linux user-land applications and services there.

To get a Graphical User Interface (GUI), we need a few more things. The GUI back-end of Linux is [**X**][xsystem] (nothing to do with what was known as Twitter, even though that service has appropriate the X name and logo). Android does *not* provide the X service, but we can set that *service* up in our Termux environment. 

With these considerations clear, we can go onto the practical steps needed to get VASSAL running on an Android device.

## Use VNC 

If you'd rather use VNC to run VASSAL on Android, stop reading here and see [this post](https://forum.vassalengine.org/t/79268).

----

# Installation

## Needed Apps

For your set-up you need the following software and Android apps

- [VASSAL][vassal] (obviously) for Linux
- A [VASSAL module][vassal-modules] to test things out.
- [Termux][termux]
- [Termux:X11][termux-x11]
- Optionally [Termux:Widget][termux-widget]

Below we will go through the steps of setting these up.

### Important

Command listed below must be typed into Termux *exactly* as written. Case matters, as in `downloads` and `Downloads` are *not* the same thing. For example, if the instructions tells you to write

    pkg update
    pkg upgrade

then two commands should be entered *exactly* as written above at the command prompt. That is first, type `p` `k` `g` `[space]` `u` `p` `d` `a` `t` `e` `[enter]` and then `p` `k` `g` `[space]` `u` `p` `g` `r` `a` `d` `e` `[enter]`.

If you are reading this on your Android device, you can highlight the commands and copy them to your clipboard. In Termux you can then long-press anywhere and select **Paste** to paste in the command into the commmand prompt.

----

## <a name="install-termux"></a> Install Termux

![termux_web.png|962x607,50%](android_01_termux_web.png)

Follow the [*installation instructions*][termux-install] on the Termux web-site.
 
![termux_inst.png|962x607,50%](android_02_termux_inst.png)

**Note** There is a Termux app in the Google Play store - *do not* use that. Currently (2024 and for the past 3+ years), that app is not kept up to date.

If you install from [GitHub][termux-github], be sure to take the version appropriate for you device architecture. If you are unsure, or do not know what your device's architecture is, take the `termux-app_v*+github-debug_universal.apk` app.

![termux_apk.png|962x607,50%](android_03_termux_apk.png)

If you already have Termux installed (duh!), you can check the architecture by doing 

    uname -m
    
If the output is `x86_64` then you need the `x64_64` variant, while `aarch64` means you need the `arm64` variant, and so on. 

Whether you install from GitHub or [**F-Droid**][termux-fdroid] you *will* need to OK installation from "untrusted" sources. Please see [this guide][apk-foreign].

Once you have installed the Termux app, go a head and launch it as any other app on your device. You will be greeted by black screen with text on it. Take a minute to read the text. More information, including a "Getting Started" Guide is available at the [Termux Wiki][termux-wiki] pages.

![termux_drawer.png|962x607,50%](android_04_termux_drawer.png)


### <a name="termux-storage"></a> Give Termux Access to Android File System

After these two commands have finish, you must set-up Termux to have access to the regular device file system.  Start up the Termux app and execute

    pkg update 
    pkg upgrade
    pkg install -y termux-tools wget openssl
    termux-setup-storage 

In the pop-up dialog shown, select **Allow**.  This is kinda important so that you can use your regular Android browser to download VASSAL modules and they become available inside Termux environment.

![termux_storage.png|962x607,50%](android_05_termux_storage.png)

### <a name="install-termux-x11"></a> Install Termux:X11 

Next, we need the [**Termux:X11**][termux-x11] app.  This app is currently (early 2025) _not_ released but it seems in rather good shape.  However, that means we can _only_ get it from GitHub. 

![termuxx11.png|962x601,50%](android_05_termuxx11.png)

Got to the [Termux:X11 nightly release][termux-x11-release] page and download the `app-*-debug.apk` appropriate for your device.  [As with the Termux app](#install-termux) if you do not know your devices architecture, take the `app-universal-debug.apk` package. 

![termuxx11_apk.png|962x601,50%](android_05_termuxx11_apk.png)

You *will* need to OK installation from "untrusted" sources. Please see [this guide][apk-foreign].

### <a name="install-termux-widget"></a> Optionally, Install Termux:Widget app

To able to launch VASSAL directly from the home screen of your device, you need to install the [**Termux:Widget**][termux-widget] Android app.  Please follow the [*installation instructions*][termux-widget-install].  As for Termux, *do not* install the app from the Google Play store.

![termuxwidget_inst.png|962x607,50%](android_06_termuxwidget_inst.png)
![termuxwidget_apk.png|962x607,50%](android_07_termuxwidget_apk.png)

Note, to make the widget work, you need to allow Termux to draw over other apps.
- Open the Android settings
- Navigate to **Apps** and select **Termux**
- Scroll down and enable drawing over other apps 

---- 

## <a name="run-setup"></a> Installation and setup of packages and VASSAL 

- Start up Termux
- In the terminal type

        wget https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/xsetup-vassal.sh -O xsetup-vassal.sh
        bash xsetup-vassal.sh 

  ![setup.png|962x607,50%](android_08_setup.png)

- Follow the on-screen instructions

  ![ready.png|962x607,50%](android_09_ready.png)

  ![done.png|962x607,50%](android_10_done.png)

If you have problems with it, open a thread in the [Technical Support & Bugs][support] Forum.

The [`xsetup-vassal.sh`][xsetup-vassal.sh] script by default install the latest Java and VASSAL versions available.  To set specific version, use the options `-j` and `-v`, respectively.  For example, to install VASSAL version 3.7.23, do 

    bash xsetup-vassal.sh -v 3.7.23 
    
More information is available via the `--help` option 

    bash xsetup-vassal.sh --help
    
---- 

## <a name="add-widget"></a> Adding a Widget to the Home Screen (optional)

If you installed Termux:Widget, then you can add a Termux widget to the home screen from which you can launch VASSAL.

Now we need to add the widget to the Android home screen.

- Go back to the Android home screen and long-press the background.

  ![widgetmenu.png|962x607,50%](android_11_widgetmenu.png)

- In the pop-up menu, select **Widgets** and scroll down to find **Termux:Widget**. Long press it, and drag to the Home screen.

  ![widget.png|962x607,50%](android_11_widget.png)

- You can choose between a 1x1 widget which will make a short-cut to a specific launcher, or you can choose a 2x2 widget that lists all known short cuts.  See also the documentation for [Termux:Widget][termux-widget].
- If you choose the 2x2 widget, You will see a small widget with a list of Termux short-cuts - in particular the short cut **VASSAL**. You can click that to launch the VNC server and client in one go and have VASSAL running in the Termux GUI environment.

  ![widgetlist.png|962x607,50%](android_12_widgetlist.png)

----

## <a name="update-setup"></a> Updating the setup 

The latest [`xsetup-vassal.sh`][xsetup-vassal.sh] script is from February 1, 2025.  If you ran the script prior to that, then consider to download the latest script and run it again (see [above](#run-setup)).  It will not tamper with your modules, saves, and logs. 

Note, you may need to remove previous set-up scripts so as not to get confused about which to run.  Do 

    rm -f xsetup-vassal.sh*
    
before downloading the new set-up script.  This will _also_ upgrade your VASSAL installation unless you specify a version (`-v`) to the [`xsetup-vassal.sh`][xsetup-vassal.sh] script. 


## <a name="clean-up"></a> Clean after setup 

After having run the setup script, you may want to clean-up a bit. Do in Termux 

    rm -f *.log *.tar.bz2 xsetup-vassal.sh 
    
----

# <a name="running-vassal"></a> Running VASSAL 

If you installed the [home-screen-widget](#add-widget), then you should be able to launch VASSAL by clicking the `VASSAL` entry in that widget.

Otherwise, you can start VASSAL by opening up the Termux app and type

    ~/.local/bin/xvassal start
    
VASSAL running in the Termux GUI:

![desktop](android_13_desktop.png)

Running my version of the **[Gettysburg (125th Anniversary Edition)][sgb-page]** module ([1.2-ch][sgb-vmod]):

![game](android_14_sgb.png)

## <a name="termux-x11-preferences"></a> Termux:X11 preferences 

The Termux:X11 preferences are available through a regular Android notification.  In those preferences you can change many settings that will dictate your experience with VASSAL on Android.  Please see those preferences and refer to the [documentation][termux-x11]. 


## <a name="stop-x11"></a> Stopping the X Server

Once you are done using VASSAL, simply quit the VASSAL application in the X session, which will automatically close that session.  If that doesn't work for you, you should stop the X server. In the Termux app execute

    ~/.local/bin/xvassal stop 
    
## <a name="get-modules"></a> Getting VASSAL modules

To get your favourite VASSAL module, visit

    https://vassalengine.org/wiki/Category:Modules

with your regular Android Web-browser, and download the module to your Android `Download` folder. For example, I took a version of the [**Gettysburg (125th Anniversary Edition)**][sgb-page] module ([1.2-ch][sgb-vmod]), which gave me the file

    GettysburgSmithsonian-1.2.vmod.zip 

in my Android `Download` folder. Note that Google Chrome appends the `.zip` to the file name - it doesn't actually put the file into a ZIP archive.

You should move the VASSAL modules to your Termux environment. Go back to the Termux app, and do for example

    cd /sdcard/Downloads/ 
    mv GettysburgSmithsonian-1.2.vmod.zip GettysburgSmithsonian-1.2.vmod

to remove the `.zip`from the file name. Here `cd` means "change directory" and `mv` means "move".

Then, go back "Home" in Termux

    cd 

and make a directory to hold your VASSAL modules, named for example
`VMods`, if not already there,

    mkdir -p VMods 

Here, `mkdir` means "make directory".

We can then move our module there to that we have all our modules in a meaningful place 

    mv /sdcard/Download/GettysburgSmithsonian-1.2.vmod VMods/
 
You can also leave modules in the regular Android user file system, in which case the modules will be in some sub-directory of 

    /sdcard 
    
in your GUI session. 

### <a name="alternative-method"></a> Alternative method

You can also use the script [`get_vmod.sh`][get_vmod.sh].   First, you should download it and make it executable 

    wget https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/get_vmod.sh -O get_vmod.sh
    chmod a+x get_vmod.sh 
    
Then, you can execute it with a URL pointing to the module, and perhaps a URL pointing to an icon that will be used in the widget launcher.  For example, to get [Napoleon at Waterloo](https://vassalengine.org/wiki/Module:Napoleon_at_Waterloo), do 

    ./get_vmod.sh -u https://obj.vassalengine.org/images/b/ba/NapoleonAtWaterloo-ch-1.1.0.vmod -i https://obj.vassalengine.org/images/0/08/Pic2236172.jpg
    
This will download the module to your `~/VMods` directory and add a short-cut to your VASSAL home-screen widget.   Note that the icon should preferably be a PNG image of a size of at least 96px x 96px, but not too big either.  If the image is _not_ a PNG it will automatically be converted to a PNG.  If you do not provide an icon URL, the module will still be downloaded and the short-cut made - it just won't have an icon. 

You can find the URL of modules in the VASSAL library by right-clicking (or long press) the module links and select something like _Copy link address_. For images, you typically want the thumbnail associated with the module.  Click the image and in the page that opens, right click (or long press) the image filename and select _Copy link address_. 

## <a name="restart"></a> Restarting

If you [made a widget](#add-widget), simply tap the `VASSAL` entry again.  Otherwise, open op Termux and do 

    ~/.local/bin/xvassal start &

## <a name="upgrade-vassal"></a> Upgrading VASSAL

If you want or need to upgrade your VASSAL installation, you should

- From your regular Android browser, navigate to the [VASSAL site][vassal] and download the VASSAL version for Linux that you need - say `VASSAL-3.7.10-linux.tar.bz2` 
- Open your Termux app
- Unpack the downloaded VASSAL archive
  
        cd 
        tar -xjvf /sdcard/Download/VASSAL-3.7.10-linux.tar.bz2

- Remove the old *current* link
  
        rm VASSAL-current

- Make the newly unpacked installation the *current* one
 
        ln -s VASSAL-3.7.10 VASSAL-current

  Here, `ln -s` means make a symbolic link from `VASSAL-current` to `VASSAL-3.7.10`.  A symbolic link is sort of an alias file. 
  
- You may want to delete the old (say 3.7.9) installation to free up some space, as well as the downloaded archive

        rm -rf VASSAL-3.7.9
        rm -f VASSAL-3.7.10-linux.tar.bz2

Alternatively, [update-the-setup](#update-setup) as described above.  This will _also_ [upgrade VASSAL](#upgrade-vassal).

## <a name="upgrade-pkg"></a> Upgrade Termux Packages

From time to time, you should upgrade the packages installed inside Termux. Do

    pkg update 
    pkg upgrade
    
Alternatively, [update-the-setup](#update-setup) as described above.  This will _also_ [upgrade the Termux packages](#upgrade-pkg).

## <a name="upgrade-apps"></a> Upgrade Termux, Termux:X11, and Termux:Widget 

Since [**Termux**][termux], [**Termux:X11**][termux-x11], and (possibly) [**Termux:Widget**][termux-widget] was not installed through the Google Play app, you will need to upgrade those apps by hand from time to time.  Follow the links to go to the GitHub pages and grab the latest app, or use the [F-Droid][termux-fdroid] app.  Note that as of February 2025, [**Termux:X11**][termux-x11] can _only_ be installed from GitHub. 

## <a name="problems"></a> Problems 

Please refer to the [Termux:X11 documentation][termux-x11-readme] for some common problems and how to interact with the X desktop. 

If you have problems executing [`xsetup-vassal.sh`][xsetup-vassal.sh] or the installation doesn't work as expected, open a thread in the VASSAL [Technical Support & Bugs][support] Forum.  _Please_ do not add comments to this thread. 

----

# <a name="more"></a> More

Below are some tricks to get more out of your installation. 

## <a name="get-modules-gui"></a> Download modules in the Termux GUI

In the VNC client connection to your Termux environment, open a terminal as described above and type

    netsurf 

This will open a simple web-browser, which you can then direct to

    https://vassalengine.org/wiki/Category:Modules

to find the module you want. By default, the modules are downloaded to your Termux home directory. You can move them to your `VMods` directory by typing in the terminal

    mv ~/DDaySmithsonian-2.0.vmod ~/VMods/

for example.

Alternatively, [use the `get_vmod.sh` script](#alternative-method) to download a module and setup a short-cut to [launch that module](#module-launcher).

## <a name="specific-module"></a> Launch VASSAL with specific module, save, or log 

Assuming your modules are put in `~/VMods`, and we want to run the module [`GettysburgSmithsonian-1.2.vmod`][sgb-page], then in Termux, run

    .local/bin/xvassal start xfc4-vassal -l $HOME/VMods/`GettysburgSmithsonian-1.2.vmod`
    
This will launch the VNC X, and connect with the X client, and in the X session you will have VASSAL running with the chosen module.

You can replace `GettysburgSmithsonian-1.2.vmod` with the filename of your favourite module, save, or log file.  

Alternatively, [use the `get_vmod.sh` script](#alternative-method) to [download a module](#get-modules) and setup a short-cut to [launch that module](#module-launcher).

_Note_, spaces in the file names _can_ be a problem.  Consider to rename your file to something without spaces.  That is easily done with 

    mv "File name with spaces" `echo "File name with spaces" | tr ' ' '_'` 
    
which will result in `File_name_with_spaces`. 

### <a name="module-launcher"></a> From the Android home screen via Widget 

Again, suppose we want to play [`GettysburgSmithsonian-1.2.vmod`][sgb-page] and we want to add an entry to the Termux widget menu.  Then do 

    nano .shortcuts/Gettysburg_Smithsonian
    
and put the content 

    #!/data/data/com.termux/files/usr/bin/bash
    ${HOME}/.local/bin/vncvassal start xfc4-vassal \
        -l $HOME/VMods/`GettysburgSmithsonian-1.2.vmod`
        
Optionally, place a PNG icon (roughtly 96px x 96px) in 

    ~/.shortcuts/icons/Gettysburg_Smithsonian.png 
    
to get a nice launcher icon. 
        
Alternatively, [use the `get_vmod.sh` script](#alternative-method) to [download a module](#get-modules) and setup the short-cut.

### <a name="minimal-gui"></a> Using a minimal graphical interface 

By default, the scripts above uses the [Xfce][xfce4] desktop environment.  But, in some sense using a complete desktop environment is overkill.  What we really only need is a [window manager][wm].

By replacing `xfc4-vassal` with `fluxbox-vassal` in the above commands, you can opt to use the [FluxBox][fluxbox] window manager.  This window manager is pretty simple and therefore light on the resources that it uses.   Your VASSAL experience shouldn't be much different from [Xfce][xfce4], except that regular desktop tools - such as file manager etc., are a bit more hidden away.  On the other hand, you will get a clean desktop. 

----

[termux]: https://github.com/termux/termux-app
[termux-x11]: https://github.com/termux/termux-x11
[termux-x11-release]: https://github.com/termux/termux-x11/releases/tag/nightly
[termux-x11-readme]: https://github.com/termux/termux-x11?tab=readme-ov-file#termuxx11
[termux-install]: https://github.com/termux/termux-app?tab=readme-ov-file#installation
[termux-github]: https://github.com/termux/termux-app/releases
[termux-fdroid]: https://f-droid.org/en/packages/com.termux/
[termux-wiki]: https://wiki.termux.com/wiki/Main_Page
[termux-widget]: https://github.com/termux/termux-widget
[termux-widget-install]: https://github.com/termux/termux-widget?tab=readme-ov-file#Installation
[termux-x]: https://wiki.termux.com/wiki/Graphical_Environment
[termux-desktop]: https://wiki.termux.com/wiki/Graphical_Environment#Desktop_environment
[vassal]: https://vassalengine.org
[vassal-modules]: https://vassalengine.org/wiki/Category:Modules
[linux]: https://en.wikipedia.org/wiki/Linux_kernel
[art]: https://en.wikipedia.org/wiki/Android_Runtime
[jre]: https://en.wikipedia.org/wiki/Java_(software_platform)#Java_Runtime_Environment
[xsystem]: https://en.wikipedia.org/wiki/X_Window_System
[xfce4]: https://www.xfce.org/
[wm]: (https://en.wikipedia.org/wiki/Window_manager)
[fluxbox]: https://fluxbox.org/
[vnc]: https://en.wikipedia.org/wiki/Virtual_Network_Computing
[realvnc]: https://www.realvnc.com/
[apk-foreign]: https://www.androidauthority.com/how-to-install-apks-31494/
[realvnc-store]: https://play.google.com/store/apps/details?id=com.realvnc.viewer.android
[tigervnc-server]: https://tigervnc.org/doc/x0vncserver.html
[sgb-page]: https://vassalengine.org/wiki/Module:Gettysburg:_125th_Anniversary_Edition
[sgb-vmod]: https://vassalengine.org/wiki/File:GettysburgSmithsonian-1.2.vmod
[support]: https://forum.vassalengine.org/c/technical-support-bugs

[xsetup-vassal.sh]:  https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/xsetup-vassal.sh
[get_vmod.sh]:  https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/android/get_vmod.sh

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode 0) -->
<!-- eval: (visual-line-mode) -->
<!-- eval: (visual-fill-column-mode) -->
<!-- eval: (adaptive-wrap-prefix-mode) -->
<!-- End: -->
