#!/data/data/com.termux/files/usr/bin/bash
#
# Post questions, suggestions, bugs, etc. to the VASSAL Technical
# Support and Bugs sub-forum.
#
# Change the VASSAL version to the version you want to install
vassal_version=
# Change the Java version to the one available in Termux 
java_version=
# Location of VASSAL downloads 
github_dl=https://github.com/vassalengine/vassal/releases/download/

# --- Colours ----------------------------------------------
# See also
# https://stackoverflow.com/questions/5947742/
# Reset
NC='\E[m'       # Text Reset

# Regular Colors
B='\E[0;30m'       # Black
R='\E[0;31m'       # Red
G='\E[0;32m'       # Green
Y='\E[0;33m'       # Yellow
B='\E[0;34m'       # Blue
P='\E[0;35m'       # Purple
C='\E[0;36m'       # Cyan
W='\E[0;37m'       # White

# Bold
BF='\E[1;30m'      # Black
BR='\E[1;31m'      # Red
BG='\E[1;32m'      # Green
BY='\E[1;33m'      # Yellow
BB='\E[1;34m'      # Blue
BP='\E[1;35m'      # Purple
BC='\E[1;36m'      # Cyan
BW='\E[1;37m'      # White

# Underline
UN='\E[4;30m'      # Black
UR='\E[4;31m'      # Red
UG='\E[4;32m'      # Green
UY='\E[4;33m'      # Yellow
UB='\E[4;34m'      # Blue
UP='\E[4;35m'      # Purple
UC='\E[4;36m'      # Cyan
UW='\E[4;37m'      # White

# ---- Help message --------------------------------------------------
usage() {
    cat <<EOF
Usage: ${0} [OPTIONS]

Options:
        -h,--help                       This help
        -j,--java-version VERSION       Java version to install
        -v,--vassal-version VERSION     Vassal version to install
        -V,--verbose                    Be verbose
EOF
}
# --- Parse the command line -----------------------------------------
while test $# -gt 0 ; do
    case $1 in
        -h|--help) usage ; exit 0 ;;
        -j|--java-version) java_version=$2 ; shift ;;
        -v|--vassal-version) vassal_version=$2 ; shift ;;
        -V|--verbose)        verbose=1 ;;
        *)
            echo -e "${UR}Unknown option: ${BF}$1{NC}"
            exit 1
    esac
    shift
done

# --- Friendly message -----------------------------------------------
echo -e "\
This script will to install ${BF}VASSAL${NC} and needed
applications into your Termux installation on an Android
device.

This script ${BF}should${NC} work, but it may not.  ${UR}No
Warrenty!${NC}

The script assumes that you have given Termux access to the
standard Android file system by executing

    ${UN}termux-setup-storage${NC}

If you have not done that, please end this script ${UR}now${NC}.

The script also assumes you have access to the internet.  If
not, then you need to download the Linux VASSAL app

    ${UC}VASSAL-version-linux.tar.bz2${NC}

to the current directory, or exit the script ${UR}now${NC}.

The script will install the latest VASSAL version. If you want to
install a different version, end the script now and pass the option '-v
DESIRED_VERSION' to this script.
"

read -n 1 -p "Are you ready? (Y/n)"
case x$REPLY in
    x|xy|xY) ;;
    *) echo "OK, ending the script"; exit 0 ;;
esac

# --- Fail on errors -------------------------------------------------
set -e

# --- Go Home --------------------------------------------------------
echo -e "${BF}Going Home: ${UR}${HOME}${NC}"
cd $HOME

# --- Update and upgraded packages -----------------------------------
echo -e  "${BF}Updating and upgrading Termux packages${NC}."
echo -e  "This may take a while if you've not done this in a while."
echo -en "Log in ${UR}update.log${NC} ... "
pkg update -y  >  update.log 2>&1 
pkg upgrade -y >> update.log 2>&1
echo "done"

# --- "Install" X repo -----------------------------------------------
echo -en "${BF}Adding X repo and installing X packages. Log in ${UR}x.log${NC} ... "
pkg install -y x11-repo                                   >  x.log 2>&1
pkg update -y                                             >> x.log 2>&1 
pkg install -y xfce4 fluxbox netsurf termux-x11 termux-am imagemagick \
    >> x.log 2>&1
echo "done"

# --- Get the java version, if not set already -----------------------
if test "x$java_version" == "x" ; then
    java_pkg=`pkg search openjdk 2>/dev/null|grep -- -x|tail -n 1`
    java_version=`dirname $java_pkg | head -n 1 | sed -e 's/openjdk-//' -e 's/-x//'`
    if test "x$java_version" = "x" ; then
        echo -e "${UR}Failed to get a Java version${NC}"
        echo -e "${BF}Set the Java version with option ${BF}-j${NC}"
        exit 1
    fi
fi

# --- Install JAVA ---------------------------------------------------
echo -en "${BF}Installing JAVA ${java_version}, log in ${UR}java.log${NC} ... "
pkg install -y openjdk-${java_version}-x which nano jq curl > java.log 2>&1
echo "done"

# --- Get the latest VASSAL version, if not already set --------------
if test "x$vassal_version" = "x" ; then
    vassal_version=$(curl https://api.github.com/repos/vassalengine/vassal/releases/latest 2>/dev/null | jq ".tag_name" | tr -d '"')
    if test "x$java_version" = "x" ; then
        echo -e "${UR}Failed to get a VASSAL version${NC}"
        echo -e "${BF}Set the VASSAL version with option ${BF}-v${NC}"
        exit 1
    fi
fi
    
# --- Download VASSAL ------------------------------------------------
# Archive to unpack
archive=VASSAL-${vassal_version}-linux.tar.bz2
if test ! -f ${archive} ; then
    echo -e "${BF}Downloading VASSAL version ${UR}${vassal_version}${NC}"
    rm -f ${archive}*
    wget -nv --show-progress ${github_dl}/${vassal_version}/${archive}

    if test ! -f ${archive} ; then
        echo "Failed to download VASSAL"
        exit 1
    fi
else
    echo -e "${BF}Using existing ${UR}${archive}${NC}"
fi

# --- Unpack VASSAL --------------------------------------------------
echo -en "${BF}Unpacking VASSAL version$ ${UR}${vassal_version}${NC} ... "
tar -xjf ${archive}

rm -f VASSAL-current
ln -s VASSAL-${vassal_version} VASSAL-current
echo "done"

# --- Change the she-bang of the VASSAL script -----------------------
sed 's,#!/usr/bin/env,#!/data/data/com.termux/files/usr/bin/env,' \
    < VASSAL-current/VASSAL.sh > VASSAL-current/VASSAL.tmp
mv VASSAL-current/VASSAL.tmp VASSAL-current/VASSAL.sh
chmod a+x VASSAL-current/VASSAL.sh

# --- Create local bin directory -------------------------------------
shebang="/data/data/com.termux/files/usr/bin/bash"
echo -e "${BF}Creating ${UR}~/.local/bin${NC}"
xsessions=$HOME/.local/etc/X11
mkdir -p $HOME/.local/bin
mkdir -p ${xsessions}

# --- Create a default X session script with full desktop ------------
echo -e "${BF}Creating ${UR}~/.local/etc/X11/xstartup${NC}"
cat <<EOF > ${xsessions}/xstartup
#!${shebang}
#
# This will launch a XFCE desktop environment.  For other
# options, see
# https://wiki.termux.com/wiki/Graphical_Environment
#
export PATH=\$HOME/.local/bin:\$PATH
xfce4-session &
EOF
chmod a+x ${xsessions}/xstartup

# --- Create X session script starting VASSAL with full desktop ------
echo -e "${BF}Creating ${UR}~/.local/etc/X11/xfce4-vassal${NC}"
cat <<EOF > ${xsessions}/xfce4-vassal
#!${shebang}
#
# This will launch a XFCE desktop environment.  For other
# options, see
#  https://wiki.termux.com/wiki/Graphical_Environment
#
export PATH=\$HOME/.local/bin:\$PATH
xfce4-session &
#
# This will execute VASSAL as the X session application.
# When VASSAL is exited, the X session will automatically end.
#
env bash \$HOME/.local/bin/vassal "\$@"
EOF
chmod a+x ${xsessions}/xfce4-vassal

# --- Create X session script starting VASSAL with simple WM ---------
echo -e "${BF}Creating ${UR}~/.local/etc/X11/fluxbox-vassal${NC}"
cat <<EOF > ${xsessions}/fluxbox-vassal
#!/data/data/com.termux/files/usr/bin/bash
#
# This will launch a XFCE desktop environment.  For other
# options, see
#  https://wiki.termux.com/wiki/Graphical_Environment
#
export PATH=\$HOME/.local/bin:\$PATH

# Generate menu.
fluxbox-generate_menu
# Start fluxbox.
fluxbox &
#
# This will execute VASSAL as the X session application.
# When VASSAL is exited, the X session will automatically end.
#
env bash \$HOME/.local/bin/vassal "\$@"
EOF
chmod a+x ${xsessions}/fluxbox-vassal

# --- Create a vassal start script in ~/.local/bin -------------------
echo -e "${BF}Creating ${UR}~/.local/bin/vassal${NC}"
mkdir -p ${HOME}/.local/bin
cat <<EOF > ${HOME}/.local/bin/vassal
#!${shebang}

exec \${HOME}/VASSAL-current/VASSAL.sh \$VASSAL_FLAGS "\$@"
EOF
chmod a+x ${HOME}/.local/bin/vassal

# --- Create X wrapper to start VASSAL in ~/.local/bin -------------
echo -e "${BF}Creating ${UR}~/.local/bin/xvassal${NC}"
cat <<EOF > ${HOME}/.local/bin/xvassal
#!${shebang}
#
# This will start up an X session with VASSAL running in it.
# When you stop the VASSAL application, the X session will
# automatically end. This will run whatever X session
# defined in ~/.local/bin/xfce4-xvassal.  Please see that file for
# more.

oper=\${1:-start}
xstart=\$HOME/.local/etc/X11/\${2:-xfce4-vassal}
shift
shift

case \$oper in
start)
    echo "IMPORTANT: Enable Termux Wake-Lock through its notification"
    termux-wake-lock
    if test \$? -ne 0 ; then 
        read -n 1 -p "Failed to aquire Wake-lock, exiting"
        exit 1
    fi

    export VASSAL_FLAGS="\$*"
    termux-x11 :1 -xstartup "dbus-launch --exit-with-session \$xstart" &

    x_pid=\$!
    sleep 1

    # termux-open-url vnc://localhost:5901
    am start -n com.termux.x11/.MainActivity

    wait \$x_pid
    am broadcast -a com.termux.x11.ACTION_STOP -p com.termux.x11

    termux-wake-unlock
    ;;
stop)
    echo "Stopping the VNC server"
    am broadcast -a com.termux.x11.ACTION_STOP -p com.termux.x11
    pkill -f com.termux.x11
    ;;
*help)
    echo "Usage: \$0 [start [session [VASSAL arguments]]|stop]"
    echo ""
    echo "session can be xfce4-vassal or fluxbox-vassal"
    echo "VASSAL arguments can be f.ex. -l GettysburgSmithsonian-1.2.vmod"
    exit 0
esac
EOF
chmod a+x ${HOME}/.local/bin/xvassal

# --- Widget set-up --------------------------------------------------
echo -e "${BF}Creating ${UR}~/.shortcuts${NC}"
mkdir -p ${HOME}/.shortcuts/icons

cat <<EOF > ${HOME}/.shortcuts/VASSAL
#!${shebang}
exec ${HOME}/.local/bin/xvassal start
EOF

vassal_icon="iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAZoklEQVR4nO2de3Rc1X3vP/uM5imNJEvyS/LbkmxsnMhvA8Y2xTYJYCCACbSFhtKb5KYpIW2TlpZ1Q18rTUuoe3Pb5ua2aXvbW9K4XW2SFsojKxAI8UsGGxvbsmxLRpIlWe+RRvM653f/ODPSPM6ZOSPJxE74rqV1RrPPPnuf32/v3/699h74AFOG/ACfHGanHOGP5Qgn5QjLd+ygpJhnFHXzBwA5ymKE2xB2Ah8Bgqmyb73AfvcYc9evZ0lzM3Enz/uAAQUg38bFcpow2IPiTgzWAcrq3uWLWAtQU8JW4AdOnv8BAywgR5mNwQ4UexD2IFRakzwTTY3g98F4hDv4gAHOId/GRT2bEO5A+CgGawGFOH9GNAbffQ3ErLPNab2feQaIoGjmFAYNU6kfS8B//hD++UUYGCGO4n/HDX7Paf2feQYohcgRjkBxDEjo8NKb8I/PQ98QKHhFgydePMTJYp7zM8+AJP4DeKiYCl/5W3itGRR0a4oHXjzE61NpWJtKpZ86xPgvIJH6NxqH9kvQetG+SiRmXgXapkp8+GAGAKBuZECOcA8aA5/+A+4+38GvA+/t3Ezoi5/gw/krO9GP7PEBA5JQG/hPgL2raY7P5qlXXyXx0te5E+F7lhWK0JDy4ZphgBxjDnEG1QZnFuZUsf8ksaIqyPRYcVUzQN6ikgR3odhLnNsQ7tm9mR6EgZcOceEn3b+ZwFXHAHkTP152IjyMzt0oPKmyH77Fc2JQjvAs8Bs/wW7OlAS6OrQgOYtXmtkjzfxfPPQifBfYC5PEB9i4mnKvG1A8+PRV0nc1TV78xGaAHMGNsAvFxxnmHqC8UB2/F9ZeBweOU/vjTdzENNS/6WJaqk8a3lcGiKDRzI2Yo/tBFHOKqMt3XoUj7wIQMRR+q/vq6lmAxjeZARo98VfU7Pu0TX8mr1e/GiqHWI3GwzTzCDC/2PqDIXjm7+GwaeR3aMJ9Lx7kkGVbLr6ihF3T67GJodHC91y1IkgOsRrFXhS/ANRP9TlvHoNn/wFGxgD4oaHxwMsH6bG6t66eGxAeWrJ0EZ/93CcBwTAEMQwMEcQQDDEyr4aBSPp1smxJ8Dyml+LKYUYZIEdZhcHHgQeBxuk+78i78PTXk8+GbwwafLb5sK0doKHx50opdc+9dyBiFEF8mbg/vcwwjOm+QkFMmwFyhEXAx4C9GNw0/S5NYu1KWFILbV2g4J/yhflqV/AJhI2btqyntm4eum5YEz0f8S1mg+17J4tkmiJoSqqcHGKeHObzcoSDQDuwD4onvggk4hCNwHg4t9ylwSfvm/h33969uKyeU7OCoIb6I6/HI7tvuwVdNzB0A93QM6966mqObvOqo6fKjPSrgYj9DFAzpAZNTZd2sQ3Fs8CmqVSPx2C4HwZ6YXgAwiGIRyGRyL13wyrYvAaApsE2ftnqeT6Dp0Rk3q27dyh/wG9P/CyiT1wzyibvMXR7BkzMALHWxpxiagwQ/gNwoCNkwtAhNAwjgyaxlQaBMqisgfJZUGIjEP/7XnC7QcEf7lxPRXrZwnqWo/FEVXWVbLlhgw3xc0d8OvF1uzIHa0BMWFQsHdIxJQaoDYRR9upBNAajFiJFgHjE/Ozxwqwa8JcWns61s+Hu7QDM0TSeSi8zFM+K4Lljz26llEojvpFG/GzC6mliKs+skMLiXReqaxumrvZOfRE2+BaK3XGdc98/wPqL3Wjtl+C9bugZhFXLGHr216lMJ67LBb5SGB8DQ4qTo79wO7xyEIZCPL5rM994+SBn6xq5Fbhref1SGhqWoRt6cqFNaj9pC+zkNbnIJhdky3vSFmtHUOxjBx/mVSyEaH5M2Z+iNvIdtYFqz2Y2ffX/8dX93+fPjpzkU70DbNOFmn3PMUvBP2bX85eC5oJEzFx8naLUD5+4CwAPBl8FXCj2aUqTj9x+6+SIN5JixUakTCzElmWZi7dh6AX75fN6UbCqrpPHnL/NJGbEDnjlEF+0LHDz2yS4ByhLfaWUKfdHhyE8aooipzPhozfC829ASzt7mq7na5djXL/phvVU11RPiIzckZ+lXlqO/Myy9HsKwR/w4x4xJJGIf3nBavZ3nGTA2duYuKIeRdVEJ/CV7O+9PnB7zEV5fKyI52nwmQdMhlV4+aTP55Ot27dMLKS5Iz9LvcwY+WkjXtctF28nDFBK0di4XIkwS+KZ65MTXHmXbohngLbsrwNBQJn6v16E5Fy1DLatA4+Ga/2qJcrr8WQR38gVN5bEtyZ6+uKdzw5IQUSoW1RHWVmpIDy+qJ7Vzt/mfWCAuoUI5IqokhJzJiCmKCoGn7wPvB4Y7bhAeHQsi/i5Iz6dsIWJPzmLnLkiBMSgcWW9QuHSNfYV8y7vS1BDbWA/iteyvw+UmWIlFjUNMaeYPQvu3wnxaJQzh37skPgpfd9mxNuIsEIQAV03qKyooLqmCmBnXQN3OH2X9y+qpPMEkKFWaBoESs3PY0XOggc/AnOqoP3EcYb7LlsQ33BIfPv1w8kaADLxzOXLlqCUEhR/Xl+P18l7vG8MUJt4G/hm9ve+ALhKzHUgYmG82cHrhl++25TBp370epYamUXYAoyxW7zzrQFprogJhrq9bmrr5ilg+bjGrzp5j/c3rurmKWAo++vS5BaH8BgU4wG+ZSNcXw8DlzrpvnAhh+gZbgYbxuRbvJ2uAemMnT9/Hm63W4Cnl6xmXqHaM8qAupVUL2pkmd3f4r2UtfXwV9n13B7THhADxosQRUrBZ/aa68i5wz8mHo/bEN+aMZl+Igt11YEIMmfAJCOVIjULgvF44SzpGWNAfT1eJeqgDufy/d3yBZ48dym3fiBoEjQSMV3UjttdBLs2w/hoiM7TJzIIa0v8dP0/j60gjmdApgirnFWJ2+MGeGxBI3X5as9YRCzs4nNKZHl9wzJqaqrMKIWYaWOSFJgiptX53KERnrr7fEZ9l8tcD8bHYCwEFVXO237sY/DGW3DxxDtULViCy+OdtIolKyqWE3TJtpIzvyuElBY0GVETRkMh4rE4wGsdLXTmqz8jDJizlLka6qnSYEB+6dGHlMfryXz5NHM/9d3FsX9hUWlmcpu/FKLj5gyIRcDjc9b+rCA8eBt88ztx2t95myVrN2a0VZj4k/dllxWCea8+QXxDN+js6BZAlOLzherPiAhye/iyiATvuHO3cnvcadM8GW1KTu90EfBG9zYMyWw+5ScCcxY4eP8J3L8T6uZA/8ULhPr7srQdG3FjsUZklBUxA1L1+/sHiEQiCvh6xxmOF6o/bQbU1rMO4ZcW1M3nw2vXpBFdz+hYtrrXF67keP+anOd5/VDiNrUhqzClHUpK4LF7ABE6TrxlEw2ziHxZqaupkKSjiJhM1I/HEvT2XBaEIYEvOen3dBmgNI19KLTb77ptouOWpr2Fi/jNSxsZT+TKmdKknygyZjrsnGLrWlh/HYQHBxjsvJiH+LoN8TMZ4yQgA0y8W2/vZXRdVwq+1NVCn5O602LAghX8vMDNTU0fYuGiutwwoJ1fPvn9WNTNG50bcp5b4gav1xxlY6Hi+vTpvWYwv+fMSeKxmGWs15b4emaZEy1IRNB1g0gkwuDgsACn55bnqtp2mDIDFizAD+rLJe4SuXXXNosYrDXx9YzRptN8aSW94VyVJ6WWxqJmEN8pFs+H27dCIhphoP2cJeMz7IB8toIDbygChmHQ23MZEKUUn3e6Sx6mwQDx86SILNy+4yYVDJaluX5tiK5biwFdN3jxfG5yhaaZWhHA2EhxffvE3VBeCsMXzxMLj9kQOM09bVlmILoDLQghFBolHB4H4bsdZ/ivYvo6JQbULmeh0tRvVlSUy+bN620Ia59vkz0Sz/fP4XTfwpx2/AHTPtD14vxEwYAZQzYMg8G2szmELUz84lwR/X39AsRx8QXnvTQxtawIF8+IiH/3R35OaS4tY/G1Jrq1ypd+fb5lHQkjqzsqGbjB9BM5kQgp3LXDzKoL9/UQGRnM8f9biaJszclJQCYR14nHE0op9nWepsV5D00UzYDaFdwE7F28eCGNK5ZPyNCM6JOVeW+j8qXEUN+onx+1r8hpz+M1fUViFBe4cWnmggww3H4uxxVtSfRstTVfamLymjDDeZc9cf7Iee8mUSwDNIR9SlPsum1HYZ+KnqWOZhA9t+yVlhWMRG3UUiAyXlz4ct1K2Hw9JMKjjPf1ZCgJudGw3CQuR74gAVE8ef48w857NomiGLCgkccUbGhqWqOqa6ptxExu0ENPI36+gEg4pvH8qVU57bpKTD8RFK+WprLqxrsvosfjDoifHpJ0EpRntOsMf1tcrybhmAE1KwgK6g88Xo9x081bCoz4TBeEdeaBtZF2oK2O9sHKnPYDZaZmFI+ZfiKnqJ1jZtUZiTjR3kvWRLfR3sSBFehRnAOmnMfumAFe4Usgc7fevEXz+byWIz59Ic4b/LYw0lL1EgmD/W9dl+MHyvATjRbnJ3r4Tqgqh9hgD3pk3JG9YugGuoNGXFpugKkYOGLAguXUK8WvVVXNkg83XZ8xqjOJnuVrLzDN05mWvpa09lZw5GJuMMnrM30+RpFqqd8Lj9wJiBDvu+SM+Mk0xysNRwwQF/tE8Oy45SaVMr1ziZ7Zebu8fEuiWxhp325uIJrI2g6gJhfk8SL9RB+9CRoXg4RH0MMhC+IbOf3PawfMEG8KMqC2kZ3AHUuXLWbh4gU2IybXiEkn8ISqaqfuWRhp/aESXjiRa5yVeMyZIEXmEynNXJCVAgZ7km0WEJeODLHpIW9AZv163N0hvqaUJlu3bVHpkR/LaFP6/qo8ZU6DIt97u46t9ZeoKctMGgqUmT6iaMR0X7s9Ni+QhdXL4ea18MOjMfrbzzMYy0pKlcx/huY6e+50kHcGvDdAlVIs8vt9BPwBC2dWenA709mWXVbISMtQVZPXSAyeO7A4t9OuST9RuEi19FP3m1l1NR5BiUFqM545aNL/ZLrncDhC3hnQe4GeuhX8cTgc/v2jR95m3camnJFvPRsMRzFYy+zlrPjsGy2zuHVVkFW1mZT2BcwZkEiYBprP4Uah2bPgvlvhn16A27evY822W5CcDXxm5vTSYBvwfKFHXtlNeu4of6oU7UePHpfh4ZEcF0KxWQfpjjqn1vM3X1tgrZamZkGRaulDyay688feZnx4CI/bg9vtTl49eDxu3G4PJW7LPYEzioIMaGsjIvAFXdfVoQPNlgTKNajyEd1i4StgpLV2+/j+u7kxA48P3N7i/UReDzx6F4hh8PYPXskQf5kOxatEDe08w36EV8+fa6O7s8c2zptO2PSZUminih3x01XWv3t1LmPR3BFZmjTOIkWmuf/cJjOrrqe9nc5zrZYz19DzxISdN5UXzn1BiicA4+ChZtHjCRtrNo3odsQvYKSlG3fpZf0hxT+/WZ3Tran6iZRKqqUanHj9VeKxeLKPmWKzEAQcJs9YwzEDOls4Bvz14MCQam29kGvN5iV6fiMtVd/egDOJsf9AJZ397py+BUpNQsZjpnrqFA2LYOcmGBsa4sI7b0/2I9VfB/GAmMFS5y3moihvqG7wlFIMHz92QqKRqEOfSm4IMF3OFvTJpxlpsbjB11/OnQWp/cZgqqXFiIdfuRcCPmg9cpjwaKaF7GQNSBjMrW1kaxFNZqCoZX50gHCwmpiu67cZhkFNTTXW2zwzjbXUIRhWZTlGmt0JJsl6bb0u1iyOUFuVKfBL3BCLmeuAUs6NM38yi7/5XZ1ELEZV3cKJPlb7Brl+jvXRdK8chO4+SAgMx2kK9fPXTGFpKDoiNi/I14CW1rMXZGQkZCFm0jUKw7LMqZGWLhLSy/Z9rwKr9bFsin6ie3fC/BroOnuGkcu9E33UVOFV3ef1gbCuroFHnLc4iaIZ0NxMXITPiog6eeK0DdELx4BtiZ6hPVnf09rl4luv5a4FJW5TNS3WT+QpgV97CBCh9chB9ITZvubgBMtAaYCSkhJRGl+pqi987Fo2phSU7zrLy8ALvT2X6e25nONCsIsB2xlp1tqTvZEWS+h8+bkEQ6O5G4xLk/lE0SLT3Desgi0fgpHLPfS2nUM3dFx5Nr6nDD+lKRpX1isR5vg0niySlNPYKa/zOBA/c/qsJBIJWyvWzkjLr7LmN9KGBgYYGhVeOJV74LmmmekskFRLi5DKn3nA3Pp08dhRYpEoJcqegxObywWWLllIaWlAFPzGguuKO4V9ygzoOEcr8L/GxsKq42KXbZzX0k6wNODyG2mp+tFolFg8Tk1NFcPBOxiI1uT0zZc6DiFe3HEI86rhkT0Qj0bofPc4Qa991Cc9OdcQWHFdgwLc6PxJMXScVm7ouMHTQO+FC+0SjcQsR3xOlnQRRlpG4CdZNjw8AgI7d9+CIYo3unN/rEKpSQu5WD/RfTvhuqXQ13aOgHIWbTQMnerqKqqrqxC4p7ae3U7bmxYDBloZQfgfuq6r9rb3LF0IThN1rYmeWTYeHsfQDeobl7F02SJ03aBtZAHnh3NtIU/qOASjuOMQNAW/8xhUlglzA/0F709FCA1dZ3nDUlBKlMY+HP6c1bTdfaEB3iqvYk84PDavorJCaS7N0rVray9k2wk2h+0ZhjAaGkXTNO69fw9er3fCNugOz+ZDNSfRVOZQL/GYPqJEwoyiaQ6HW1kA1tTDslqxPUjk5QPQ3Q8urw9v1WwMEVyuEuLxmAqFRmcHQ/SG+jlcqK2Z2CFjiOJzItDe3pETA7Yb8Rl2go0tkb4OhEfHEBHWb2yisrIiQ0PqDwdp7rk+p2MuVzJOIMUn+K5calrYdvB7TUaV+c3BYr6zzoKFdaZaKvxh3UpyzfYszNQJvNQ1sB/F/XV18/H5feboFQMxMEd1cjQLyc1vycVLktauuZhJ2v+k3WcwPhbBH/Dx2H/7RdweT87RMm4V41Nrv02ZezyjX2LAYJ+5DgRngcehhVwsTnX4efIf6hBD6BuL0d72HgJf62rh8Xz1ZowB81ayxGVwGpxt0Z8Kbt5+Ixs2NtmKtaY5Z7iz4c2cepGwqZK6XFBZzQy+9SQMgb2/CaEwuGsXc+pcF7FYPKFgSb6dkjO2TbX7NG21DTyKKvCTH1OAglrg4daz52lau4bcE3HNa3PXctbOPUNdeebi6QtM5pWOhyfjyTMJTUHTSnj9KEQHB4mZ21QPdrTQla/ejJ6c23WW54DnZvKZKdQ1wqWu7ofPnGqhvnG5bVbG8y3r+JUNL+cM8tJg8oSuaXnv82PDKpMBMh4CMDT4PAVMwaviDH4ncLv5olIq9MYbByUSjWbFpvWJz+f7q3inOzefyO0xN3+7rkCY91If/PurpmYE4EIIlPBv77UU1oKufNR5hjB0mdFgFVo8Hr9VoZg7b07WQjw5G9oHZrFl8Xlc2pWJ6RoCre/B86/DN/4V/ubf4fBJRe+Yn75xQy6FZTwBHx3uK3y26lX3Eyb54BeeiWg8euztE8uW1S9VpaV+S5ujf8zLmxeWsqO+dcbaDoXN4/MPHofD7ybPRXWVoPnLUDWl4C2lu7eP/vFxJcLTXWexOBEjF9cUA1pbiS5o4Ld00f/lyMGjbN1+g2VgpyoQYm1dx7Tbu9QHB9+BA8fh+Fnz5wuV148WCOKqLAO3byJoFI3EGOgfFOBCQPifTtu4AgrZlUddIy8Bu3bu2k7NnJoMJlR4x3h8+5tUB4pIn05CN+D0BZPgPzoGHT2ApuHylaICQbRAOYbLZWnhd3ZcYnR0DFHc3XWG7zpt85pkwKJ6VuuKY5VVFdqu3TuUiOmTKXWP87ntP2Zu0Hk0ZjhkipQDx83fKwhHQLk9uErL0UrL0XxlSaPS+sh7EYOxsTCdHZcAvt/Zws5i3uWaEkEpXGzlZF0D/2docPjT51rbWLJ0IYYhPHLjWwWJLwJnL8LBE6Z4OXsRRBQuX4CSYDn+eRUoj28ibVK3iE+n+6t0Q7jc2y8IhnIVPh0lG9ckAwCUh98lwQMnT5yqnDd/juZylXCiq5qVc3OPaIjG4OR5k+CvH03+/KyrBHdZBb66ClyBIKK5Jh1/yZQU+3OoJxkyNDRMLBZTovEXnad5p+j3mBFq/IRQt4InEP5s2fIlXLeqEY8W50/ufR2/O0HPgEbzSYMDJ6D5FMTj4PL4cJdX4gpW4vKXmYdJ5U0kznP4twiJRILOjktiGMYQGg2dpynsv87CNc0AdlBS28UxTanrtm67QQX8PlbN72dg1E1LywjD7WdxlwZxl8/CHaxE8/icnayeR+Ska1wDA0OMhkYR+NWuFv5yKq9wbTMAcwePgpdrZlfTtHbNJOF0HSOhI5qWfzOJhSGXfcqXFUOi0Ti9Pb0CnO6s5UNTOboeriFL2A6hfs6XV7MxHB5vDAbL8CUDNWIIBuQnvpFmwKUnjE2Meqt9C2b54MAguq4rMfjF0FHOTrX/14wvKB+UeSpv7GzLOUnoibRAjkWgxzIRzEGMOi3jIxwOE43GEPi3rlZemk7fr/kZADAyyEB5NZXxeOJGl8tFaWkgmc44Gc6cvE7uYUuFNNNTHzOuOXXNWTE0OCwiktBcfGykr7jfC8jGT8UMAIgqfk+EnovtHUYkGkuGNa2yra0yMyz2rlmlVeoGo6Nj6LqugGc6Tk1d9KTwUzEDAML9xCpqGDZE7tITOsHyoE3Cr81vxhg260La+qEndEKhUQEujxt8fHyAIpLhrXHNa0FZcNU1clgpmsrKypSk/c5d6nPqQNnJi+T5Lvk5eU3NCoRHO8/ydzPR4Z82BrCwgW2GxW8VzCAOd7awhWkc0JGO/w8m6z+SicP+sQAAAABJRU5ErkJggg=="

echo "$vassal_icon" | base64 -d > ${HOME}/.shortcuts/icons/VASSAL.png

# --- Create directory for VASSAL modules ----------------------------
mkdir -p ${HOME}/VMods

# --- Desktop setup --------------------------------------------------
echo -e "${BF}Creating ${UR}~/Desktop${NC}"
mkdir -p ${HOME}/Desktop

echo -e "${BF}Creating ${UR}~/Desktop/VASSAL.desktop${NC}"
cat <<EOF > ${HOME}/Desktop/VASSAL.desktop
[Desktop Entry]
Type=Application
MimeType=application/x-vassal-module;application/x-vassal-log;application/x-vassal-save
Name=VASSAL
Exec=vassal
Icon=${HOME}/VASSAL-current/VASSAL.svg
Actions=Run;Edit;

[Desktop Action Run]
Name=Run
Exec=vassal -l %f
                                   
[Desktop Action Edit]
Name=Edit                                                     
Exec=vassal -e %f
EOF
chmod a+x $HOME/Desktop/VASSAL.desktop

# --- Make application database entries ------------------------------
dsk_dir=$HOME/.local/share/applications/
mkdir -p ${dsk_dir}
cp ${HOME}/Desktop/VASSAL.desktop ${dsk_dir}/

echo -en "${BF}Updating Desktop database${NC} ... "
update-desktop-database ${dsk_dir}
echo "done"

# --- Copy icon to icon directory ------------------------------------
echo -en "${BF}Make mime icons${NC} ... "
icon_dir=${HOME}/.local/share/icons/hicolor/scalable/mimetypes
icon_nam=application-x-vassal
mkdir -p ${icon_dir}
cp ${HOME}/VASSAL-current/VASSAL.svg ${icon_dir}/${icon_nam}.svg
echo "done"

# --- Make mime database entries -------------------------------------
mime_dir=${HOME}/.local/share/mime/
echo -e "${BF}Creating ${UR}~/.local/share/mime/packages/application-x-vassal.xml${NC}"
mkdir -p ${mime_dir}/packages
cat <<EOF > ${mime_dir}/packages/application-x-vassal.xml
<?xml version="1.0"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
  <mime-type type="application/x-vassal-module">
    <comment>VASSAL module file</comment>
    <glob pattern="*.vmod"/>
    <generic-icon name="${icon_nam}"/>
  </mime-type>
  <mime-type type="application/x-vassal-log">
    <comment>VASSAL log file</comment>
    <glob pattern="*.vlog"/>
    <generic-icon name="${icon_nam}"/>
  </mime-type>
  <mime-type type="application/x-vassal-save">
    <comment>VASSAL save file</comment>
    <glob pattern="*.vsav"/>
    <generic-icon name="${icon_nam}"/>
  </mime-type>
</mime-info>
EOF

echo -en "${BF}Updating Mime database${NC} ... "
update-mime-database ${mime_dir}
echo "done"

# --- Epilog ---------------------------------------------------------
echo -e "\
${BF}You are now ready to launch VASSAL on your device.${NC}

If you have installed the Termux-Widget package, go a head
and add a Termux widget to your home screen.  It should show
the entry ${BF}VASSAL${NC}.  Pressing that entry will launch your
X interface with VASSAL running.

Otherwise, in Termux, run

    ${UR}~/.local/bin/xvassal start &${NC}

(replace ${UR}stop${NC} to stop the session, or ${UR}help${NC})

${BF}Enjoy${NC}.
"

# Local Variables:
#  mode: sh
# End:
