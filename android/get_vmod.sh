#!/data/data/com.termux/files/usr/bin/bash

# --- Colours ----------------------------------------------
# See also
# https://stackoverflow.com/questions/5947742/
# Reset
NC='\E[m'       # Text Reset

# Regular Colors
B='\E[0;30m'       # Black
R='\E[0;31m'       # Red
G='\E[0;32m'       # Green
Y='\E[0;33m'       # Yellow
B='\E[0;34m'       # Blue
P='\E[0;35m'       # Purple
C='\E[0;36m'       # Cyan
W='\E[0;37m'       # White

# Bold
BF='\E[1;30m'      # Black
BR='\E[1;31m'      # Red
BG='\E[1;32m'      # Green
BY='\E[1;33m'      # Yellow
BB='\E[1;34m'      # Blue
BP='\E[1;35m'      # Purple
BC='\E[1;36m'      # Cyan
BW='\E[1;37m'      # White

# Underline
UN='\E[4;30m'      # Black
UR='\E[4;31m'      # Red
UG='\E[4;32m'      # Green
UY='\E[4;33m'      # Yellow
UB='\E[4;34m'      # Blue
UP='\E[4;35m'      # Purple
UC='\E[4;36m'      # Cyan
UW='\E[4;37m'      # White

# Defaults 
module_url=https://obj.vassalengine.org/images/9/97/GettysburgSmithsonian-1.2.vmod
module_icon="https://gitlab.com/wargames_tex/sgb_tex/-/raw/master/.imgs/logo.png?ref_type=heads&inline=false"

# Help
usage() {
    cat <<-EOF
	Usage: $0 -u MODULE_URL [-i ICON_URL]

        Options:
          -u,--url MODULE_URL   Specify URL to get module from
          -i,--icon ICON_URL    Specify URL to get icon from
          -h,--help             This help

        Note that the icon should be a PNG of minimum resolution of 96px x 96px
	EOF
}

# Command line
while test $# -gt 0 ; do
    case $1 in
        -u|--url) module_url="$2" ; shift ;;
        -i|--icon) module_icon="$2" ; shift ;;
        -h|--help) usage ; exit 0 ;;
        *)
            echo -e "${BR}Unknown option: ${BC}$1${NC}" > /dev/stderr
            exit 1
    esac
    shift
done

# Deduced 
module_name=${module_url##*/}
module_name=`basename ${module_name%%\?*} .vmod`
module_esc=`echo $module_name | tr ' ' '_'`
module_file=${HOME}/VMods/${module_esc}.vmod

# Only get if we do not have it already 
if test ! -f ${module_file} ; then
    echo -e "${BY}Downloading ${UG}${module_name}${BY} from ${UG}${module_url}${NC}"
    wget -nv ${module_url} -O ${module_file}
else
    echo -e "${BY}Using existing ${UG}${module_file}${NC}"
fi

# Check that we got the module
if test ! -f ${module_file} ; then
    echo -e "${BR}Failed to get VASSAL module from ${UC}${module_url}${BR} or missing ${UC}${module_file}${NC}" > /dev/stderr
    exit 1
fi

# Create the short cut
echo -e "${BY}Creating short cut ${UG}${module_name}${NC}"
cat <<EOF > ${HOME}/.shortcuts/${module_name}
#!/data/data/com.termux/files/usr/bin/bash

exec ${HOME}/.local/bin/vncvassal start xsimple_vassal -l ${module_file}
EOF
chmod 755 ${HOME}/.shortcuts/${module_name}

# Create the X short cut 
cat <<EOF > ${HOME}/.shortcuts/X${module_name}
#!/data/data/com.termux/files/usr/bin/bash

exec ${HOME}/.local/bin/xvassal start xfce4-vassal -l ${module_file}
EOF
chmod 755 ${HOME}/.shortcuts/X${module_name}

# Function to get icon
get_icon()
{
    url="$1"
    tgt="$2"

    if test -f $tgt ; then
        echo -e "${BY}Using existing ${UG}${tgt}${BY} as icon${NC}"
        return
    fi

    if test "x$url" = "x" ; then
        echo -e "${BY}No icon specified${NC}"
        return
    fi
    
    echo -e "${BY}Downloading ${UG}${url} as icon${NC}"
    wget -nv "${url}" -O ${tgt}.tmp

    magick convert ${tgt}.tmp ${tgt}
    rm -f ${tgt}.tmp
}

# Create Icons
get_icon "${module_icon}" $HOME/.shortcuts/icons/${module_name}.png
get_icon "${module_icon}" $HOME/.shortcuts/icons/X${module_name}.png

#
# EOF
#
