#!/bin/bash
#
# Image mapping
#
# android_01_termux_web		upload://h6GlhPS47IrLOSKz9dU9ZAS75FX.png
# android_02_termux_inst	upload://sizMpT4HKaDzX2eHzp9Up7LbUho.png
# android_03_termux_apk		upload://qV6gicHKjWWc1bxDw7tH8fZDwS8.png
# android_04_termux_drawer	upload://rl25iBLDyx68qQZS9UucU7hDIe5.png
# android_05_termux_storage	upload://eDHRljgHPGiprk3RsiExdH5NkNx.png
# android_05_termuxx11		upload://cbrKjzEoOqg7O8uFSYLGBayPzWO.png
# android_05_termuxx11_apk      upload://mvKOnSjP54F7fDkCSjncFVibwCS.png
# android_06_termuxwidget_inst	upload://nSHTKiQGZgYRrXmHXybFzYBARTF.png
# android_07_termuxwidget_apk	upload://22o0faEHKUdxkxumZ2ZRAokZuWk.png
# android_08_setup		upload://oDIIu9HKRX1hwm2GsZYlYFBQlWE.png
# android_09_ready		upload://t1xdPz0vZx6vVdcbzhBqpJBDjDR.png
# android_10_done 		upload://s9P94Rqn0UNGokydxvTlbRBHPVn.png
# android_11_widgetmenu		upload://vpTNMcIxNRlhW5Z2AUvWa2Svbvg.png
# android_11_widget		upload://4yAhnjuommITLFKFKzEB94dao8S.png
# android_12_widgetlist		upload://h0Ek7WNJZtjwZGlJ7UpbujAdI2t.png
# android_13_desktop		upload://3pXM9QJRAbwXIHxlIJ6diUvTK6V.png
# android_14_sgb		upload://rWDMrorbWcF3rE52FiDPtfZmaKO.jpeg

process() {
    in=$1
    out=$2 
    sed \
        -e 's,android_01_termux_web.png,upload://h6GlhPS47IrLOSKz9dU9ZAS75FX.png,' \
        -e 's,android_02_termux_inst.png,upload://sizMpT4HKaDzX2eHzp9Up7LbUho.png,' \
        -e 's,android_03_termux_apk.png,upload://qV6gicHKjWWc1bxDw7tH8fZDwS8.png,' \
        -e 's,android_04_termux_drawer.png,upload://rl25iBLDyx68qQZS9UucU7hDIe5.png,' \
        -e 's,android_05_termux_storage.png,upload://eDHRljgHPGiprk3RsiExdH5NkNx.png,' \
	-e 's,android_05_termuxx11.png,upload://cbrKjzEoOqg7O8uFSYLGBayPzWO.png,' \
	-e 's,android_05_termuxx11_apk.png,upload://mvKOnSjP54F7fDkCSjncFVibwCS.png,' \
        -e 's,android_06_termuxwidget_inst.png,upload://nSHTKiQGZgYRrXmHXybFzYBARTF.png,' \
        -e 's,android_07_termuxwidget_apk.png,upload://22o0faEHKUdxkxumZ2ZRAokZuWk.png,' \
        -e 's,android_08_setup.png,upload://oDIIu9HKRX1hwm2GsZYlYFBQlWE.png,' \
        -e 's,android_09_ready.png,upload://t1xdPz0vZx6vVdcbzhBqpJBDjDR.png,' \
        -e 's,android_10_done.png,upload://s9P94Rqn0UNGokydxvTlbRBHPVn.png,' \
        -e 's,android_11_widgetmenu.png,upload://vpTNMcIxNRlhW5Z2AUvWa2Svbvg.png,' \
        -e 's,android_11_widget.png,upload://4yAhnjuommITLFKFKzEB94dao8S.png,' \
        -e 's,android_12_widgetlist.png,upload://h0Ek7WNJZtjwZGlJ7UpbujAdI2t.png,' \
        -e 's,android_13_desktop.png,upload://3pXM9QJRAbwXIHxlIJ6diUvTK6V.png,' \
        -e 's,android_14_sgb.png,upload://rWDMrorbWcF3rE52FiDPtfZmaKO.jpeg,' \
        < $in > $out
}

process README.md post.md
process XREADME.md xpost.md

    
