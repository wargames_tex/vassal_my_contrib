# Various contributions for VASSAL 

- [`android`](android) Run VASSAL on Android using Termux
- [`steamdeck`](steamdeck) Run VASSAL on SteamDeck 
- [`common`](common) Common files 
- [`pybuild`](pybuild)
  - Make XML snippets and images of NATO App6 prototypes
  - [Example](pybuild/example) of generating a VASSAL module using Python 
- [`imagemagick`](imagemagic) Script to use [ImageMagick](https://imagemagick.org) to do various manipulations of counter images (borders, bevels, drop-shadows,...).

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode 0) -->
<!-- eval: (visual-line-mode) -->
<!-- eval: (visual-fill-column-mode) -->
<!-- eval: (adaptive-wrap-prefix-mode) -->
<!-- End: -->
