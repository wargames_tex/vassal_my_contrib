#
#
#
include Docker.mk

docker-artifacts:
	$(MAKE) -C pybuild pngs
	$(MAKE) -C pybuild svgs
	$(MAKE) -C pybuild/example
	mkdir -p public
	cp -a pybuild/pngs public/
	cp -a pybuild/svgs public/
	mkdir -p public/example/
	cp pybuild/example/Test.vmod public/example/

local-clean:
	rm -rf public *~ 

local-realclean:local-clean

clean:	local-clean
	$(MAKE) -C pybuild clean
	$(MAKE) -C pybuild/example clean

realclean: local-realclean
	$(MAKE) -C pybuild realclean
	$(MAKE) -C pybuild/example realclean

#
#
#

