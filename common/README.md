# Common scripts, etc. 

- [`dot-local-share-mime-packages-application-x-vassal.xml`](dot-local-share-mime-packages-application-x-vassal.xml) 

  Application database entry 
  
- [`VASSAL.desktop`](VASSAL.desktop)

  Application launcer 
  
- [`integration.sh`](https://gitlab.com/wargames_tex/vassal_my_contrib/-/raw/master/common/integration.sh)

  Script that makes all desktop integration and can remove them again. 
  
  By default, it assumes it resides in the same directory as VASSAL,
  but option allows the user to specify where VASSAL is installed. 
  
  It makes the integration for a single user or system-wide. 
  
  Download by right-clicking and select _Save target as..._. 
  
  Run 
  
        ./integration --help
  
  to get more help on using this script. 

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode 0) -->
<!-- eval: (visual-line-mode) -->
<!-- eval: (visual-fill-column-mode) -->
<!-- eval: (adaptive-wrap-prefix-mode) -->
<!-- End: -->
