#!/bin/bash
recover=steamdeck-repair-20231127.10-3.5.7.img
new_image=steamos.qcow2

run_qemu()
{
    qemu-system-x86_64 \
        -enable-kvm \
        -smp cores=4 \
        -m 8G \
        -device usb-ehci \
        -device usb-tablet \
        -device intel-hda \
        -device hda-duplex \
        -device VGA,xres=1280,yres=800 \
        -netdev user,id=net0,hostfwd=tcp::2222-:22 \
        -drive if=pflash,format=raw,readonly=on,file=/usr/share/ovmf/OVMF.fd \
        $@
}

if test ! -f first ; then 
    if test ! -f ${new_image} ; then 
        qemu-img create -f qcow2 ${new_image} 64G
    fi


    cat <<-EOF
        In SteamOS, open a terminal and do

            ~/tools/repair_reimage.sh

        Select "proceed" in the pop-up dialog to re-image.
	Then, select "cancel" in the second pop-up dialog, to _not_ reboot.

        Then make missing lock dir and copy session file here 

            sudo mkdir /var/lock/dev
            cp /etc/sddm.conf.d/zz-steamos-autologin.conf .

        And do for 'A'

        
            sudo steamos-chroot --no-overlay /dev/nvme0n1 --partset A -- /bin/bbash
            steamos-readwrite disable
            echo -e "[Autologin]\nSession=plasma.desktop" \
                 > /mnt/etc/sddm.conf.d/zz-steamos-autologin.conf
            exit

        and the same for 'B'

	SSH into the machine
	
	  ssh -p 2222 deck@localhost    
EOF

    run_qemu \
        -drive if=virtio,file=${recover},driver=raw \
        -device nvme,drive=drive0,serial=badbeef \
        -drive if=none,id=drive0,file=${new_image}

    touch first

else

    if test ! -f OVMF_VARS.fd ; then 
        cp /usr/share/OVMF/OVMF_VARS_4M.fd .
    fi

    run_qemu \
        -drive if=pflash,format=raw,file=OVMF_VARS_4M.fd \
        -drive if=virtio,file=${new_image} \
        -device virtio-net-pci,netdev=net0 
fi
