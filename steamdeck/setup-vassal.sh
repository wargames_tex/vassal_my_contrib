#!/bin/bash
#
# This script is _EXPERIMENTAL_
#
# Post questions, suggestions, bugs, etc. to the VASSAL Technical
# Support and Bugs sub-forum.
#
# Change the VASSAL version to the version you want to install
vassal_version=3.7.9
# Change the Java version to the one available in Termux 
java_version=17
# Location of VASSAL downloads 
github_dl=https://github.com/vassalengine/vassal/releases/download/
# Archive to unpack
archive=VASSAL-${vassal_version}-linux.tar.bz2
# Example module
example=BattleForMoscow-ch-1.0.vmod

# --- Colours ----------------------------------------------
# See also
# https://stackoverflow.com/questions/5947742/
# Reset
NC='\E[m'       # Text Reset

# Regular Colors
B='\E[0;30m'       # Black
R='\E[0;31m'       # Red
G='\E[0;32m'       # Green
Y='\E[0;33m'       # Yellow
B='\E[0;34m'       # Blue
P='\E[0;35m'       # Purple
C='\E[0;36m'       # Cyan
W='\E[0;37m'       # White

# Bold
BF='\E[1;30m'      # Black
BR='\E[1;31m'      # Red
BG='\E[1;32m'      # Green
BY='\E[1;33m'      # Yellow
BB='\E[1;34m'      # Blue
BP='\E[1;35m'      # Purple
BC='\E[1;36m'      # Cyan
BW='\E[1;37m'      # White

# Underline
UN='\E[4;30m'      # Black
UR='\E[4;31m'      # Red
UG='\E[4;32m'      # Green
UY='\E[4;33m'      # Yellow
UB='\E[4;34m'      # Blue
UP='\E[4;35m'      # Purple
UC='\E[4;36m'      # Cyan
UW='\E[4;37m'      # White

echo -e "\
This script will to install ${BF}VASSAL${NC} and needed
applications on your SteamDeck.
device.

This script is ${BF}EXPERIMENTAL${NC} - it may or may not work.  ${UR}No
Warrenty!${NC}

The script assumes that you have set a password for the ${BC}deck${NC} user by

    ${UB}passwd${NC}

If you have not done that, please end this script ${UR}now${NC}.

The script also assumes you have access to the internet.  If
not, then you need to download the Linux VASSAL app

    ${UB}${archive}${NC}

to your home directory, or exit the script ${UR}now${NC}.

The script will install VASSAL version ${BG}${vassal_version}${NC}.

If you want to install a different version, end the script
now and edit the line

    ${UB}vassal_version=${vassal_version}${NC}

in the script, and re-execute the script.
"


read -n 1 -p "Are you ready? (Y/n)"
case x$REPLY in
    x|xy|xY) ;;
    *) echo "OK, ending the script"; exit 0 ;;
esac

# --- Fail on errors -------------------------------------------------
set -e

# --- Go Home --------------------------------------------------------
echo -e "${BF}Going Home: ${UR}${HOME}${NC}"
cd $HOME

# --- Enable write ---------------------------------------------------
sudo steamos-readonly disable

# -- Initialize keys -------------------------------------------------
echo -e "${BF}Setting up pacman keys${NC}"
sudo pacman-key --init
sudo pacman-key --populate holo

# --- Install Java ---------------------------------------------------
echo -e "${BF}Installing Java Runtime Environment${NC}"
sudo pacman -S --noconfirm --color auto --needed jre-openjdk 

# --- Download VASSAL ------------------------------------------------
if test ! -f ${archive} ; then
    echo -e "${BF}Downloading VASSAL version ${UR}${vassal_version}${NC}"
    rm -f ${archive}*
    wget -nv --show-progress ${github_dl}/${vassal_version}/${archive}

    if test ! -f ${archive} ; then
        echo "Failed to download VASSAL"
        exit 1
    fi
else
    echo -e "${BF}Using existing ${UR}${archive}${NC}"
fi

# --- Unpack VASSAL --------------------------------------------------
echo -en "${BF}Unpacking VASSAL version$ ${UR}${vassal_version}${NC} ... "
tar -xjf ${archive}

rm -f VASSAL-current
ln -s VASSAL-${vassal_version} VASSAL-current
echo "done"

# --- Create directory for VASSAL modules ----------------------------
echo -e "${BF}Create directory for modules ${UR}~/VMods${NC}"
mkdir -p ${HOME}/VMods

# --- Desktop setup --------------------------------------------------
echo -e "${BF}Creating ${UR}~/Desktop${NC}"
mkdir -p ${HOME}/Desktop

echo -e "${BF}Creating ${UR}~/Desktop/VASSAL.desktop${NC}"
cat <<EOF > ${HOME}/Desktop/VASSAL.desktop
[Desktop Entry]
Type=Application
MimeType=application/x-vassal-module;application/x-vassal-log;application/x-vassal-save
Name=VASSAL
Exec=${HOME}/VASSAL-current/VASSAL.sh
Icon=${HOME}/VASSAL-current/VASSAL.svg
Actions=Run;Edit;
Categories=Game

[Desktop Action Run]
Name=Run
Exec=${HOME}/VASSAL-current/VASSAL.sh -l %f
                                   
[Desktop Action Edit]
Name=Edit                                                     
Exec=${HOME}/VASSAL-current/VASSAL.sh -e %f
EOF
chmod a+x $HOME/Desktop/VASSAL.desktop

dsk_dir=$HOME/.local/share/applications/
mkdir -p ${dsk_dir}
cp ${HOME}/Desktop/VASSAL.desktop ${dsk_dir}/

echo -en "${BF}Updating Desktop database${NC} ... "
update-desktop-database ${dsk_dir}
echo "done"

echo -en "${BF}Make mime icons${NC} ... "
icon_dir=${HOME}/.local/share/icons/hicolor/scalable/mimetypes
icon_nam=application-x-vassal
mkdir -p ${icon_dir}
cp ${HOME}/VASSAL-current/VASSAL.svg ${icon_dir}/${icon_nam}.svg
echo "done"

mime_dir=${HOME}/.local/share/mime
echo -e "${BF}Creating ${UR}~/.local/share/mime/packages/application-x-vassal.xml${NC}"
mkdir -p ${mime_dir}/packages
cat <<EOF > ${mime_dir}/packages/application-x-vassal.xml
<?xml version="1.0"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
  <mime-type type="application/x-vassal-module">
    <comment>VASSAL module file</comment>
    <glob pattern="*.vmod"/>
    <icon name="${icon_nam}"/>
  </mime-type>
  <mime-type type="application/x-vassal-log">
    <comment>VASSAL log file</comment>
    <glob pattern="*.vlog"/>
    <icon name="${icon_nam}"/>
  </mime-type>
  <mime-type type="application/x-vassal-save">
    <comment>VASSAL save file</comment>
    <glob pattern="*.vsav"/>
    <icon name="${icon_nam}"/>
  </mime-type>
</mime-info>
EOF

echo -en "${BF}Updating Mime database${NC} ... "
update-mime-database ${mime_dir}
echo "done"

# --- VASSAL alias ---------------------------------------------------
echo -e "${BF}Create alias for VASSAL: ${UR}vassal${NC}"
echo -e "\n# VASSAL alias\nalias vassal=$HOME/VASSAL-current/VASSAL.sh\n" \
     >> ${HOME}/.bashrc

# --- Example module -------------------------------------------------
echo -e "${BF}Getting example module: ${UR}Battle for Moscow${NC}"
wget -nv --show-progress \
     https://obj.vassalengine.org/images/e/e1/${example} \
     -O ${HOME}/VMods/${example}

# --- Enable write ---------------------------------------------------
sudo steamos-readonly enable

# --- Epilog ---------------------------------------------------------
echo -e "\
${BF}You are now ready to launch VASSAL on your device.${NC}

I've made a Desktop icon that will launch VASSAL for your. You can also
open a new terminal and type

    vassal

I've downloaded an example module for you to play with.  You can run that with

    vassal -l ${HOME}/VMods/${example}

${BF}Enjoy${NC}.
"

# Local Variables:
#  mode: sh
# End:
